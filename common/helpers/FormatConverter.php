<?php

namespace app\helpers;

use IntlDateFormatter;
use yii\helpers\BaseFormatConverter;

class FormatConverter extends BaseFormatConverter
{
    private static $_icuShortFormats = [
        'short' => 3, // IntlDateFormatter::SHORT,
        'medium' => 2, // IntlDateFormatter::MEDIUM,
        'long' => 1, // IntlDateFormatter::LONG,
        'full' => 0, // IntlDateFormatter::FULL,
    ];

    public static function convertDateIcuToJui($pattern, $type = 'date', $locale = null)
    {
        if (isset(self::$_icuShortFormats[$pattern])) {
            if (extension_loaded('intl')) {
                if ($locale === null) {
                    $locale = \Yii::$app->language;
                }
                if ($type === 'date') {
                    $formatter = new IntlDateFormatter(
                        $locale, self::$_icuShortFormats[$pattern],
                        IntlDateFormatter::NONE
                    );
                } elseif ($type === 'time') {
                    $formatter = new IntlDateFormatter(
                        $locale, IntlDateFormatter::NONE,
                        self::$_icuShortFormats[$pattern]
                    );
                } else {
                    $formatter = new IntlDateFormatter(
                        $locale, self::$_icuShortFormats[$pattern],
                        self::$_icuShortFormats[$pattern]
                    );
                }
                $pattern = $formatter->getPattern();
            } else {
                return static::$juiFallbackDatePatterns[$pattern][$type];
            }
        }
        // http://userguide.icu-project.org/formatparse/datetime#TOC-Date-Time-Format-Syntax
        // escaped text
        $escaped = [];
        if (preg_match_all('/(?<!\')\'.*?[^\']\'(?!\')/', $pattern, $matches)) {
            foreach ($matches[0] as $match) {
                $escaped[$match] = $match;
            }
        }

        return strtr(
            $pattern,
            array_merge(
                $escaped,
                [
                    'G' => '',
                    // era designator like (Anno Domini)
                    'Y' => '',
                    // 4digit year of "Week of Year"
                    'y' => 'yy',
                    // 4digit year e.g. 2014
                    'yy' => 'yy',
                    // 2digit year number eg. 14
                    'yyyy' => 'yy',
                    // 4digit year e.g. 2014
                    'u' => '',
                    // extended year e.g. 4601
                    'U' => '',
                    // cyclic year name, as in Chinese lunar calendar
                    'r' => '',
                    // related Gregorian year e.g. 1996
                    'Q' => '',
                    // number of quarter
                    'QQ' => '',
                    // number of quarter '02'
                    'QQQ' => '',
                    // quarter 'Q2'
                    'QQQQ' => '',
                    // quarter '2nd quarter'
                    'QQQQQ' => '',
                    // number of quarter '2'
                    'q' => '',
                    // number of Stand Alone quarter
                    'qq' => '',
                    // number of Stand Alone quarter '02'
                    'qqq' => '',
                    // Stand Alone quarter 'Q2'
                    'qqqq' => '',
                    // Stand Alone quarter '2nd quarter'
                    'qqqqq' => '',
                    // number of Stand Alone quarter '2'
                    'M' => 'm',
                    // Numeric representation of a month, without leading zeros
                    'MM' => 'mm',
                    // Numeric representation of a month, with leading zeros
                    'MMM' => 'M',
                    // A short textual representation of a month, three letters
                    'MMMM' => 'MM',
                    // A full textual representation of a month, such as January or March
                    'MMMMM' => '',
                    //
                    'L' => 'm',
                    // Stand alone month in year
                    'LL' => 'mm',
                    // Stand alone month in year
                    'LLL' => 'M',
                    // Stand alone month in year
                    'LLLL' => 'MM',
                    // Stand alone month in year
                    'LLLLL' => '',
                    // Stand alone month in year
                    'w' => '',
                    // ISO-8601 week number of year
                    'ww' => '',
                    // ISO-8601 week number of year
                    'W' => '',
                    // week of the current month
                    'd' => 'd',
                    // day without leading zeros
                    'dd' => 'dd',
                    // day with leading zeros
                    'D' => 'o',
                    // day of the year 0 to 365
                    'F' => '',
                    // Day of Week in Month. eg. 2nd Wednesday in July
                    'g' => '',
                    // Modified Julian day. This is different from the conventional Julian day number in two regards.
                    'E' => 'D',
                    // day of week written in short form eg. Sun
                    'EE' => 'D',
                    'EEE' => 'D',
                    'EEEE' => 'DD',
                    // day of week fully written eg. Sunday
                    'EEEEE' => '',
                    'EEEEEE' => '',
                    'e' => '',
                    // ISO-8601 numeric representation of the day of the week 1=Mon to 7=Sun
                    'ee' => '',
                    // php 'w' 0=Sun to 6=Sat isn't supported by ICU -> 'w' means week number of year
                    'eee' => 'D',
                    'eeee' => '',
                    'eeeee' => '',
                    'eeeeee' => '',
                    'c' => '',
                    // ISO-8601 numeric representation of the day of the week 1=Mon to 7=Sun
                    'cc' => '',
                    // php 'w' 0=Sun to 6=Sat isn't supported by ICU -> 'w' means week number of year
                    'ccc' => 'D',
                    'cccc' => 'DD',
                    'ccccc' => '',
                    'cccccc' => '',
                    'a' => '',
                    // am/pm marker
                    'h' => '',
                    // 12-hour format of an hour without leading zeros 1 to 12h
                    'hh' => '',
                    // 12-hour format of an hour with leading zeros, 01 to 12 h
                    'H' => '',
                    // 24-hour format of an hour without leading zeros 0 to 23h
                    'HH' => '',
                    // 24-hour format of an hour with leading zeros, 00 to 23 h
                    'k' => '',
                    // hour in day (1~24)
                    'kk' => '',
                    // hour in day (1~24)
                    'K' => '',
                    // hour in am/pm (0~11)
                    'KK' => '',
                    // hour in am/pm (0~11)
                    'm' => '',
                    // Minutes without leading zeros, not supported by php but we fallback
                    'mm' => '',
                    // Minutes with leading zeros
                    's' => '',
                    // Seconds, without leading zeros, not supported by php but we fallback
                    'ss' => '',
                    // Seconds, with leading zeros
                    'S' => '',
                    // fractional second
                    'SS' => '',
                    // fractional second
                    'SSS' => '',
                    // fractional second
                    'SSSS' => '',
                    // fractional second
                    'A' => '',
                    // milliseconds in day
                    'z' => '',
                    // Timezone abbreviation
                    'zz' => '',
                    // Timezone abbreviation
                    'zzz' => '',
                    // Timezone abbreviation
                    'zzzz' => '',
                    // Timzone full name, not supported by php but we fallback
                    'Z' => '',
                    // Difference to Greenwich time (GMT) in hours
                    'ZZ' => '',
                    // Difference to Greenwich time (GMT) in hours
                    'ZZZ' => '',
                    // Difference to Greenwich time (GMT) in hours
                    'ZZZZ' => '',
                    // Time Zone: long localized GMT (=OOOO) e.g. GMT-08:00
                    'ZZZZZ' => '',
                    //  TIme Zone: ISO8601 extended hms? (=XXXXX)
                    'O' => '',
                    // Time Zone: short localized GMT e.g. GMT-8
                    'OOOO' => '',
                    //  Time Zone: long localized GMT (=ZZZZ) e.g. GMT-08:00
                    'v' => '',
                    // Time Zone: generic non-location (falls back first to VVVV and then to OOOO) using the ICU defined fallback here
                    'vvvv' => '',
                    // Time Zone: generic non-location (falls back first to VVVV and then to OOOO) using the ICU defined fallback here
                    'V' => '',
                    // Time Zone: short time zone ID
                    'VV' => '',
                    // Time Zone: long time zone ID
                    'VVV' => '',
                    // Time Zone: time zone exemplar city
                    'VVVV' => '',
                    // Time Zone: generic location (falls back to OOOO) using the ICU defined fallback here
                    'X' => '',
                    // Time Zone: ISO8601 basic hm?, with Z for 0, e.g. -08, +0530, Z
                    'XX' => '',
                    // Time Zone: ISO8601 basic hm, with Z, e.g. -0800, Z
                    'XXX' => '',
                    // Time Zone: ISO8601 extended hm, with Z, e.g. -08:00, Z
                    'XXXX' => '',
                    // Time Zone: ISO8601 basic hms?, with Z, e.g. -0800, -075258, Z
                    'XXXXX' => '',
                    // Time Zone: ISO8601 extended hms?, with Z, e.g. -08:00, -07:52:58, Z
                    'x' => '',
                    // Time Zone: ISO8601 basic hm?, without Z for 0, e.g. -08, +0530
                    'xx' => '',
                    // Time Zone: ISO8601 basic hm, without Z, e.g. -0800
                    'xxx' => '',
                    // Time Zone: ISO8601 extended hm, without Z, e.g. -08:00
                    'xxxx' => '',
                    // Time Zone: ISO8601 basic hms?, without Z, e.g. -0800, -075258
                    'xxxxx' => '',
                    // Time Zone: ISO8601 extended hms?, without Z, e.g. -08:00, -07:52:58
                ]
            )
        );
    }
}