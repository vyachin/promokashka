<?php

namespace common\helpers;

use yii\helpers\Url;

abstract class Image
{
    public static function url($name, $width, $height, $schema = false): string
    {
        return Url::to(['/image/get', 'name' => $name, 'width' => $width, 'height' => $height], $schema);
    }
}
