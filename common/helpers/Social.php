<?php

namespace app\helpers;

use yii\web\View;

class Social
{
    public static function RegisterFacebookInit(View $view){
        $view->registerJs(
            'if (FB && FB.Event && FB.Event.subscribe) {
                FB.init({appId: "820882854708575", version: "v2.5", xfbml: true});
                FB.Event.subscribe("edge.create", function(url) {
                    ga && ga("send", "social", "facebook", "like", url);
                });
                FB.Event.subscribe("edge.remove", function(url) {
                    ga && ga("send", "social", "facebook", "unlike", url);
                });
            }',
            View::POS_READY,
            'fbinit'
        );
    }

    public static function RegisterVkInit(View $view)
    {
        $view->registerJs('if (VK && VK.Observer && VK.Observer.subscribe) {
            VK.init({apiId: 5296221, onlyWidgets: true});
            VK.Observer.subscribe("widgets.like.liked", function(e,b) {
                ga && ga("send", "social", "vkontakte", "like", location.href );
            });
            VK.Observer.subscribe("widgets.like.unliked", function(e,b) {
                ga && ga("send", "social", "vkontakte", "unlike", location.href );
            });
        }', View::POS_READY, 'vkinit');
    }
}