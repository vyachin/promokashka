<?php

namespace common\helpers;


abstract class AssetHelper
{
    public static function getUrl($path)
    {
        $v = static::getHash($path);

        return \Yii::getAlias("@web/assets/{$path}?_={$v}");
    }

    public static function getHash($path)
    {
        return filemtime(\Yii::getAlias("@webroot/assets/{$path}"));
    }

    public static function getContent($path)
    {
        return file_get_contents(\Yii::getAlias("@webroot/assets/{$path}"));
    }
}