<?php
/**
 * Этот файл не для исполнения. Он не подключается в других файлах, а нужен
 * лишь для того, чтобы IDE делала правильные подсказки при написании кода.
 *
 * Для избавления от предупреждения PhpStorm "Multiple Implementations"
 * нужно пометить исходный файл vendor/yiisoft/yii2/Yii.php как Plain Text
 */

namespace {

    use common\components\ContentWatch;
    use common\components\GoogleUrlShortener;
    use common\components\Twitter;
    use yii\i18n\Formatter;
    use yii\queue\Queue;
    use yii\redis\Connection;

    require __DIR__.'/../../vendor/yiisoft/yii2/BaseYii.php';

    /**
     * Class Yii
     */
    class Yii extends \yii\BaseYii
    {
        /**
         * @var WebApplication|ConsoleApplication the application instance
         */
        public static $app;
    }

    /**
     *
     * @property-read Queue $queue
     * @property-read ContentWatch $antiplagiat
     * @property-read GoogleUrlShortener $shortUrl
     * @property-read Twitter $twitter
     * @property-read Connection $redis
     * @property-read \yii\sphinx\Connection $sphinx
     * @property-read \common\components\Formatter $formatter
     */
    trait BaseApplication
    {
    }

    /**
     * @property Formatter $formatter
     */
    class WebApplication extends yii\web\Application
    {
        use BaseApplication;
    }

    /**
     */
    class ConsoleApplication extends yii\console\Application
    {
        use BaseApplication;
    }
}
