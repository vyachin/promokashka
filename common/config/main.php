<?php

return [
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'bootstrap' => ['log', 'queue'],
    'vendorPath' => __DIR__ . '/../../vendor',
    'runtimePath' => __DIR__ . '/../../runtime',
    'aliases' => [
        '@bower' => __DIR__ . '/../../vendor/bower-asset',
        '@npm' => __DIR__ . '/../../vendor/npm-asset',
        '@storage' => __DIR__ . '/../../storage',
    ],
    'components' => [
        'authManager' => [
            'class' => \yii\rbac\DbManager::class,
            'cache' => 'cache',
        ],
        'db' => [
            'class' => \yii\db\Connection::class,
            'charset' => 'utf8',
            'enableSchemaCache' => true,
        ],
        'cache' => [
            'class' => \yii\redis\Cache::class,
        ],
        'redis' => [
            'class' => \yii\redis\Connection::class,
        ],
        'mailer' => [
            'class' => \yii\swiftmailer\Mailer::class,
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => 5,
            'targets' => [
                'file' => [
                    'class' => \yii\log\FileTarget::class,
                    'logFile' => '@runtime/logs/app.log',
                    'levels' => ['error', 'warning'],
                    'except' => YII_DEBUG ? [] : [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                    ],
                ],
                'rollbar' => [
                    'class' => \common\components\rollbar\RollbarTarget::class,
                    'levels' => ['error', 'warning'],
                    'enabled' => !YII_DEBUG,
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                    ],
                ],
            ],
        ],
        'rollbar' => [
            'class' => \common\components\rollbar\RollbarComponent::class,
            'serverToken' => '',
            'clientToken' => '',
            'root' => '@application',
            'environment' => 'production',
        ],
        'urlManager' => [
            'class' => \yii\web\UrlManager::class,
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
        ],
        'antiplagiat' => [
            'class' => \common\components\ContentWatch::class,
        ],
        'shortUrl' => [
            'class' => \common\components\GoogleUrlShortener::class,
        ],
        'twitter' => [
            'class' => \common\components\Twitter::class,
        ],
        'queue' => [
            'class' => \yii\queue\redis\Queue::class,
            'serializer' => \yii\queue\serializers\JsonSerializer::class,
            'channel' => 'promokashka',
        ],
        'formatter' => \common\components\Formatter::class,
    ],
];
