<?php

namespace common\widgets;


use yii\base\Widget;

class YandexMetrika extends Widget
{
    public function run()
    {
        return $this->render('metrika');
    }
}