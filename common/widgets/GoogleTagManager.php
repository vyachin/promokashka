<?php

namespace common\widgets;


use yii\base\Widget;

class GoogleTagManager extends Widget
{
    public function run()
    {
        return $this->render('googletagmanager');
    }
}