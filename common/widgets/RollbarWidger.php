<?php

namespace common\widgets;

use common\components\rollbar\RollbarComponent;
use yii\base\Widget;
use yii\di\Instance;
use yii\web\View;

class RollbarWidger extends Widget
{
    /** @var string|RollbarComponent */
    public $rollbar = 'rollbar';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->rollbar = Instance::ensure($this->rollbar, RollbarComponent::class);
    }

    public function run()
    {
        $this->getView()->registerJs(
            $this->render(
                'rollbar',
                [
                    'accessToken' => $this->rollbar->clientToken,
                    'environment' => $this->rollbar->environment,
                ]
            ),
            View::POS_HEAD
        );
    }
}
