<?php

namespace common\widgets;


use yii\base\Widget;

class GoogleAnalytics extends Widget
{
    public function run()
    {
        return $this->render('analytics');
    }
}