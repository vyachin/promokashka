<?php

namespace common\components;

use Abraham\TwitterOAuth\TwitterOAuth;
use yii\base\Component;
use yii\helpers\Json;

class Twitter extends Component
{
    public $consumer_key;
    public $consumer_secret;
    public $access_token;
    public $access_token_secret;

    /** @var TwitterOAuth */
    private $connection;

    public function init()
    {
        $this->connection = new TwitterOAuth(
            $this->consumer_key,
            $this->consumer_secret,
            $this->access_token,
            $this->access_token_secret
        );
        parent::init();
    }

    /**
     * @param string $status
     * @return mixed
     */
    public function updateStatus($status)
    {
        try {
            $result = $this->connection->post('statuses/update', ['status' => $status]);
            if (isset($result->id_str)) {
                return $result->id_str;
            }
            throw new \Exception(Json::encode($result));
        } catch (\Exception $e) {
            \Yii::error($e, 'twitter');
        }

        return null;
    }
}