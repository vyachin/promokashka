<?php

namespace common\components;


use common\models\Category;
use common\models\Offer;
use common\models\Post;
use common\models\Seo;
use common\models\Shop;

class Formatter extends \yii\i18n\Formatter
{
    public function asShopStatusName($value)
    {
        return Shop::getStatusList()[$value] ?? null;
    }

    public function asOfferStatusName($value)
    {
        return Offer::getStatusList()[$value] ?? null;
    }

    public function asOfferTypeName($value)
    {
        return Offer::getTypeList()[$value] ?? null;
    }

    public function asCategoryStatusName($value)
    {
        return Category::getStatusList()[$value] ?? null;
    }

    public function asPostStatusName($value)
    {
        return Post::getStatusList()[$value] ?? null;
    }

    public function asSeoPageName($value)
    {
        return Seo::getPageList()[$value] ?? null;
    }
}