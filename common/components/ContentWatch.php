<?php

namespace common\components;

use yii\base\Component;
use yii\helpers\Json;

class ContentWatch extends Component
{
    const CONTENT_WATCH_URL = 'http://www.content-watch.ru/public/api/';
    const CONTENT_WATCH = 'content-watch.ru';

    public $key;

    /**
     * @param $text
     * @return float
     */
    public function checkText($text)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt(
            $curl,
            CURLOPT_POSTFIELDS,
            [
                'key' => $this->key,
                'text' => $text,
            ]
        );
        curl_setopt($curl, CURLOPT_URL, self::CONTENT_WATCH_URL);
        $body = curl_exec($curl);
        curl_close($curl);
        $result = Json::decode($body);
        if (empty($result['error'])) {
            return isset($result['percent']) ? $result['percent'] * 0.01 : 0;
        }
        \Yii::error($body, self::CONTENT_WATCH);

        return null;
    }

    public function getBalance()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt(
            $curl,
            CURLOPT_POSTFIELDS,
            [
                'action' => 'GET_BALANCE',
                'key' => $this->key,
            ]
        );
        curl_setopt($curl, CURLOPT_URL, self::CONTENT_WATCH_URL);
        $return = Json::decode(curl_exec($curl));
        curl_close($curl);

        return $return['balance'] ?? false;
    }
}