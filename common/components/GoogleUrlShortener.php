<?php

namespace common\components;

use yii\base\Component;
use yii\helpers\Json;

class GoogleUrlShortener extends Component
{
    const URL = 'https://www.googleapis.com/urlshortener/v1/url';
    public $key;

    /**
     * @param string $url
     * @return string
     */
    public function shortUrl($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, Json::encode(['longUrl' => $url]));
        curl_setopt($curl, CURLOPT_URL, self::URL.'?'.http_build_query(['key' => $this->key]));
        $httpResult = curl_exec($curl);
        curl_close($curl);
        try {
            $result = Json::decode($httpResult);
            if (empty($result['id'])) {
                throw new \Exception('Google Shortener API response '.$httpResult);
            }

            return $result['id'];
        } catch (\Exception $e) {
            \Yii::error($e, 'GoogleUrlShortener');
        }

        return null;
    }
}