<?php

namespace common\components\rollbar;


use yii\di\Instance;
use yii\log\Target;

class RollbarTarget extends Target
{
    /** @var RollbarComponent */
    public $rollbar = 'rollbar';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->rollbar = Instance::ensure($this->rollbar, RollbarComponent::class);
    }

    public function export()
    {
        $this->rollbar->export($this->messages);
    }

    public function getContextMessage()
    {
        return '';
    }
}