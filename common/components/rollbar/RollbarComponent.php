<?php

namespace common\components\rollbar;


use Rollbar\Payload\Level;
use Rollbar\Rollbar;
use yii\log\Logger;

class RollbarComponent extends \yii\base\Component
{
    public $serverToken;
    public $clientToken;
    public $root;
    public $environment;
    public $baseApiUrl = 'https://api.rollbar.com/api/1/';
    public $batchSize;
    public $batched;
    public $branch;
    public $codeVersion;
    public $host;
    public $includedErrno;
    public $logger;
    public $personFn;
    public $scrubFields = ['passwd', 'password', 'secret', 'auth_token', '_csrf'];
    public $timeout = 3;
    public $proxy;
    public $enableUtf8Sanitization = true;

    protected $requestId;

    public function init()
    {
        $this->requestId = uniqid($_SERVER['HTTP_HOST'] ?? null, true);
        parent::init();

        Rollbar::init(
            [
                'framework' => 'Yii '.\Yii::getVersion(),
                'access_token' => $this->serverToken,
                'environment' => $this->environment,
                'root' => $this->root ? \Yii::getAlias($this->root) : null,
                'base_api_url' => $this->baseApiUrl,
                'batch_size' => $this->batchSize,
                'batched' => $this->batched,
                'branch' => $this->branch,
                'code_version' => $this->codeVersion,
                'host' => $this->host,
                'included_errno' => $this->includedErrno,
                'logger' => $this->logger,
                'person_fn' => $this->personFn,
                'scrub_fields' => $this->scrubFields,
                'timeout' => $this->timeout,
                'proxy' => $this->proxy,
                'enable_utf8_sanitization' => $this->enableUtf8Sanitization,

            ],
            false,
            false,
            false
        );
    }

    public function export(array $messages)
    {
        foreach ($messages as $message) {
            $levelName = self::getLevelName($message[1]);
            $extra = [
                'category' => $message[2],
                'request_id' => $this->requestId,
                'timestamp' => (int)$message[3],
            ];
            Rollbar::log(Level::$levelName(), $message[0], $extra);
        }
    }

    protected static function getLevelName($level): string
    {
        switch ($level) {
            case Logger::LEVEL_PROFILE:
            case Logger::LEVEL_PROFILE_BEGIN:
            case Logger::LEVEL_PROFILE_END:
            case Logger::LEVEL_TRACE:
                return 'debug';
            default:
                return Logger::getLevelName($level);
        }
    }
}