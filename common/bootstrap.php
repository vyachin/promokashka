<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../vendor/yiisoft/yii2/Yii.php';

Yii::setAlias('frontend', __DIR__.'/../frontend');
Yii::setAlias('backend', __DIR__.'/../backend');
Yii::setAlias('console', __DIR__.'/../console');
Yii::setAlias('common', __DIR__);
Yii::setAlias('application', dirname(__DIR__));
