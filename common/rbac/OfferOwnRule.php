<?php

namespace common\rbac;

use yii\rbac\Rule;

class OfferOwnRule extends Rule
{
    public $name = 'OfferOwnRule';

    public function execute($user, $item, $params)
    {
        return $params['model']->author_id == \Yii::$app->user->id;
    }
}
