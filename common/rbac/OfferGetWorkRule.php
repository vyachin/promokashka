<?php

namespace common\rbac;

use common\models\Offer;
use yii\rbac\Rule;

class OfferGetWorkRule extends Rule
{
    public $name = 'OfferGetWorkRule';

    public function execute($user, $item, $params)
    {
        return !$params['model']->author_id && $params['model']->status == Offer::STATUS_DRAFT;
    }
}
