<?php

namespace common\rbac;

use common\models\Offer;
use yii\rbac\Rule;

class OfferViewRule extends Rule
{
    public $name = 'OfferViewRule';

    public function execute($user, $item, $params)
    {
        return !$params['model']->author_id && $params['model']->status == Offer::STATUS_DRAFT;
    }
}
