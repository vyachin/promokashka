<?php

namespace common\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $page
 * @property integer $user_id
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $created_at
 * @property string $updated_at
 */
class Seo extends ActiveRecord
{
    public const MAIN_PAGE = 1;
    public const SHOP_PAGE = 2;
    public const CATEGORY_PAGE = 3;
    public const BLOG_PAGE = 4;

    public static function tableName(): string
    {
        return '{{%seo}}';
    }

    public static function getPageList(): array
    {
        return [
            self::MAIN_PAGE => 'Главная страница',
            self::SHOP_PAGE => 'Все магазина',
            self::CATEGORY_PAGE => 'Все категории',
            self::BLOG_PAGE => 'Все посты',
        ];
    }

    public function rules(): array
    {
        return [
            [['seo_title', 'seo_description', 'seo_keywords',], 'trim'],
            ['page', 'required'],
            ['page', 'unique'],
            ['page', 'in', 'range' => array_keys(static::getPageList())],
            [['seo_title', 'seo_description', 'seo_keywords',], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => '#',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'user_id' => 'Автор',
            'page' => 'Страница',
        ];
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => 'user_id',
            ],
        ];
    }
}
