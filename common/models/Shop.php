<?php

namespace common\models;

use voskobovich\behaviors\ManyToManyBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%shop}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $logo
 * @property string $domain
 * @property string $status
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property-read string $statusName
 * @property integer $admitad_id
 * @property integer $cityads_id
 * @property integer $actionpay_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $rate_sum
 * @property int $rate_count
 * @property Offer[] $offers
 */
class Shop extends ActiveRecord
{
    public const  STATUS_DRAFT = 'draft';
    public const  STATUS_ACTIVE = 'active';
    public const  STATUS_DELETED = 'deleted';

    /**
     * @return ShopQuery
     */
    public static function find()
    {
        return new ShopQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%shop}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    'name',
                    'domain',
                    'logo',
                    'admitad_id',
                    'cityads_id',
                    'actionpay_id',
                    'description',
                    'seo_title',
                    'seo_description',
                    'seo_keywords',
                ],
                'trim',
            ],
            ['domain', 'filter', 'filter' => 'strtolower'],
            [['name', 'domain'], 'required'],
            [['name', 'domain', 'logo', 'seo_title', 'seo_description', 'seo_keywords',], 'string', 'max' => 255],
            ['description', 'safe'],
            ['status', 'default', 'value' => self::STATUS_DRAFT],
            ['status', 'in', 'range' => array_keys(static::getStatusList())],
            [
                ['admitad_id', 'cityads_id', 'actionpay_id'],
                'filter',
                'filter' => function ($value) {

                    return $value > 0 ? $value : '';
                },
            ],
            [['admitad_id', 'cityads_id', 'actionpay_id'], 'integer'],
            ['user_list', 'each', 'rule' => ['integer']],
        ];
    }

    public static function getStatusList(): array
    {
        return [
            self::STATUS_DRAFT => 'Черновик',
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DELETED => 'Удален',
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'name' => 'Наименование',
            'domain' => 'Домен',
            'description' => 'Описание',
            'logo' => 'Логотип',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'admitad_id' => '#admitad',
            'cityads_id' => '#cityads',
            'actionpay_id' => '#actionpay',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'offerCount' => 'Кол-во купонов',
            'user_list' => 'Редакторы',
        ];
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
            ],
            [
                'class' => ManyToManyBehavior::class,
                'relations' => [
                    'user_list' => 'users',
                ],
            ],
        ];
    }

    public function getOfferCount()
    {
        return $this->getOffers()->count();
    }

    public function getActiveOfferCount()
    {
        return $this->getOffers()->andWhere(['status' => [Offer::STATUS_ACTIVE, Offer::STATUS_DRAFT]])->count();
    }

    public function getChangedOfferCount()
    {
        return $this->getOffers()->andWhere('author_id IS NOT NULL')->count();
    }

    /**
     * @return OfferQuery
     */
    public function getOffers()
    {
        return $this->hasMany(Offer::class, ['shop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('{{%user_shop}}', ['shop_id' => 'id']);
    }

    public function getRate()
    {
        return $this->rate_count ? round($this->rate_sum / $this->rate_count) : 0;
    }

    public function getFormattedRateCount()
    {
        return \Yii::t(
            'app',
            '{n, plural, =0{нет оценок} =1{одна оценка} one{# оценка} few{# оценки} many{# оценок} other{# оценки}}',
            ['n' => $this->rate_count]
        );
    }
}
