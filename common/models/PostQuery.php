<?php

namespace common\models;

use yii\db\ActiveQuery;

class PostQuery extends ActiveQuery
{
    public function published()
    {
        return $this->andWhere(['status' => Post::STATUS_PUBLISHED]);
    }

    public function slug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }
}