<?php

namespace common\models;

use yii\db\ActiveQuery;

class OfferQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['{{%offer}}.status' => Offer::STATUS_ACTIVE]);
    }

    public function valid()
    {
        return $this->andWhere(['>', '{{%offer}}.end_at',date('Y-m-d H:i:s')])
            ->andWhere(['{{%offer}}.status'=>[Offer::STATUS_ACTIVE, Offer::STATUS_DRAFT, Offer::STATUS_READY]]);
    }

    public function old()
    {
        return $this->andWhere(
            ['AND', '{{%offer}}.author_id IS NOT NULL', ['{{%offer}}.status' => Offer::STATUS_DELETED]]
        );
    }
}