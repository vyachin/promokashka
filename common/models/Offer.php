<?php

namespace common\models;

use console\workers\CheckDescriptionWorker;
use console\workers\TwitterWorker;
use voskobovich\behaviors\ManyToManyBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%offer}}".
 *
 * @property integer $id
 * @property integer $shop_id
 * @property string $name
 * @property string $description
 * @property string $url
 * @property string $begin_at
 * @property string $end_at
 * @property string $promo_code
 * @property float $discount
 * @property string $type
 * @property string $status
 * @property integer $admitad_id
 * @property integer $cityads_id
 * @property integer $actionpay_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $user_id
 * @property integer $author_id
 * @property float $content_unique_percent
 * @property string $twitter_status_id
 * @property string $category_names
 * @property integer $count_used
 * @property integer $count_like
 * @property integer $count_dislike
 * @property User $user
 * @property User $author
 * @property Shop $shop
 * @property integer[] $category_list
 * @property Category[] $categories
 */
class Offer extends ActiveRecord
{
    public const  TYPE_DISCOUNT = 'discount';
    public const  TYPE_FREE_SHIPPING = 'free_shipping';
    public const  TYPE_GIFT = 'gift';
    public const  TYPE_CASH = 'cash';

    public const  STATUS_DRAFT = 'draft';
    public const  STATUS_ACTIVE = 'active';
    public const  STATUS_DELETED = 'deleted';
    public const  STATUS_READY = 'ready';

    public const  SCENARIO_EDITOR_UPDATE = 'scenario-editor-update';

    /**
     * @return OfferQuery
     */
    public static function find()
    {
        return new OfferQuery(static::class);
    }

    public static function tableName(): string
    {
        return '{{%offer}}';
    }

    public function rules(): array
    {
        return [
            [['name', 'status'], 'required'],
            [['name', 'description'], 'trim'],
            ['name', 'string', 'max' => '255'],
            ['shop_id', 'required', 'except' => self::SCENARIO_EDITOR_UPDATE],
            [
                'shop_id',
                'exist',
                'targetClass' => Shop::class,
                'targetAttribute' => 'id',
                'except' => self::SCENARIO_EDITOR_UPDATE,
            ],
            [
                'author_id',
                'exist',
                'targetClass' => User::class,
                'targetAttribute' => 'id',
                'except' => self::SCENARIO_EDITOR_UPDATE,
            ],
            ['status', 'in', 'range' => array_keys(static::getStatusList()), 'except' => self::SCENARIO_EDITOR_UPDATE,],
            [
                'status',
                'in',
                'range' => [self::STATUS_DRAFT, self::STATUS_READY],
                'on' => self::SCENARIO_EDITOR_UPDATE,
            ],
            [
                'description',
                'filter',
                'filter' => function ($value) {
                    return trim(preg_replace('/\s+/', ' ', $value));
                },
            ],
            [
                'description',
                'string',
                'min' => 500,
                'max' => 600,
                'when' => function ($model) {
                    return in_array($model->status, [self::STATUS_ACTIVE, self::STATUS_READY]);
                },
            ],
            ['type', 'in', 'range' => array_keys(static::getTypeList())],
            ['discount', 'default', 'value' => 0],
            ['discount', 'double'],
            [
                ['admitad_id', 'cityads_id'],
                'filter',
                'filter' => function ($value) {

                    return $value > 0 ? $value : '';
                },
                'except' => self::SCENARIO_EDITOR_UPDATE,
            ],
            [['admitad_id', 'cityads_id', 'actionpay_id'], 'integer', 'except' => self::SCENARIO_EDITOR_UPDATE,],
            ['content_unique_percent', 'double', 'except' => self::SCENARIO_EDITOR_UPDATE,],
            ['category_list', 'each', 'rule' => ['integer']],
        ];
    }

    public static function getStatusList(): array
    {
        return [
            self::STATUS_DRAFT => 'Черновик',
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DELETED => 'Удален',
            self::STATUS_READY => 'Готов',
        ];
    }

    public static function getTypeList(): array
    {
        return [
            self::TYPE_CASH => 'Деньги в подарок',
            self::TYPE_DISCOUNT => 'Скидка',
            self::TYPE_FREE_SHIPPING => 'Бесплатная доставка',
            self::TYPE_GIFT => 'Подарок',
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => '#',
            'shop_id' => 'Магазин',
            'name' => 'Наименование',
            'description' => 'Описание',
            'type' => 'Тип',
            'typeName' => 'Тип',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'discount' => 'Скидка',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'admitad_id' => '#admitad',
            'cityads_id' => '#cityads',
            'actionpay_id' => '#actionpay',
            'begin_at' => 'Действителен с',
            'end_at' => 'Действителен до',
            'user_id' => 'Автор',
            'content_unique_percent' => 'Уникальность',
            'descriptionLength' => 'Символов',
            'author_id' => 'Редактор',
            'category_names' => 'Оригинальные категории',
            'category_list' => 'Категории',
            'count_like' => 'Like',
            'count_dislike' => 'DisLike',
            'count_used' => 'Использовано',
        ];
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => false,
                'updatedByAttribute' => 'user_id',
            ],
            [
                'class' => ManyToManyBehavior::class,
                'relations' => [
                    'category_list' => 'categories',
                ],
            ],
        ];
    }

    public function getShop()
    {
        return $this->hasOne(Shop::class, ['id' => 'shop_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getAuthor()
    {
        return $this->hasOne(User::class, ['id' => 'author_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (!$insert && isset($changedAttributes['status'])) {
            \Yii::$app->queue->push(new CheckDescriptionWorker(['id' => $this->id]));
            \Yii::$app->queue->push(new TwitterWorker(['id' => $this->id]));
        }
    }

    public function getDescriptionLength()
    {
        return mb_strlen($this->description, \Yii::$app->charset);
    }

    public function getToWork()
    {
        if (!$this->author_id && $this->status == self::STATUS_DRAFT) {
            $this->author_id = \Yii::$app->user->id;

            return $this->save();
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])
            ->viaTable('{{%offer_category}}', ['offer_id' => 'id']);
    }

    private function getTodayCountUsedKey()
    {
        return 'todayUsedCounter:'.date('Y-m-d').':'.$this->id;
    }

    public function incrementTodayCountUsed()
    {
        \Yii::$app->redis->executeCommand('INCR', [$this->getTodayCountUsedKey()]);
        $this->updateCounters(['count_used' => 1]);
    }

    public function getSuccessFormatterPercents()
    {
        $total = (int)$this->count_like + (int)$this->count_dislike;
        $value = $total ? (int)(100 * $this->count_like / $total) : 100;

        return $value.'&nbsp;%';
    }

    public function incrementLike()
    {
        return $this->updateCounters(['count_like' => 1]);
    }

    public function incrementDislike()
    {
        return $this->updateCounters(['count_dislike' => 1]);
    }

    public function getLikeCounters()
    {
        return [
            'like' => $this->count_like,
            'dislike' => $this->count_dislike,
        ];
    }
}
