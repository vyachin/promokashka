<?php

namespace common\models;

use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $icon
 * @property string $status
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property-read string $statusName
 * @property string $created_at
 * @property string $updated_at
 * @property int $rate_sum
 * @property int $rate_count
 */
class Category extends ActiveRecord
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DELETED = 'deleted';

    /**
     * @return CategoryQuery
     */
    public static function find()
    {
        return new CategoryQuery(static::class);
    }

    public static function tableName(): string
    {
        return '{{%category}}';
    }

    public function rules(): array
    {
        return [
            [['name', 'seo_title', 'seo_description', 'seo_keywords'], 'trim'],
            ['name', 'required'],
            [['name', 'icon', 'seo_title', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            ['description', 'safe'],
            ['status', 'default', 'value' => self::STATUS_DELETED],
            ['status', 'in', 'range' => array_keys(static::getStatusList())],
        ];
    }

    public static function getStatusList(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DELETED => 'Удален',
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'name' => 'Наименование',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'icon' => 'Иконка',
            'description' => 'Описание',
        ];
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
            ],
        ];
    }

    public function getRate()
    {
        return $this->rate_count ? round($this->rate_sum / $this->rate_count) : 0;
    }

    public function getFormattedRateCount()
    {
        return \Yii::t(
            'app',
            '{n, plural, =0{нет оценок} =1{одна оценка} one{# оценка} few{# оценки} many{# оценок} other{# оценки}}',
            ['n' => $this->rate_count]
        );
    }
}
