<?php

namespace common\models;

use HTMLPurifier_Config;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Post
 * @package common\models
 * @property integer $id
 * @property string $slug
 * @property string $image
 * @property string $title
 * @property string $body
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_title
 * @property integer $status
 * @property integer $user_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $published_at
 * @property int $rate_sum
 * @property int $rate_count
 */
class Post extends ActiveRecord
{
    public const DELIMITER = '_____DELIMITER_____';
    public const STATUS_DRAFT = 0;
    public const STATUS_PUBLISHED = 1;
    public const STATUS_DELETED = 2;

    /**
     * @return PostQuery
     */
    public static function find()
    {
        return new PostQuery(static::class);
    }

    public static function tableName(): string
    {
        return '{{%post}}';
    }

    public function rules(): array
    {
        return [
            [['title', 'seo_description', 'seo_keywords', 'seo_title'], 'trim'],
            [['title', 'seo_description', 'seo_keywords', 'seo_title', 'body',], 'required'],
            [['title', 'seo_description', 'seo_keywords', 'seo_title', 'image'], 'string', 'max' => 255],
            ['image', 'default', 'value' => ''],
            [
                'body',
                'filter',
                'filter' => function ($value) {
                    return \yii\helpers\HtmlPurifier::process(
                        $value,
                        function (HTMLPurifier_Config $config) {
                        }
                    );
                },
            ],
            ['body', 'safe'],
            ['status', 'default', 'value' => self::STATUS_DRAFT],
            ['status', 'in', 'range' => array_keys(static::getStatusList())],
        ];
    }

    public static function getStatusList(): array
    {
        return [
            self::STATUS_DRAFT => 'Черновик',
            self::STATUS_PUBLISHED => 'Опубликован',
            self::STATUS_DELETED => 'Удален',
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'title' => 'Заголовок',
            'image' => 'Картинка',
            'status' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'published_at' => 'Опубликован',
        ];
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => 'user_id',
            ],
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'ensureUnique' => true,
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->status == self::STATUS_PUBLISHED && !$this->published_at) {
            $this->published_at = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getRate()
    {
        return $this->rate_count ? round($this->rate_sum / $this->rate_count) : 0;
    }

    public function getFormattedRateCount()
    {
        return \Yii::t(
            'app',
            '{n, plural, =0{нет оценок} =1{одна оценка} one{# оценка} few{# оценки} many{# оценок} other{# оценки}}',
            ['n' => $this->rate_count]
        );
    }
}
