<?php

namespace common\models;

use yii\db\ActiveQuery;

class CategoryQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['status' => Shop::STATUS_ACTIVE]);
    }

    public function slug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }
}