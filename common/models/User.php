<?php

namespace common\models;

use voskobovich\behaviors\ManyToManyBehavior;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $email
 * @property string $password
 * @property string $passwordVerify
 * @property string $auth_key
 * @property string $created_at
 * @property string $updated_at
 * @property string $last_login_at
 */
class User extends ActiveRecord implements IdentityInterface
{
    public const ROLE_EDITOR = 'editor';
    public const ROLE_ADMIN = 'admin';
    public const ROLE_SEO = 'seo';

    public const SHOP_LIST = 'shop-list';
    public const SHOP_CREATE = 'shop-create';
    public const SHOP_UPDATE = 'shop-update';

    public const USER_LIST = 'user-list';
    public const USER_CREATE = 'user-create';
    public const USER_UPDATE = 'user-update';
    public const USER_LOGIN = 'user-login';

    public const OFFER_LIST = 'offer-list';
    public const OFFER_EDITOR_LIST = 'offer-editor-list';

    public const OFFER_UPDATE = 'offer-update';
    public const OFFER_EDITOR_UPDATE = 'offer-editor-update';
    public const OFFER_VIEW = 'offer-view';
    public const OFFER_EDITOR_VIEW = 'offer-editor-view';
    public const OFFER_GET_WORK = 'offer-get-work';
    public const CATEGORY_LIST = 'category-list';
    public const CATEGORY_CREATE = 'category-create';
    public const CATEGORY_UPDATE = 'category-update';

    public const SEO_LIST = 'seo-list';
    public const SEO_CREATE = 'seo-create';
    public const SEO_UPDATE = 'seo-update';

    public const POST_LIST = 'post-list';
    public const POST_CREATE = 'post-create';
    public const POST_UPDATE = 'post-update';


    public $password;
    public $passwordVerify;
    public $roles = [];

    public static function tableName(): string
    {
        return '{{%user}}';
    }

    /**
     * @param string $email
     * @return User
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return void|IdentityInterface
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('');
    }

    public static function onAfterLogin()
    {
        /** @var User $user */
        $user = \Yii::$app->user->identity;
        $user->last_login_at = date('Y-m-d H:i:s');
        $user->save(false, ['last_login_at']);
    }

    public function rules(): array
    {
        return [
            [['username', 'email'], 'trim'],
            [['username', 'email'], 'required'],
            ['roles', 'each', 'rule' => ['in', 'range' => array_keys(static::getRoleList())]],
            ['email', 'email'],
            ['email', 'unique'],
            [['username', 'email', 'password', 'passwordVerify'], 'string', 'max' => 255],
            ['passwordVerify', 'compare', 'compareAttribute' => 'password'],
            ['shop_list', 'each', 'rule' => ['integer']],
        ];
    }

    public static function getRoleList(): array
    {
        return [
            self::ROLE_ADMIN => 'Администратор',
            self::ROLE_EDITOR => 'Редактор',
            self::ROLE_SEO => 'SEO',
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'username' => 'Имя пользователя',
            'email' => 'Email',
            'roles' => 'Роли',
            'password' => 'Пароль',
            'passwordVerify' => 'Повторите пароль',
            'created_at' => 'Создан',
            'last_login_at' => 'Последний заход',
            'shop_list' => 'Магазины',
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key == $authKey;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
            [
                'class' => ManyToManyBehavior::class,
                'relations' => [
                    'shop_list' => 'shops',
                ],
            ],
        ];
    }

    /**
     * @param string $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $authManager = \Yii::$app->authManager;
        $authManager->revokeAll($this->id);
        if (is_array($this->roles)) {
            foreach ($this->roles as $roleName) {
                $role = $authManager->getRole($roleName);
                $authManager->assign($role, $this->id);
            }
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->password) {
                $this->password_hash = \Yii::$app->security->generatePasswordHash($this->password);
                $this->generateAuthKey();
            }
            if (!$this->auth_key) {
                $this->generateAuthKey();
            }

            return true;
        }

        return false;
    }

    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    public function afterFind()
    {
        $this->roles = [];
        $authManager = \Yii::$app->authManager;
        foreach ($authManager->getAssignments($this->id) as $item) {
            $this->roles[] = $item->roleName;
        }
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShops()
    {
        return $this->hasMany(Shop::class, ['id' => 'shop_id'])->viaTable('{{%user_shop}}', ['user_id' => 'id']);
    }
}
