<?php

namespace common\models;

use yii\db\ActiveQuery;

class ShopQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['{{%shop}}.status' => Shop::STATUS_ACTIVE]);
    }

    public function slug($slug)
    {
        return $this->andWhere(['{{%shop}}.slug' => $slug]);
    }
}