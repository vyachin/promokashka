<?php

namespace common\controllers;

use Imagine\Image\AbstractImagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ImageController extends Controller
{
    /**
     * @param $name
     * @param $width
     * @param $height
     * @return \yii\console\Response|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionGet($name, $width, $height)
    {
        $supportImageSizes = [
            320 => [180 => 1],
            50 => [20 => 1, 50 => 1],
            74 => [45 => 1],
            87 => [50 => 1],
            105 => [88 => 1],
            146 => [82 => 1],
            178 => [102 => 1],
            240 => [180 => 1],
            600 => [400 => 1],
            968 => [504 => 1],
        ];
        if (!isset($supportImageSizes[$width][$height])) {
            throw new NotFoundHttpException();
        }

        return $this->getAndProcessImage(
            $name,
            function (AbstractImagine $imagine, ImageInterface $image) use ($height, $width) {
                $sourceSize = $image->getSize();
                if ($width * $sourceSize->getHeight() > $height * $sourceSize->getWidth()) {
                    $newSize = $sourceSize->scale($height / $sourceSize->getHeight());
                    $startPoint = new Point(($width - $newSize->getWidth()) / 2, 0);
                } else {
                    $newSize = $sourceSize->scale($width / $sourceSize->getWidth());
                    $startPoint = new Point(0, ($height - $newSize->getHeight()) / 2);
                }

                return $imagine->create(new Box($width, $height))->paste($image->resize($newSize), $startPoint);
            }
        );
    }

    public function actionView($name)
    {
        return $this->getAndProcessImage(
            $name,
            function (AbstractImagine $imagine, ImageInterface $image) {
                return $image;
            }
        );
    }

    private function getAndProcessImage(string $name, callable $processImage)
    {
        $imagine = new \Imagine\Gd\Imagine();
        $hash = base64_encode(\Yii::$app->params['yandex']['login'].':'.\Yii::$app->params['yandex']['password']);
        $opts = [
            'http' => [
                'method' => 'GET',
                'header' => "Authorization: Basic $hash\r\n",
            ],
        ];
        $context = stream_context_create($opts);
        $fp = fopen("https://webdav.yandex.ru/v2/$name", 'rb', false, $context);
        $image = $imagine->read($fp)->strip();
        fclose($fp);
        $format = pathinfo($name, PATHINFO_EXTENSION);
        $content = $processImage($imagine, $image)->get($format);
        $imagePath = \Yii::getAlias('@storage'.Url::current());
        FileHelper::createDirectory(dirname($imagePath), 0777);
        file_put_contents($imagePath, $content);
        if ($format === 'png') {
            exec("optipng -o7 {$imagePath}");
        } elseif ($format === 'jpg') {
            exec("jpegoptim {$imagePath}");
        }

        return \Yii::$app->response->sendFile($imagePath, $name, ['inline' => true]);
    }
}