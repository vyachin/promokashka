<?php

return [
    'id' => 'promocode-application',
    'basePath' => dirname(__DIR__),
    'name' => 'Promokashka.com',
    'controllerNamespace' => 'frontend\controllers',
    'controllerMap' => [
        'image' => \common\controllers\ImageController::class,
    ],
    'components' => [
        'assetManager' => [
            'bundles' => false,
        ],
        'request' => [
            'cookieValidationKey' => '9dc33af2c7d19b9a4515a4900054a540',
            'enableCsrfValidation' => false,
        ],
        'errorHandler' => [
            'errorAction' => '/site/error',
        ],
        'session' => [
            'class' => 'yii\redis\Session',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                ['pattern' => 'sitemap', 'route' => 'site/map', 'suffix' => '.xml'],
                'shops' => 'shop/index',
                'blog' => 'post/index',
                'blog/<slug>' => 'post/view',
                '<_c:(category|shop|post)>/rate' => '<_c>/rate',
                'categories' => 'category/index',
                '<_c:(shop|category)>/<slug>' => '<_c>/view',
                '' => 'site/index',
                '<_a:(search|error|like|dislike)>' => 'site/<_a>',
                'out/<id:\d+>' => 'site/out',
                'site/shop' => 'site/shop',
                'image/<width>x<height>/<name>' => 'image/get',
                'image/<name>' => 'image/view',
            ],
        ],
        'log' => [
            'targets' => [
                'file' => [
                    'logFile' => '@runtime/logs/frontend.log',
                ],
            ],
        ],
    ],
];
