<?php

namespace frontend\widgets;

use common\models\Offer;
use common\models\Shop;
use yii\base\Widget;
use yii\db\Query;

class NextShops extends Widget
{
    const CACHE_DURATION = 86400;

    public $shopId;

    public function run()
    {
        $key = __CLASS__.':'.$this->shopId;
        if (!$result = \Yii::$app->cache->get($key)) {
            $subQuery = (new Query())->select('shop_id')->from('{{%offer}}')->andWhere(
                '{{%offer}}.shop_id={{%shop}}.id'
            )->andWhere(['NOT', ['{{%offer}}.status' => Offer::STATUS_DELETED]]);
            $allShopIds = (new Query())->select('id')->from('{{%shop}}')
                ->andWhere(['status' => Shop::STATUS_ACTIVE])
                ->andWhere(['EXISTS', $subQuery])
                ->orderBy(['id' => SORT_ASC])->column();
            $allShopIds = array_merge($allShopIds, $allShopIds);
            $pos = array_search($this->shopId, $allShopIds) + 1;

            $shops = Shop::find()
                ->andWhere(['id' => array_slice($allShopIds, $pos, 6)])
                ->select('id, slug, logo, name')
                ->asArray()
                ->all();

            $result = $this->render('_shops', ['shops' => $shops, 'title' => 'Популярные магазины']);
            \Yii::$app->cache->set($key, $result, self::CACHE_DURATION);
        }

        return $result;
    }
}