<?php

namespace frontend\widgets;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    public $options = ['class' => 'breadcrumb', 'itemscope' => '', 'itemtype' => 'http://schema.org/BreadcrumbList'];
    public $itemTemplate = '<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">{link}</li>';
    public $activeItemTemplate = '<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">{link}</li>';

    public function renderItemEx($link, $template, $index)
    {
        $encodeLabel = ArrayHelper::remove($link, 'encode', $this->encodeLabels);
        if (array_key_exists('label', $link)) {
            $label = $encodeLabel ? Html::encode($link['label']) : $link['label'];
            $label = Html::tag(
                    'span',
                    $label,
                    ['itemprop' => 'name']
                ).'<meta itemprop="position" content="'.$index.'" />';
        } else {
            throw new InvalidConfigException('The "label" element is required for each link.');
        }
        if (isset($link['template'])) {
            $template = $link['template'];
        }
        if (isset($link['url'])) {
            $options = $link;
            $options['itemprop'] = 'item';
            unset($options['template'], $options['label'], $options['url']);
            $link = Html::a($label, $link['url'], $options);
        } else {
            $link = $label;
        }

        return strtr($template, ['{link}' => $link]);
    }

    public function run()
    {
        if (empty($this->links)) {
            return;
        }
        $links = [];
        $index = 1;
        if ($this->homeLink === null) {
            $links[] = $this->renderItemEx(
                [
                    'label' => \Yii::t('yii', 'Home'),
                    'url' => \Yii::$app->homeUrl,
                ],
                $this->itemTemplate,
                $index++
            );
        } elseif ($this->homeLink !== false) {
            $links[] = $this->renderItemEx($this->homeLink, $this->itemTemplate, $index++);
        }
        foreach ($this->links as $link) {
            if (!is_array($link)) {
                $link = ['label' => $link];
            }
            $links[] = $this->renderItemEx(
                $link,
                isset($link['url']) ? $this->itemTemplate : $this->activeItemTemplate,
                $index++
            );
        }
        echo Html::tag($this->tag, implode('', $links), $this->options);
    }
}
