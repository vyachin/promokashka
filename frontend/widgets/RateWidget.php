<?php

namespace frontend\widgets;


use yii\base\Widget;

class RateWidget extends Widget
{
    public $value;
    public $id;

    public function run()
    {
        $value = round($this->value);

        return $this->render('_rate', ['value' => $value, 'id' => $this->id]);
    }
}