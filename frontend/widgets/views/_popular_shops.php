<?php
/**
 * @var yii\web\View $this
 * @var array $shops
 */

use common\helpers\Image;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<section class="popular-stores">
    <div class="popular-stores--inner js-store-carousel">
        <?php foreach ($shops as $shop): ?>
            <a class="popular-stores--item" href="<?= Url::to(['/shop/view', 'slug' => $shop['slug']]) ?>">
                <?= Html::img(
                    Image::url($shop['logo'], 178, 102),
                    ['alt' => $shop['name'], 'title' => $shop['name'], 'class' => 'img-fluid']
                ) ?>
                <span class="popular-stores--name"><?= Html::encode($shop['name']) ?></span>
            </a>
        <?php endforeach; ?>
    </div>
</section>
