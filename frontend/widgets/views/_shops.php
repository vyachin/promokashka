<?php
/**
 * @var yii\web\View $this
 * @var array $shops
 * @var string $title
 */

use common\helpers\Image;
use yii\helpers\Html;

?>
<aside class="card popular-shops">
    <div class="card-header"><?= $title ?></div>
    <div class="card-body popular-shops--inner">
        <?php foreach ($shops as $shop): ?>
            <?= Html::a(
                Html::img(
                    Image::url($shop['logo'], 105, 88),
                    ['alt' => $shop['name'], 'title' => $shop['name']]
                ),
                ['/shop/view', 'slug' => $shop['slug']],
                ['class' => 'popular-shops--item', 'title' => $shop['name']]
            ) ?>
        <?php endforeach; ?>
    </div>
</aside>
