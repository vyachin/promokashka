<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class DescriptionWidget extends Widget
{
    public $text;
    public $length;
    public $options = [];
    public $wrapperOptions = [];

    public function run()
    {
        Html::addCssClass($this->options, 'description');
        Html::addCssClass($this->wrapperOptions, 'wrapper');
        $longText = '';
        if (mb_strlen($this->text, \Yii::$app->charset) > $this->length) {
            Html::addCssClass($this->options, 'less');
            $longText = '<a class="show-full" href="#">Подробнее <span class="glyphicon glyphicon-chevron-down"></span></a><a class="show-less" href="#">Скрыть <span class="glyphicon glyphicon-chevron-up"></span></a>';
        }

        return Html::tag('div', Html::tag('div', $this->text, $this->wrapperOptions).$longText, $this->options);
    }
}