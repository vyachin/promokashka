<?php

namespace frontend\widgets;

use common\models\Offer;
use common\models\Shop;
use yii\base\Widget;
use yii\db\Query;

class NewShops extends Widget
{
    const CACHE_KEY = __CLASS__;
    const CACHE_DURATION = 3600;

    public function run()
    {
        if (!$result = \Yii::$app->cache->get(self::CACHE_KEY)) {
            $subQuery = (new Query())->select('shop_id')->from('{{%offer}}')->andWhere(
                '{{%offer}}.shop_id={{%shop}}.id'
            )->andWhere(['NOT', ['{{%offer}}.status' => Offer::STATUS_DELETED]]);
            $shops = Shop::find()
                ->active()
                ->limit(6)
                ->orderBy(['created_at' => SORT_DESC])
                ->select('{{%shop}}.id, {{%shop}}.slug, {{%shop}}.logo, {{%shop}}.name')
                ->andWhere(['EXISTS', $subQuery])
                ->asArray()
                ->all();

            $result = $this->render('_shops', ['shops' => $shops, 'title' => 'Новые магазины']);
            \Yii::$app->cache->set(self::CACHE_KEY, $result, self::CACHE_DURATION);
        }

        return $result;
    }
}