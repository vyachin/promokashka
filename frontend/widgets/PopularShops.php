<?php

namespace frontend\widgets;

use common\models\Offer;
use common\models\Shop;
use yii\base\Widget;
use yii\db\Query;

class PopularShops extends Widget
{
    const CACHE_KEY = __CLASS__;
    const CACHE_DURATION = 86400;

    public function run()
    {
        $this->getView()->registerJs('$(".popular-stores").PopularShopSlider();');
        $shops = \Yii::$app->cache->getOrSet(self::CACHE_KEY, function () {
            $subQuery = (new Query())->select('shop_id')->from('{{%offer}}')->andWhere(
                '{{%offer}}.shop_id={{%shop}}.id'
            )->andWhere(['NOT', ['{{%offer}}.status' => Offer::STATUS_DELETED]]);

            $ids = (new Query())
                ->select('{{%offer}}.shop_id')
                ->from('{{%offer}}')
                ->innerJoin(
                    '{{%shop}}',
                    '{{%shop}}.id = {{%offer}}.shop_id AND {{%shop}}.status=:status',
                    [':status' => Shop::STATUS_ACTIVE]
                )
                ->andWhere(['EXISTS', $subQuery])
                ->groupBy('{{%offer}}.shop_id')
                ->orderBy('SUM({{%offer}}.count_used) DESC')
                ->limit(12)
                ->column();
            $ids = array_map('intval', $ids);

            return Shop::find()->andWhere(['id' => $ids])->select('id, slug, logo, name')->asArray()->all();
        }, self::CACHE_DURATION);
        return $this->render('_popular_shops', ['shops' => $shops]);
    }
}