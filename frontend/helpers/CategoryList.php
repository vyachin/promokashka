<?php

namespace frontend\helpers;


use common\models\Category;
use common\models\Offer;
use yii\db\Expression;

abstract class CategoryList
{
    public static function getList()
    {
        $offerQuery = Offer::find()
            ->valid()
            ->select(new Expression('1'))
            ->innerJoin(['oc' => '{{%offer_category}}'], Offer::tableName() . '.id=oc.offer_id')
            ->andWhere('oc.category_id=' . Category::tableName() . '.id');
        return Category::find()->active()->orderBy(['name' => SORT_ASC])->andWhere(['EXISTS', $offerQuery])->all();
    }
}