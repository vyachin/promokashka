<?php

namespace frontend\helpers;

use yii\data\BaseDataProvider;
use yii\db\Query;

class OfferUsed
{
    const USED_SHOP_NAMES = 'used_shop_names';

    public static function getToday(BaseDataProvider $dataProvider)
    {
        $cacheKeys = [];
        $keys = $dataProvider->getKeys();
        $prefix = 'todayUsedCounter:'.date('Y-m-d').':';
        foreach ($keys as $id) {
            $cacheKeys[] = $prefix.$id;
        }

        return $keys ? array_combine($keys, \Yii::$app->redis->executeCommand('MGET', $cacheKeys)) : [];
    }

    public static function getShopNames()
    {

        if (!$result = \Yii::$app->cache->get(self::USED_SHOP_NAMES)) {
            $result = (new Query())->select('name')->from('{{%shop}}')->where(
                'id IN (SELECT shop_id FROM {{%offer}} GROUP BY shop_id HAVING SUM(count_used) > 0 ORDER BY SUM(count_used) DESC)'
            )->column();
            \Yii::$app->cache->set(self::USED_SHOP_NAMES, $result, 86400);
        }

        return $result;
    }
}