<?php

namespace frontend\controllers;

use common\helpers\Image;
use common\models\Category;
use common\models\Offer;
use common\models\Post;
use common\models\Seo;
use common\models\Shop;
use common\models\ShopQuery;
use frontend\helpers\OfferUsed;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SiteController extends Controller
{
    public function actions(): array
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $lastOffers = Offer::find()->valid()->groupBy('shop_id')->innerJoinWith(
            [
                'shop' => function (ShopQuery $query) {
                    $query->active();
                },
            ]
        )->select('MAX({{%offer}}.id)')->limit(6)->column();
        $lastOffers = array_map('intval', $lastOffers);
        $dataProvider = new ActiveDataProvider(
            [
                'query' => Offer::find()->with(['shop'])->where(['IN', 'id', $lastOffers]),
                'pagination' => false,
            ]
        );
        $todayUsed = OfferUsed::getToday($dataProvider);
        $seo = Seo::findOne(['page' => Seo::MAIN_PAGE]);

        return $this->render('index', ['dataProvider' => $dataProvider, 'todayUsed' => $todayUsed, 'seo' => $seo,]);
    }

    /**
     * @param null $q
     * @return array
     */
    public function actionSearch($q = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $results = [];
        if ($q) {
            /** @var Offer[] $offers */
            $offers = Offer::find()
                ->valid()
                ->joinWith('shop')
                ->andWhere(new Expression('MATCH({{%offer}}.name, {{%offer}}.description) AGAINST(:q)'), ['q' => $q])
                ->limit(10)
                ->all();

            foreach ($offers as $offer) {
                $results[] = [
                    'url' => Url::to(['shop/view', 'slug' => $offer->shop->slug]),
                    'name' => $offer->shop->name,
                    'image' => Image::url($offer->shop->logo, 74, 45),
                    'description' => StringHelper::truncateWords($offer->description, 20),
                ];
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function actionMap()
    {
        \Yii::$app->response->headers->set('Content-Type', 'application/xml');
        \Yii::$app->response->format = Response::FORMAT_RAW;
        $xml = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
        $xml .= '<url><loc>'.Url::to(['site/index'], true).'</loc><changefreq>daily</changefreq></url>';
        $xml .= '<url><loc>'.Url::to(['post/index'], true).'</loc><changefreq>daily</changefreq></url>';
        /** @var Shop[] $shops */
        $shops = Shop::find()->active()->each();
        foreach ($shops as $shop) {
            $loc = Url::to(['shop/view', 'slug' => $shop->slug], true);
            $xml .= '<url><loc>'.$loc.'</loc><changefreq>daily</changefreq></url>';
        }
        /** @var Category[] $categories */
        $categories = Category::find()->active()->each();
        foreach ($categories as $category) {
            $loc = Url::to(['category/view', 'slug' => $category->slug], true);
            $xml .= '<url><loc>'.$loc.'</loc><changefreq>daily</changefreq></url>';
        }
        /** @var Post[] $posts */
        $posts = Post::find()->published()->each();
        foreach ($posts as $post) {
            $loc = Url::to(['post/view', 'slug' => $post->slug], true);
            $xml .= '<url><loc>'.$loc.'</loc><changefreq>daily</changefreq></url>';
        }
        $xml .= '</urlset>';

        return $xml;
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionOut($id)
    {
        $this->layout = false;
        /** @var Offer $offer */
        $offer = Offer::findOne($id);
        if (!$offer) {
            throw new NotFoundHttpException;
        }
        $offer->incrementTodayCountUsed();

        return $this->render('out', ['url' => $offer->url]);
    }

    /**
     * @param null $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionLike($id = null)
    {
        /** @var Offer $offer */
        $offer = Offer::findOne($id);
        if (!$offer) {
            throw new NotFoundHttpException;
        }
        $offer->incrementLike();

        return $this->asJson($offer->getLikeCounters());
    }

    /**
     * @param null $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDislike($id = null)
    {
        /** @var Offer $offer */
        $offer = Offer::findOne($id);
        if (!$offer) {
            throw new NotFoundHttpException;
        }
        $offer->incrementDislike();

        return $this->asJson($offer->getLikeCounters());
    }
}
