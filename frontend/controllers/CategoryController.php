<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Offer;
use common\models\Seo;
use frontend\helpers\OfferUsed;
use frontend\models\CategoryRate;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CategoryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $categories = Category::find()->active()->select('id, name, slug, icon')->asArray()->all();
        $seo = Seo::findOne(['page' => Seo::CATEGORY_PAGE]);

        return $this->render('index', ['categories' => $categories, 'seo' => $seo]);
    }

    /**
     * @param $slug
     * @param null $page
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug, $page = null)
    {
        /** @var Category $model */
        $model = Category::find()->active()->slug($slug)->one();
        if (!$model) {
            throw new NotFoundHttpException;
        }

        $dataProvider = new ActiveDataProvider(
            [
                'query' => Offer::find()->innerJoin(
                    '{{%offer_category}} oc',
                    'oc.offer_id={{%offer}}.id and oc.category_id=:category_id',
                    [':category_id' => $model->id]
                )->with(['shop'])->valid(),
                'pagination' => [
                    'pageSize' => 6,
                    'defaultPageSize' => 6,
                    'forcePageParam' => false,
                ],
            ]
        );
        $todayUsed = OfferUsed::getToday($dataProvider);

        return $this->render('view', ['model' => $model, 'dataProvider' => $dataProvider, 'todayUsed' => $todayUsed]);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionRate($id)
    {
        $model = new CategoryRate(['id' => $id]);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load(\Yii::$app->request->post(), '') && $model->save()) {
            return [
                'success' => true,
                'rate' => $model->getCategoryRate(),
                'count' => $model->getCategoryRateCount(),
            ];
        }

        return ['success' => false, 'errors' => $model->getErrors()];
    }
}
