<?php

namespace frontend\controllers;

use common\models\Offer;
use common\models\Seo;
use common\models\Shop;
use frontend\helpers\OfferUsed;
use frontend\models\ShopRate;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ShopController extends Controller
{
//    public function behaviors(): array
//    {
//        return [
//            [
//                'class' => \yii\filters\PageCache::class,
//                'only' => ['index'],
//                'duration' => 86400,
//                'dependency' => [
//                    'class' => \yii\caching\ChainedDependency::class,
//                    'dependencies' => [
//                        new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM {{%shop}}']),
//                        new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM {{%seo}}']),
//                    ],
//                ],
//            ],
//        ];
//    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $shops = [];
        $offerStatuses = Offer::getStatusList();
        unset($offerStatuses[Offer::STATUS_DELETED]);

        $subQuery = (new Query())
            ->select('shop_id')
            ->from('{{%offer}}')
            ->andWhere('{{%offer}}.shop_id={{%shop}}.id')
            ->andWhere(['{{%offer}}.status' => array_keys($offerStatuses)]);
        /** @var array $shopList */
        $shopList = Shop::find()->active()->andWhere(['EXISTS', $subQuery])->orderBy(['name' => SORT_ASC])->select(
            'id, name, slug'
        )->asArray()->all();
        foreach ($shopList as $shop) {
            $letter = mb_strtoupper(mb_substr($shop['name'], 0, 1, 'utf-8'), 'utf-8');
            if (is_numeric($letter)) {
                $letter = '0';
            }
            $shops[$letter][] = $shop;
        }

        $seo = Seo::findOne(['page' => Seo::SHOP_PAGE]);

        return $this->render('index', ['shops' => $shops, 'seo' => $seo,]);
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        /** @var Shop $shop */
        $shop = Shop::find()->active()->slug($slug)->one();
        if (!$shop) {
            throw new NotFoundHttpException;
        }
        $validOfferQuery = Offer::find()
            ->with(['shop'])
            ->valid()
            ->andWhere(['shop_id' => $shop->id])
            ->orderBy(['updated_at' => SORT_DESC]);
        $validOfferProvider = new ActiveDataProvider(['query' => $validOfferQuery, 'pagination' => false,]);
        $expiredOfferQuery = Offer::find()
            ->with(['shop'])
            ->limit(5)
            ->old()
            ->andWhere(['shop_id' => $shop->id])
            ->orderBy(['updated_at' => SORT_DESC]);
        $expiredOfferProvider = new ActiveDataProvider(['query' => $expiredOfferQuery, 'pagination' => false,]);
        $todayUsed = OfferUsed::getToday($validOfferProvider);

        return $this->render(
            'view',
            [
                'shop' => $shop,
                'validOfferProvider' => $validOfferProvider,
                'expiredOfferProvider' => $expiredOfferProvider,
                'todayUsed' => $todayUsed,
            ]
        );
    }

    /**
     * @param $id
     * @return array
     */
    public function actionRate($id)
    {
        $model = new ShopRate(['id' => $id]);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load(\Yii::$app->request->post(), '') && $model->save()) {
            return [
                'success' => true,
                'rate' => $model->getShopRate(),
                'count' => $model->getShopRateCount(),
            ];
        }

        return ['success' => false, 'errors' => $model->getErrors()];
    }
}
