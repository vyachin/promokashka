<?php

namespace frontend\controllers;

use common\models\Post;
use common\models\Seo;
use frontend\models\PostRate;
use frontend\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class PostController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->getQueryParams());
        $seo = Seo::findOne(['page' => Seo::BLOG_PAGE]);

        return $this->render('index', ['dataProvider' => $dataProvider, 'seo' => $seo]);
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        /** @var Post $model */
        $model = Post::find()->published()->slug($slug)->one();
        if (!$model) {
            throw new NotFoundHttpException;
        }
        $seo = Seo::findOne(['page' => Seo::BLOG_PAGE]);

        return $this->render('view', ['model' => $model, 'seo' => $seo]);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionRate($id)
    {
        $model = new PostRate(['id' => $id]);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load(\Yii::$app->request->post(), '') && $model->save()) {
            return [
                'success' => true,
                'rate' => $model->getPostRate(),
                'count' => $model->getPostRateCount(),
            ];
        }

        return ['success' => false, 'errors' => $model->getErrors()];
    }
}
