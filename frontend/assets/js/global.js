jQuery(document).ready(function () {
  'use strict'
  var $ = jQuery

    $('.description').on('click', '.show-full, .show-less', function () {
      $(this).parents('.description').toggleClass('less')
      return false
    })
    $('.js-like').click(function () {
      var self = $(this)
      $.post(self.data('url'))
      if (window.ga) ga('send', 'event', 'like', self.data('id'))
      return false
    })
    $('.js-dislike').click(function () {
      var self = $(this)
      $.post(self.data('url'))
      if (window.ga) ga('send', 'event', 'dislike', self.data('id'))
      return false
    })
    $('.js-go-store').click(function () {
      if (window.ga) ga('send', 'event', 'GoShop', $(this).attr('href'))
    })
    $('.code-text').click(function () {
        var doc = document, text = this, range, selection;
        if (doc.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(text);
            range.select();
        } else if (window.getSelection) {
            selection = window.getSelection();
            range = document.createRange();
            range.selectNodeContents(text);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    });

    $('.modal .js-desc-toggle').click(function () {
        var self = $(this);
        self.find('span').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
        self.parents('.modal').find('.js-desc').toggleClass('hide');
        return false;
    });

    $('.coupon').each(function () {
        var modal = $(this).find('.modal');
        if (modal) {
            var id = modal.attr('id');
            $(this).find('.coupon-button').click(function () {
                var url = $(this).data('aff-url');
                window.open(url, '_self');
                window.open('#' + id, '_blank');
                modal.modal('show');
                return false;
            });
            if (location.hash == '#' + id) {
                modal.modal('show');
            }
        }
    });

    function templater(html) {
        return function (data) {
            var result = html;
            for (var x in data) {
                if (data.hasOwnProperty(x)) {
                    var re = "{{\\s?" + x + "\\s?}}";
                    result = result.replace(new RegExp(re, "ig"), data[x]);
                }
            }
            return result;
        };
    }

    var source = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: "/search?q=%QUERY",
            wildcard: "%QUERY"
        }
    });
    $("#js-search").typeahead(null, {
        name: "search",
        source: source,
        limit: 25,
        display: 'name',
        templates: {
            empty: '<div class="empty-message">Промокоды не найдены</div>',
            suggestion: templater('<div><div class="frame"><img src="{{image}}" /></div><div><p class="h4">{{name}}</p><p><small>{{description}}</small></p></div><div class="clearfix"></div></div>')
        }
    }).bind('typeahead:select', function (ev, suggestion) {
        if (suggestion.url) {
            window.location.href = suggestion.url;
        }
    });
});

(function ($) {
    $.fn.PopularShopSlider = function () {
        var wrapper = this.find('.js-store-carousel');
        var first = wrapper.find('.popular-stores--item:first');
        var width = first.width();
        var current = 0;
        var visible = 12 * width - this.width();
        setInterval(function () {
            current += width;
            if (current > visible) current = 0;
            wrapper.animate({
                transform: "translateX(-" + current + "px)"
            });
        }, 2000);
    };
})(jQuery);
