(function ($) {
    var methods = {
        init: function (options) {
            this.on("click", ".star.rating", function () {
                var self = $(this);
                var rating = self.data('rating');
                self.parent().attr('data-stars', rating);
                var data = {
                    rate: rating
                };
                data[methods.getCsrfParam()] = methods.getCsrfToken();
                $.post(options.url, data, function (e) {
                    if (e.success) {
                        $('.js-rate-count').text(e.count);
                        window.ga && ga('send', 'event', 'rating', options.url);
                    }
                });
            });
        },
        getCsrfParam: function () {
            return $('meta[name=csrf-param]').attr('content');
        },
        getCsrfToken: function () {
            return $('meta[name=csrf-token]').attr('content');
        }
    };

    $.fn.StarRating = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для StarRating');
        }
    };
})(jQuery);
