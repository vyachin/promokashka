<?php

namespace frontend\models;


use common\models\Post;
use yii\data\ActiveDataProvider;

class PostSearch extends Post
{
    public function rules(): array
    {
        return [];
    }

    public function search($params = [])
    {
        $query = static::find()->published();
        if ($this->load($params) && $this->validate()) {

        }

        return new ActiveDataProvider(
            [
                'pagination' => ['defaultPageSize' => 9],
                'query' => $query,
                'sort' => ['defaultOrder' => ['published_at' => SORT_DESC]],
            ]
        );
    }
}