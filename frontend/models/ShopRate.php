<?php

namespace frontend\models;

use common\models\Shop;
use yii\base\Model;

class ShopRate extends Model
{
    public $rate;
    public $id;

    public function rules(): array
    {
        return [
            [['rate', 'id'], 'required'],
            ['rate', 'integer', 'min' => 1, 'max' => 5],
            ['id', 'exist', 'targetClass' => Shop::class, 'targetAttribute' => 'id'],
        ];
    }

    public function save(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        return (bool)Shop::updateAllCounters(
            ['rate_sum' => $this->rate, 'rate_count' => 1],
            ['id' => $this->id, 'status' => Shop::STATUS_ACTIVE]
        );
    }

    public function getShopRate()
    {
        return $this->getShop()->getRate();
    }

    /**
     * @return Shop
     */
    private function getShop()
    {
        static $shop = null;
        if (!$shop) {
            $shop = Shop::find()->active()->andWhere(['id' => $this->id])->one();
        }

        return $shop;
    }

    public function getShopRateCount()
    {
        return $this->getShop()->getFormattedRateCount();
    }
}