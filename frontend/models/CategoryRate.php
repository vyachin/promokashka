<?php

namespace frontend\models;

use common\models\Category;
use yii\base\Model;

class CategoryRate extends Model
{
    public $rate;
    public $id;

    public function rules(): array
    {
        return [
            [['rate', 'id'], 'required'],
            ['rate', 'integer', 'min' => 1, 'max' => 5],
            ['id', 'exist', 'targetClass' => Category::class, 'targetAttribute' => 'id'],
        ];
    }

    public function save(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        return (bool)Category::updateAllCounters(
            ['rate_sum' => $this->rate, 'rate_count' => 1],
            ['id' => $this->id, 'status' => Category::STATUS_ACTIVE]
        );
    }

    public function getCategoryRate()
    {
        return $this->getCategory()->getRate();
    }

    /**
     * @return Category
     */
    private function getCategory()
    {
        static $category = null;
        if (!$category) {
            /** @var Category $category */
            $category = Category::find()->active()->andWhere(['id' => $this->id])->one();
        }

        return $category;
    }

    public function getCategoryRateCount()
    {
        return $this->getCategory()->getFormattedRateCount();
    }
}