<?php

namespace frontend\models;

use common\models\Post;
use yii\base\Model;

class PostRate extends Model
{
    public $rate;
    public $id;

    public function rules(): array
    {
        return [
            [['rate', 'id'], 'required'],
            ['rate', 'integer', 'min' => 1, 'max' => 5],
            ['id', 'exist', 'targetClass' => Post::class, 'targetAttribute' => 'id'],
        ];
    }

    public function save(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        return (bool)Post::updateAllCounters(
            ['rate_sum' => $this->rate, 'rate_count' => 1],
            ['id' => $this->id, 'status' => Post::STATUS_PUBLISHED]
        );
    }

    public function getPostRate()
    {
        return $this->getPost()->getRate();
    }

    /**
     * @return Post
     */
    private function getPost()
    {
        /** @var Post $post */
        static $post = null;
        if (!$post) {
            $post = Post::find()->published()->andWhere(['id' => $this->id])->one();
        }

        return $post;
    }

    public function getPostRateCount()
    {
        return $this->getPost()->getFormattedRateCount();
    }
}