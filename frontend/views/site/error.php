<?php
/**
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */

use common\helpers\Image;
use frontend\widgets\PopularShops;
use yii\helpers\Html;

if ($exception instanceof yii\web\NotFoundHttpException){
    $this->title = 'Ошибка 404';
    $image = Yii::$app->params['error_404'];
} else {
    $this->title = 'Ошибка 503';
    $image = Yii::$app->params['error_503'];
    $message = '';
}
?>

<div class="row">
    <main class="col-md-9">
        <div class="page-title"><h1><?=$this->title ?> <small><?=$message ?></small></h1></div>
        <?= Html::img(
            Image::url($image, 600, 400),
            ['class' => 'img-responsive', 'alt' => 'error', 'title' => 'error']
        ) ?>
    </main>
    <div class="clearfix"></div>
    <div>
        <?= PopularShops::widget() ?>
    </div>
</div>
