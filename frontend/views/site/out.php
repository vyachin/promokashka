<?php
/**
 * @var $this \yii\web\View
 * @var $url string
 */
?>
<html>
<head>
    <meta refresh="1;<?= $url ?>">
    <meta http-equiv="refresh" content="1, URL=<?= $url ?>">
</head>
<script type="application/javascript">window.location = "<?=$url ?>"</script>
</html>
