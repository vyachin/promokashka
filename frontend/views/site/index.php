<?php
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var int[] $todayUsed
 * @var $seo \common\models\Seo
 */

use frontend\widgets\PopularShops;
use yii\widgets\ListView;

if ($seo) {
    $this->title = $seo->seo_title;
    $this->params['description'] = $seo->seo_description;
    $this->params['keywords'] = $seo->seo_keywords;
} else {
    $this->title = 'Все промокоды на скидки в одном месте';
    $this->params['description'] = 'Возьми бесплатный промокод и получи скидку до 70% в интернет магазинах · Wildberries · Babadu · Lamoda · Sapato · Юлмарт · KupiVIP · МВидео · Roxy';
}
$this->params['image'] = \Yii::$app->params['logo'];
?>
<h1>Промокоды для популярных онлайн-магазинов</h1>
<?= PopularShops::widget() ?>
<div class="row">
    <main class="col-lg-17 col-xl-18">
        <h2 class="section-heading">Новые промокоды</h2>
        <?= ListView::widget(
            [
                'dataProvider' => $dataProvider,
                'itemView' => '@frontend/views/offer/_offer',
                'viewParams' => ['todayUsed' => $todayUsed],
                'options' => [
                    'class' => 'coupon-list',
                ],
                'itemOptions' => [
                    'class' => 'card coupon',
                ],
                'summary' => false,
            ]
        ) ?>
    </main>
    <div class="col-lg-7 col-xl-6">
        <?= \frontend\widgets\NewShops::widget() ?>
    </div>
</div>

