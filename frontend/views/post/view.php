<?php
/**
 * @var yii\web\View $this
 * @var common\models\Post $model
 * @var \common\models\Seo $seo
 */

use frontend\widgets\RateWidget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

$this->title = $model->seo_title;
$this->params['description'] = $model->seo_description;
$this->params['keywords'] = $model->seo_keywords;
if ($model->image) {
    $this->params['image'] = $model->image;
}
$this->params['breadcrumbs'][] = ['label' => 'Блог', 'url' => ['post/index']];
$this->params['breadcrumbs'][] = $model->title;
$params = Json::encode(['url' => Url::to(['post/rate', 'id' => $model->id])]);
$this->registerJs("$('#js-post-rating').StarRating({$params});");

?>
<div itemscope itemtype="http://schema.org/Article">
    <h1 itemprop="name"><?= Html::encode($model->title) ?></h1>
    <div class="aggregateRating category">
        <?= RateWidget::widget(['value' => $model->getRate(), 'id' => 'js-post-rating']) ?>
        <div>
            <span class="svg--user svg-icon" data-grunticon-embed></span>
            <span class="js-rate-count"><?= $model->getFormattedRateCount() ?></span>
        </div>
    </div>
    <div itemprop="description">
        <?= str_replace(\common\models\Post::DELIMITER, '', $model->body) ?>
    </div>
    <meta content="<?= Yii::$app->formatter->asDatetime($model->published_at, 'php:c') ?>" itemprop="datePublished"/>
    <meta content="<?= Yii::$app->formatter->asDatetime($model->updated_at, 'php:c') ?>" itemprop="dateModified"/>
    <div class="aggregateRating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        <meta content="<?= $model->rate_count ?>" itemprop="reviewCount"/>
        <meta content="<?= $model->getRate() ?>" itemprop="ratingValue"/>
    </div>
</div>