<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\Post
 */


if (($pos = strpos($model->body, \common\models\Post::DELIMITER)) !== false) {
    $body = \yii\helpers\HtmlPurifier::process(substr($model->body, 0, $pos));
} else {
    $body = $model->body;
}

use yii\helpers\Html; ?>
<div class="m-2">
    <div class="text-center">
        <?= Html::a(
            Html::img(
                \common\helpers\Image::url($model->image, 320, 180),
                ['width' => 320, 'height' => 180,]
            ),
            ['post/view', 'slug' => $model->slug]
        ) ?>
    </div>
    <h5><?= Html::a(Html::encode($model->title), ['post/view', 'slug' => $model->slug]) ?></h5>
    <ul class="list-inline">
        <li class="list-inline-item"><?= Yii::$app->formatter->asDate($model->published_at, 'dd MMMM yyyy') ?></li>
        <li class="list-inline-item"><?= Html::encode($model->user->username) ?></li>
    </ul>
</div>
