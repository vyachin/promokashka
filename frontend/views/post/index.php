<?php
/**
 * @var yii\web\View $this
 * @var $seo \common\models\Seo
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
$this->title = $seo ? $seo->seo_title : 'Блог';
if ($seo) {
    $this->params['description'] = $seo->seo_description;
    $this->params['keywords'] = $seo->seo_keywords;
}
$this->params['image'] = \Yii::$app->params['logo'];
$this->params['breadcrumbs'][] = 'Блог';
?>
    <h1>Блог</h1>
<?= \yii\widgets\ListView::widget(
    [
        'itemView' => '_item',
        'dataProvider' => $dataProvider,
        'layout' => "{items}\n{pager}",
        'itemOptions' => ['class' => 'col-md-6 d-flex align-items-stretch'],
        'options' => ['class' => 'row'],
        'pager' => [
            'pageCssClass' => 'page-item',
            'disabledPageCssClass' => 'page-item disabled',
            'disabledListItemSubTagOptions' => ['class' => 'page-link'],
            'registerLinkTags' => true,
            'linkOptions' => ['class' => 'page-link'],
            'options' => ['class' => 'col-md-12 pagination'],
        ],
    ]
);