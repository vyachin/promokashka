<?php
/**
 * @var yii\web\View $this
 * @var common\models\Offer $model
 */

use frontend\widgets\DescriptionWidget;

?>
<div class="card-body coupon-body">
    <div class="coupon-description" itemscope itemtype="http://schema.org/Thing">
        <h4 itemprop="name"><?= $model->name ?></h4>
        <div class="coupon-info">
            <span class="label label-primary type-<?= $model->type ?>"><?= Yii::$app->formatter->asOfferTypeName($model->type) ?></span>
            Истекает <?= Yii::$app->formatter->asDate($model->end_at, 'php:d/m/Y') ?>
        </div>
        <?= DescriptionWidget::widget(['text' => $model->description, 'length'=>300, 'options' => ['itemprop' => 'description']]) ?>
    </div>
</div>
<div class="card-footer">
    <div class="coupon-stat">
        <div class="coupon-stat--used">
            <span class="svg--signal svg-icon" data-grunticon-embed></span> <?= $model->count_used ?> использовался
        </div>
        <div class="coupon-stat--success">
            <?= $model->getSuccessFormatterPercents() ?> успешно
        </div>
    </div>
</div>
