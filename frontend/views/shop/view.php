<?php
/**
 * @var yii\web\View $this
 * @var common\models\Shop $shop
 * @var yii\data\ActiveDataProvider $validOfferProvider
 * @var yii\data\ActiveDataProvider $expiredOfferProvider
 * @var int[] $todayUsed
 */

use common\helpers\Image;
use frontend\widgets\NextShops;
use frontend\widgets\RateWidget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ListView;

if ($shop->seo_title) {
    $this->title = $shop->seo_title;
} else {
    $this->title = "Промокоды {$shop->name}";
}
if ($shop->seo_description) {
    $this->params['description'] = $shop->seo_description;
} else {
    $this->params['description'] = "Только действующие промокоды в интернет-магазин {$shop->name} ➤ скидки до ㊿% ➤ подарки ➤ бесплатная доставка";
}
if ($shop->seo_keywords) {
    $this->params['keywords'] = $shop->seo_keywords;
}
$this->params['image'] = $shop->logo;
$this->params['breadcrumbs'][] = ['label' => 'Все магазины', 'url' => ['shop/index']];
$this->params['breadcrumbs'][] = $shop->name;
$params = Json::encode(['url' => Url::to(['shop/rate', 'id' => $shop->id])]);
$this->registerJs("$('#js-shop-rating').StarRating({$params});");
$this->registerJsFile('//yastatic.net/es5-shims/0.0.2/es5-shims.min.js');
$this->registerJsFile('//yastatic.net/share2/share.js');
?>
<div class="row">
    <main class="col-lg-17 col-xl-18" itemscope itemtype="http://schema.org/Article">
        <h1 itemprop="name">Действующие промокоды <?= $shop->name ?></h1>
        <?= ListView::widget(
            [
                'dataProvider' => $validOfferProvider,
                'itemView' => '_offer',
                'viewParams' => ['todayUsed' => $todayUsed],
                'pager' => [
                    'options' => ['class' => 'pagination'],
                ],
                'options' => [
                    'class' => 'shop-coupon-list',
                ],
                'itemOptions' => [
                    'class' => 'card coupon',
                ],
                'summary' => false,
                'emptyText' => 'Действующие промокоды не найдены',
            ]
        ) ?>
        <?php if ($expiredOfferProvider->count) : ?>
            <h2>Промокоды, которые Вы пропустили</h2>
            <?= ListView::widget(
                [
                    'dataProvider' => $expiredOfferProvider,
                    'itemView' => '_offer_expired',
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                    ],
                    'options' => [
                        'class' => 'shop-coupon-list',
                    ],
                    'itemOptions' => [
                        'class' => 'card card-default coupon-expired coupon',
                    ],
                    'summary' => false,
                ]
            ) ?>
        <?php endif ?>
        <div itemprop="description"><?= nl2br($shop->description) ?></div>
        <meta content="<?= Yii::$app->formatter->asDatetime($shop->created_at, 'php:c') ?>" itemprop="datePublished"/>
        <meta content="<?= Yii::$app->formatter->asDatetime($shop->updated_at, 'php:c') ?>" itemprop="dateModified"/>
        <div class="aggregateRating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
            <meta content="<?= $shop->rate_count ?>" itemprop="reviewCount"/>
            <meta content="<?= $shop->getRate() ?>" itemprop="ratingValue"/>
        </div>
    </main>
    <div class="col-lg-7 col-xl-6">
        <div class="image text-center">
            <div class="frame">
                <?= Html::img(
                    $imageUrl = Image::url($shop->logo, 240, 180),
                    ['alt' => $shop->name, 'title' => $shop->name, 'itemprop' => 'image', 'class' => 'img-fluid']
                ) ?>
            </div>
            <div class="aggregateRating category">
                <?= RateWidget::widget(['value' => $shop->getRate(), 'id' => 'js-shop-rating']) ?>
                <div>
                    <span class="svg--user svg-icon" data-grunticon-embed></span>
                    <span class="js-rate-count"><?= $shop->getFormattedRateCount() ?></span>
                </div>
            </div>
            <div class="ya-share2"
                 data-services="vkontakte,facebook,odnoklassniki,twitter,viber,whatsapp,skype,telegram"></div>
        </div>
        <?= NextShops::widget(['shopId' => $shop->id]) ?>
    </div>
</div>
