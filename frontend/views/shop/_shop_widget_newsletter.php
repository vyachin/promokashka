<?php
/**
 * @var yii\web\View $this
 */
?>
<aside class="widget widget_newsletter">
    <h4 class="widget-title">Nature Coupon Into your inbox</h4>
    <div class="newsletter-box-wrapper shadow-box">
        <form id="newsletter-box-widget">
            <div class="ui action left icon input">
                <input placeholder="Enter your email ..." type="text">
                <i class="mail outline icon"></i>
                <div class="ui button">Subscribe</div>
            </div>
            <div class="clear"></div>
            <div class="newsletter-text">You can opt out of our newsletters at any time. See our <a href="#">privacy
                    policy</a>.
            </div>
        </form>
    </div>
</aside>
