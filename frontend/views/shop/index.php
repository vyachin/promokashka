<?php
/**
 * @var yii\web\View $this
 * @var array $shops
 * @var $seo \common\models\Seo
 */

use frontend\helpers\OfferUsed;
use yii\helpers\Html;

if ($seo) {
    $this->title = $seo->seo_title;
    $this->params['description'] = $seo->seo_description;
    $this->params['keywords'] = $seo->seo_keywords;
} else {
    $this->title = 'Все магазины';
    $this->params['description'] = 'Промокоды ';
    $length = 0;
    foreach (OfferUsed::getShopNames() as $name) {
        $this->params['description'] .= '· '.$name.' ';
        $length += 3 + mb_strlen($name);
        if ($length >= 155) {
            break;
        }
    }
}

$this->params['image'] = \Yii::$app->params['logo'];
$this->params['breadcrumbs'][] = 'Все магазины';
?>
<h1>Все магазины</h1>
<div class="shop-index">
    <?php foreach($shops as $label => $list): ?>
        <a class="shop-index--item" href="#<?= $label ?>">
            <?= $label == '0' ? '0 - 9' : $label ?>
        </a>
    <?php endforeach ?>
</div>

<div class="row">
    <div class="col-md-10 col-sm-8 col-xs-12">
        <?php foreach($shops as $label => $list): ?>
            <a id="<?= $label ?>"></a>
            <h2>Магазины <?= $label === '0' ? '0 - 9' : $label ?></h2>
            <ul class="list-inline">
                <?php foreach($list as $item): ?>
                    <li><?= Html::a($item['name'], ['shop/view', 'slug' => $item['slug']],
                            ['title' => $item['name']]) ?></li>
                <?php endforeach ?>
            </ul>
        <?php endforeach ?>
    </div>
</div>
