<?php
/**
 * @var yii\web\View $this
 */
?>
<aside class="widget">
    <h4 class="widget-title">Submit Nature Coupon</h4>
    <div class="widget-content shadow-box">
        <div class="ui form">
            <div class="field">
                <input type="text" placeholder="Nature.com" disabled="disabled">
            </div>
            <div class="field">
                <select class="ui dropdown">
                    <option value="">Offer Type</option>
                    <option value="1">Coupon Code</option>
                    <option value="2">Sale</option>
                    <option value="3">Printable</option>
                </select>
            </div>
            <div class="field">
                <label>Add code or change offer type</label>
                <input type="text" placeholder="Code">
            </div>
            <div class="field">
                <input type="text" placeholder="Offer Title">
            </div>
            <div class="field ui icon input">
                <input type="text" placeholder="Exp Date : dd/mm/yyyy">
                <i class="calendar outline icon"></i>
            </div>
            <div class="inline field">
                <div class="ui checkbox">
                    <input type="checkbox">
                    <label>Don't know the expiration date.</label>
                </div>
            </div>
            <div class="field">
                <textarea placeholder="Offer Description"></textarea>
            </div>
            <button class="fluid ui button btn btn_primary">Submit</button>
        </div>
    </div>
</aside>
