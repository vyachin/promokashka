<?php
/**
 * @var yii\web\View $this
 * @var common\models\Offer $model
 * @var int[] $todayUsed
 */

use common\helpers\Image;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div role="dialog" tabindex="-1" id="coupon<?= $model->id ?>" class="modal fade">
    <div role="document" class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close " type="button"><span
                        aria-hidden="true">×</span></button>
                <div class="modal-image">
                    <?= Html::img(
                        Image::url($model->shop->logo, 87, 50),
                        ['alt' => $model->shop->name, 'title' => $model->shop->name]
                    ) ?>
                </div>
                <div class="modal-title">
                    <div class="h3 coupon-name"><?= Html::encode($model->name) ?></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-24">
                        <p class="h4 text-center">Скопируйте этот код и используйте в момент оплаты</p>
                        <div class="modal-code"><span class="code-text"><?= $model->promo_code ?></span></div>
                        <div class="clearfix">
                            <div class="float-left">
                                <?= Html::a(
                                    '<span class="svg--thumbs-up svg-icon" data-grunticon-embed></span>',
                                    '#',
                                    [
                                        'class' => 'btn btn-info btn-sm js-like',
                                        'data' => [
                                            'id' => $model->id,
                                            'url' => Url::to(['site/like', 'id' => $model->id]),
                                        ],
                                        'title' => 'Понравилось',
                                    ]
                                ) ?>
                                <?= Html::a(
                                    '<span class="svg--thumbs-down svg-icon" data-grunticon-embed></span>',
                                    '#',
                                    [
                                        'class' => 'btn btn-info btn-sm js-dislike',
                                        'data' => [
                                            'id' => $model->id,
                                            'url' => Url::to(['site/dislike', 'id' => $model->id]),
                                        ],
                                        'title' => 'Не понравилось',
                                    ]
                                ) ?>
                            </div>
                            <div class="float-right">
                                <?= Html::a(
                                    'Перейти в магазин <span class="svg--chevron-right svg-icon" data-grunticon-embed></span>',
                                    ['site/out', 'id' => $model->id],
                                    [
                                        'class' => 'btn btn-warning js-go-store',
                                        'target' => '_blank',
                                        'title' => 'Перейти в магазин '.$model->shop->name,
                                        'rel' => 'nofollow',
                                    ]
                                ) ?>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="float-left"> Получилось ?</div>
                            <div class="float-right">
                                <a href="#" class="css-desc-toggle js-desc-toggle">
                                    Описание <span class="svg--chevron-down svg-icon" data-grunticon-embed></span>
                                </a>
                            </div>
                        </div>
                        <div class="js-desc hide modal-offer-description">
                            <p><?= Html::encode($model->description) ?></p>
                            <p></p>
                            <p><strong>Истекает</strong>: <?= Yii::$app->formatter->asDate(
                                    $model->end_at,
                                    'php:d/m/Y'
                                ) ?></p>
                            <p><strong>Добавлен</strong>: <?= Yii::$app->formatter->asRelativeTime(
                                    $model->created_at
                                ) ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="coupon-stat">
                    <div class="coupon-stat--used">
                        <span class="svg--signal svg-icon" data-grunticon-embed></span> <?= $model->count_used ?>
                        использовался (сегодня: <?= isset($todayUsed[$model->id]) ? $todayUsed[$model->id] : 0 ?>)
                    </div>
                    <div class="coupon-stat--success"><?= $model->getSuccessFormatterPercents() ?>
                        успешно
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>