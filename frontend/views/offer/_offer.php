<?php
/**
 * @var yii\web\View $this
 * @var common\models\Offer $model
 * @var int[] $todayUsed
 */

use common\helpers\Image;
use frontend\widgets\DescriptionWidget;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="card-body coupon-body">

        <?= Html::a(
            Html::img(
                Image::url($model->shop->logo, 146, 82),
                ['alt' => $model->shop->name, 'title' => $model->shop->name]
            ) .
            '<span class="coupon--name"> Промокоды <br>' . $model->shop->name.' </span>',
            ['shop/view', 'slug' => $model->shop->slug],
            ['class' => 'coupon--link', 'title' => 'Промокоды '.$model->shop->name]
        ) ?>

    <div class="coupon-description" itemscope itemtype="http://schema.org/Thing">
        <h4 itemprop="name"><?= $model->name ?></h4>
        <div class="coupon-info">
            <span class="label label-primary type-<?= $model->type ?>"><?= Yii::$app->formatter->asOfferTypeName($model->type) ?></span>
            Истекает <?= Yii::$app->formatter->asDate($model->end_at, 'php:d/m/Y') ?>
        </div>
        <?= DescriptionWidget::widget(
            ['text' => $model->description, 'length' => 80, 'options' => ['itemprop' => 'description']]
        ) ?>
    </div>

    <div class="coupon-code">
        <a class="coupon-button" href="#coupon<?= $model->id ?>"
           data-aff-url="<?= Url::to(['site/out', 'id' => $model->id]) ?>">
            <span class="code-text"><?= $model->promo_code ?></span>
            <span class="get-code">Показать код</span>
        </a>
        <div class="flex justify-content-center">
            <?= Html::a(
                '<span class="svg--thumbs-up svg-icon" data-grunticon-embed></span>',
                '#',
                [
                    'class' => 'css-like js-like',
                    'data' => ['id' => $model->id, 'url' => Url::to(['site/like', 'id' => $model->id])],
                    'title' => 'Понравилось',
                ]
            ) ?>
            <?= Html::a(
                '<span class="svg--thumbs-down svg-icon" data-grunticon-embed></span>',
                '#',
                [
                    'class' => 'css-dislike js-dislike',
                    'data' => ['id' => $model->id, 'url' => Url::to(['site/dislike', 'id' => $model->id])],
                    'title' => 'Не понравилось',
                ]
            ) ?>
        </div>
    </div>
</div>
<div class="card-footer">
    <div class="coupon-stat">
        <div class="coupon-stat--used">
            <span class="svg--signal svg-icon" data-grunticon-embed></span> <?= $model->count_used ?> использовался
            (сегодня: <?= isset($todayUsed[$model->id]) ? $todayUsed[$model->id] : 0 ?>)
        </div>
        <div class="coupon-stat--success">
            <?= $model->getSuccessFormatterPercents() ?> успешно
        </div>
    </div>
</div>
<?= $this->render('_offer_modal', ['model' => $model, 'todayUsed' => $todayUsed]) ?>
