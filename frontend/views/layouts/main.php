<?php
/**
 * @var string $content
 * @var yii\web\View $this
 */

use common\helpers\AssetHelper;
use common\helpers\Image;
use frontend\widgets\Breadcrumbs;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;

if (isset($this->params['image'])) {
    $image = Image::url($this->params['image'], 968, 504, true);
} else {
    $image = null;
}

$canonicalUrl = Url::canonical();

$this->registerLinkTag(['href' => AssetHelper::getUrl('img/favicon.ico'), 'rel' => 'icon']);
$this->registerLinkTag(['href' => $canonicalUrl, 'rel' => 'canonical']);
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']);
$this->registerMetaTag(['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge']);
$this->registerMetaTag(['charset' => Yii::$app->charset]);
if (isset($this->params['description'])) {
    $this->registerMetaTag(['name' => 'description', 'content' => $this->params['description']]);
}
if (isset($this->params['keywords'])) {
    $this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['keywords']]);
}
// open graph
$this->registerMetaTag(['property' => 'og:type', 'content' => 'website']);
$this->registerMetaTag(['property' => 'og:site_name', 'content' => Yii::$app->name]);
$this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
if (isset($this->params['description'])) {
    $this->registerMetaTag(['property' => 'og:description', 'content' => $this->params['description']]);
}
$this->registerMetaTag(['property' => 'og:url', 'content' => $canonicalUrl]);
$this->registerMetaTag(['property' => 'og:locale', 'content' => Yii::$app->language]);
if ($image) {
    $this->registerMetaTag(['property' => 'og:image', 'content' => $image,]);
    $this->registerMetaTag(['property' => 'og:image:width', 'content' => '968']);
    $this->registerMetaTag(['property' => 'og:image:height', 'content' => '504']);
}
// twitter
$this->registerMetaTag(['name' => 'twitter:card', 'content' => 'summary']);
$this->registerMetaTag(['name' => 'twitter:site', 'content' => '@promokashkacom']);
$this->registerMetaTag(['name' => 'twitter:title', 'content' => $this->title]);
if (isset($this->params['description'])) {
    $this->registerMetaTag(['name' => 'twitter:description', 'content' => $this->params['description']]);
}
if ($image) {
    $this->registerMetaTag(['name' => 'twitter:image', 'content' => $image,]);
}
$this->registerJs(AssetHelper::getContent('icons/grunticon.loader.js'), View::POS_HEAD);
$this->registerJs(
    'grunticon(["/assets/css/icons.data.svg.css", "/assets/css/icons.data.png.css", "/assets/css/icons.fallback.css"], grunticon.svgLoadedCallback);',
    View::POS_HEAD
);
$this->registerJs(
    'var config='.Json::encode(
        [
            'search_url' => Url::to(['site/search', 'q' => '']).'{query}',
        ]
    ).';',
    View::POS_HEAD
);
$this->registerJsFile(AssetHelper::getUrl('js/script.min.js'));
$this->registerCssFile(AssetHelper::getUrl('css/style.min.css'));
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?php if (!YII_DEBUG): ?>
        <?= \common\widgets\RollbarWidger::widget(); ?>
        <?= \common\widgets\YandexMetrika::widget(); ?>
        <?= \common\widgets\GoogleAnalytics::widget(); ?>
    <?php endif; ?>
    <title><?= $this->title ?></title>
    <meta name="yandex-verification" content="9833a82bfdd574d7"/>
    <?php /*sape*/ ?>
    <meta name="yandex-verification" content="3df67dfdaac8891b"/>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php NavBar::begin(
    ['brandLabel' => Yii::$app->name, 'options' => ['class' => 'navbar navbar-expand-lg navbar-dark bg-primary']]
); ?>
<form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Найти промокод..."
           aria-label="Найти промокод..." id="js-search">
</form>
<?php echo Nav::widget(
    [
        'items' => [
            ['label' => 'Главная', 'url' => ['/site/index']],
            ['label' => 'Магазины', 'url' => ['/shop/index']],
            ['label' => 'Блог', 'url' => ['/post/index']],
            [
                'label' => 'Категории',
                'items' => array_map(
                    function (\common\models\Category $category) {
                        return ['label' => $category->name, 'url' => ['category/view', 'slug' => $category->slug]];
                    },
                    \frontend\helpers\CategoryList::getList()
                ),
            ],
        ],
        'options' => ['class' => 'navbar-nav ml-auto'],
    ]
); ?>
<?php NavBar::end(); ?>

<div class="container">
    <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs'] ?? []]) ?>
    <?= $content ?>
</div>

<footer itemscope itemtype="http://schema.org/Organization">
    <div class="container">
        <div class="col-sm-24">
            <h4>Мы в социальных сетях</h4>
            <ul class="social-menu">
                <li><a itemprop="sameAs" href="https://vk.com/promokashka_com" target="_blank" rel="noreferrer">ВКонтакте</a>
                </li>
                <li><a itemprop="sameAs" href="https://www.facebook.com/promokashacom" target="_blank" rel="noreferrer">Facebook</a>
                </li>
                <li><a itemprop="sameAs" href="https://twitter.com/promokashkacom" target="_blank" rel="noreferrer">Twitter</a>
                </li>
            </ul>
        </div>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

