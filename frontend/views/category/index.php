<?php
/**
 * @var yii\web\View $this
 * @var array $categories
 * @var $seo \common\models\Seo
 */

use common\helpers\Image;
use yii\helpers\Html;

if ($seo) {
    $this->title = $seo->seo_title;
    $this->params['description'] = $seo->seo_description;
    $this->params['keywords'] = $seo->seo_keywords;
} else {
    $this->title = 'Все категории';
    $this->params['description'] = '';
    $length = 0;
    foreach ($categories as $category) {
        $this->params['description'] .= '· '.$category['name'].' ';
        $length += 3 + mb_strlen($category['name']);
        if ($length >= 155) {
            break;
        }
    }
}
$this->params['image'] = \Yii::$app->params['logo'];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1>Все категории</h1>
<div class="row">
    <div class="page-header"></div>
    <ul class="list-inline category-list" style="list-style-type: none; padding-left: 0">
        <?php foreach ($categories as $category): ?>
            <li style="text-transform: uppercase;">
                <?= Html::a(
                    Html::img(
                        Image::url($category['icon'], 50, 50),
                        ['alt' => $category['name'], 'title' => $category['name'], 'style' => 'margin-right: 10px']
                    ).' '.$category['name'],
                    ['category/view', 'slug' => $category['slug']],
                    ['title' => $category['name']]
                ) ?>
            </li>
        <?php endforeach ?>
    </ul>
</div>