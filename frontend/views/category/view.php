<?php
/**
 * @var yii\web\View $this
 * @var common\models\Category $model
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var int[] $todayUsed
 */

use common\helpers\Image;
use frontend\widgets\NewShops;
use frontend\widgets\RateWidget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ListView;

if ($model->seo_title) {
    $this->title = $model->seo_title;
} else {
    $this->title = "Промокоды на {$model->name}";
}
if ($model->seo_description) {
    $this->params['description'] = $model->seo_description;
} else {
    $this->params['description'] = "Только действующие промокоды на {$model->name} ➤ скидки до ㊿% ➤ подарки ➤ бесплатная доставка";
}
if ($model->seo_keywords) {
    $this->params['keywords'] = $model->seo_keywords;
}
$this->params['image'] = $model->icon;
$this->params['breadcrumbs'][] = ['label' => 'Все категории', 'url' => ['category/index']];
$this->params['breadcrumbs'][] = $model->name;
if ($dataProvider->pagination->page) {
    $this->title .= ' | Страница '.(1 + $dataProvider->pagination->page);
    $this->params['description'] .= ' | Страница '.(1 + $dataProvider->pagination->page);
}
$params = Json::encode(['url' => Url::to(['category/rate', 'id' => $model->id])]);
$this->registerJs("$('#js-category-rating').StarRating({$params});");
$this->registerJsFile('//yastatic.net/es5-shims/0.0.2/es5-shims.min.js');
$this->registerJsFile('//yastatic.net/share2/share.js');

?>
<div class="row">
    <main class="col-lg-17 col-xl-18">
        <h1>Промокоды на <?= $model->name ?> <?= $dataProvider->pagination->page ? Yii::t(
                'app',
                '<small>страница {n}</small>',
                ['n' => $dataProvider->pagination->page + 1]
            ) : '' ?></h1>
        <?= ListView::widget(
            [
                'dataProvider' => $dataProvider,
                'itemView' => '@frontend/views/offer/_offer',
                'viewParams' => ['todayUsed' => $todayUsed],
                'pager' => [
                    'options' => ['class' => 'pagination'],
                    'linkContainerOptions' => ['class' => 'page-item'],
                    'linkOptions' => ['class' => 'page-link'],
                    'disabledListItemSubTagOptions' => ['class' => 'page-link', 'tag' => 'a', 'href' => '#'],
                ],
                'options' => [
                    'class' => 'coupon-list',
                ],
                'itemOptions' => [
                    'class' => 'card coupon',
                ],
                'summary' => false,
            ]
        ) ?>
        <div>
            <?= nl2br($model->description) ?>
        </div>
    </main>
    <div class="col-lg-7 col-xl-6">
        <div class="text-center image">
            <?= Html::img(
                Image::url($model->icon, 240, 180),
                ['alt' => $model->name, 'title' => $model->name, 'class' => 'img-fluid']
            ) ?>
            <div class="aggregateRating category" itemprop="aggregateRating" itemscope
                 itemtype="http://schema.org/AggregateRating">
                <?= RateWidget::widget(['value' => $model->getRate(), 'id' => 'js-category-rating']) ?>
                <div>
                    <span class="svg--user svg-icon" data-grunticon-embed></span>
                    <span class="js-rate-count"><?= $model->getFormattedRateCount() ?></span>
                    <meta content="<?= $model->rate_count ?>" itemprop="reviewCount"/>
                    <meta content="<?= $model->getRate() ?>" itemprop="ratingValue"/>
                </div>
            </div>
            <div class="ya-share2"
                 data-services="vkontakte,facebook,odnoklassniki,twitter,viber,whatsapp,skype,telegram"></div>
        </div>
        <?= NewShops::widget(); ?>
    </div>
</div>
