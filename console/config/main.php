<?php
return [
    'id' => 'promocode-console',
    'basePath' => dirname(__DIR__),
    'name' => 'promokashka.com',
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'targets' => [
                'file' => [
                    'logFile' => '@runtime/logs/console.log',
                ],
            ],
        ],
        'urlManager' => [
            'scriptUrl' => '',
            'baseUrl' => '',
            'hostInfo' => 'https://promokashka.com',
            'rules' => [
                ['pattern' => 'sitemap', 'route' => 'site/map', 'suffix' => '.xml'],
                'shops' => 'shop/index',
                '<_c:(category|shop)>/rate' => '<_c>/rate',
                'categories' => 'category/index',
                '<_c:(shop|category)>/<slug>' => '<_c>/view',
                '' => 'site/index',
                '<_a:(search|error|like|dislike)>' => 'site/<_a>',
                'out/<id:\d+>' => 'site/out',
                'site/shop' => 'site/shop',
            ],
        ],
    ],
];
