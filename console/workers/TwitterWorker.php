<?php

namespace console\workers;


use common\models\Offer;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\queue\JobInterface;
use yii\queue\Queue;

class TwitterWorker extends BaseObject implements JobInterface
{
    public $id;

    /**
     * @param Queue $queue which pushed and is handling the job
     * @throws Exception
     */
    public function execute($queue)
    {
        /** @var Offer $offer */
        $offer = Offer::find()->with(['shop'])->where(['id' => $this->id, 'status' => Offer::STATUS_ACTIVE])
            ->andWhere('twitter_status_id IS NULL')->one();

        if (!$offer) {
            throw new Exception('Offer not found or offer state is not active or already twit');
        }
        $offerUrl = Url::to(
            [
                '/shop/view',
                'slug' => $offer->shop->slug,
                'utm_source' => 'twitter',
                'utm_medium' => 'promokashkacom',
                'utm_campaign' => $offer->shop->slug,
                '#' => 'coupon'.$offer->id,
            ],
            true
        );
        $shortUrl = \Yii::$app->shortUrl->shortUrl($offerUrl);
        if ($shortUrl === null) {
            throw new Exception('Error generate short url');
        }
        $shortUrlLength = mb_strlen($shortUrl, \Yii::$app->charset);
        $text = '#'.$offer->shop->slug.' '.$offer->name.' '.$offer->description;
        $text = StringHelper::truncate($text, 100 - $shortUrlLength).' '.$shortUrl;
        $offer->twitter_status_id = \Yii::$app->twitter->updateStatus($text);
        if ($offer->twitter_status_id !== null) {
            $offer->detachBehaviors();
            $offer->save(false, ['content_unique_percent']);
        } else {
            throw new Exception('Error update status');
        }
    }
}