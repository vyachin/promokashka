<?php

namespace console\workers;


use common\models\Offer;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\queue\JobInterface;
use yii\queue\Queue;

class CheckDescriptionWorker extends BaseObject implements JobInterface
{
    public $id;

    /**
     * @param Queue $queue
     * @throws Exception
     */
    public function execute($queue)
    {
        /** @var Offer $offer */
        $offer = Offer::findOne(['status' => Offer::STATUS_READY, 'id' => $this->id]);
        if (!$offer) {
            throw new Exception('Offer not found or offer state is not ready');
        }
        $offer->content_unique_percent = \Yii::$app->antiplagiat->checkText($offer->description);
        if ($offer->content_unique_percent !== null) {
            $offer->detachBehaviors();
            $offer->save(false, ['content_unique_percent']);
        } else {
            throw new Exception('Error check text');
        }
    }
}