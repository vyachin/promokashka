<?php

use yii\db\Migration;

/**
 * Class m190102_201526_create_table_seo
 */
class m190102_201526_create_table_seo extends Migration
{
    public function safeUp()
    {
        $this->createTable(
            '{{%seo}}',
            [
                'id' => $this->primaryKey(),
                'page' => $this->tinyInteger()->notNull()->unique(),
                'seo_title' => $this->string(),
                'seo_description' => $this->string(),
                'seo_keywords' => $this->string(),
                'user_id' => $this->integer(),
                'created_at' => $this->dateTime()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
            ],
            'ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%seo}}');
    }
}
