<?php

use yii\db\Migration;

/**
 * Class m190102_200533_add_column_category_seo_columns
 */
class m190102_200533_add_column_category_seo_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'seo_title', $this->string());
        $this->addColumn('{{%category}}', 'seo_description', $this->string());
        $this->addColumn('{{%category}}', 'seo_keywords', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%category}}', 'seo_title');
        $this->dropColumn('{{%category}}', 'seo_description');
        $this->dropColumn('{{%category}}', 'seo_keywords');
    }
}
