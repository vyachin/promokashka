<?php

use yii\db\Migration;

class m190118_233714_add_column_post_seo_title extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%post}}', 'seo_title', $this->string()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%post}}', 'seo_title');
    }
}
