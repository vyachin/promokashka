<?php

use yii\db\Migration;

class m160129_153944_create_table_offer extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%offer}}', [
            'id' => $this->primaryKey(),
            'shop_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'url' => $this->string()->notNull(),
            'begin_at' => $this->dateTime()->notNull(),
            'end_at' => $this->dateTime()->notNull(),
            'promo_code' => $this->string()->notNull(),
            'discount' => $this->float()->notNull(),
            'type' => "ENUM('discount', 'free_shipping', 'gift', 'cash') NOT NULL",
            'status' => "enum('draft', 'active', 'deleted', 'ready') NOT NULL",
            'admitad_id' => $this->integer(),
            'cityads_id' => $this->integer(),
            'content_unique_percent' => $this->float(),
            'user_id' => $this->integer(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], 'ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci');
        $this->addForeignKey('{{%fk_offer_shop}}', '{{%offer}}', 'shop_id', '{{%shop}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%offer}}');
    }
}
