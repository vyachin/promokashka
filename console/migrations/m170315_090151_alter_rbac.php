<?php

use yii\base\InvalidConfigException;
use yii\db\Migration;
use yii\rbac\DbManager;

class m170315_090151_alter_rbac extends Migration
{
    /** @var  DbManager */
    private $authManager;

    public function init()
    {
        parent::init();
        $this->authManager = Yii::$app->getAuthManager();
        if (!$this->authManager instanceof DbManager) {
            throw new InvalidConfigException(
                'You should configure "authManager" component to use database before executing this migration.'
            );
        }
    }

    public function up()
    {
        $this->dropColumn($this->authManager->itemTable, 'created_at');
        $this->dropColumn($this->authManager->itemTable, 'updated_at');
        $this->addColumn($this->authManager->itemTable, 'created_at', $this->integer());
        $this->addColumn($this->authManager->itemTable, 'updated_at', $this->integer());

        $this->dropColumn($this->authManager->ruleTable, 'created_at');
        $this->dropColumn($this->authManager->ruleTable, 'updated_at');
        $this->addColumn($this->authManager->ruleTable, 'created_at', $this->integer());
        $this->addColumn($this->authManager->ruleTable, 'updated_at', $this->integer());

        $this->dropColumn($this->authManager->assignmentTable, 'created_at');
        $this->addColumn($this->authManager->assignmentTable, 'created_at', $this->integer());
    }

    public function down()
    {
    }
}
