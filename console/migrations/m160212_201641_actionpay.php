<?php

use yii\db\Migration;

class m160212_201641_actionpay extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop}}', 'actionpay_id', 'integer AFTER cityads_id');
        $this->addColumn('{{%offer}}', 'actionpay_id', 'integer AFTER cityads_id');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%shop}}', 'actionpay_id');
        $this->dropColumn('{{%offer}}', 'actionpay_id');
    }
}
