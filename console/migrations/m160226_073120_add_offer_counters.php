<?php

use yii\db\Migration;

class m160226_073120_add_offer_counters extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%offer}}', 'count_used', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('{{%offer}}', 'count_like', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('{{%offer}}', 'count_dislike', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%offer}}', 'count_used');
        $this->dropColumn('{{%offer}}', 'count_like');
        $this->dropColumn('{{%offer}}', 'count_dislike');
    }
}
