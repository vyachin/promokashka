<?php

use yii\db\Migration;

class m160206_114923_optimization extends Migration
{
    public function safeUp()
    {
        $this->createIndex('{{%idx_shop_status_updated_at}}', '{{%shop}}', ['status', 'updated_at']);
        $this->createIndex('{{%idx_offer_status_updated_at}}', '{{%offer}}', ['status', 'updated_at']);
    }

    public function safeDown()
    {
        $this->dropIndex('{{%idx_shop_status_updated_at}}', '{{%shop}}');
        $this->dropIndex('{{%idx_offer_status_updated_at}}', '{{%offer}}');
    }
}
