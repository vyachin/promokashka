<?php

use yii\db\Migration;

class m160315_183429_add_shop_rate extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop}}', 'rate_sum', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('{{%shop}}', 'rate_count', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%shop}}', 'rate_sum');
        $this->dropColumn('{{%shop}}', 'rate_count');
    }
}
