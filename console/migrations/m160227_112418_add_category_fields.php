<?php

use yii\db\Migration;

class m160227_112418_add_category_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'slug', $this->string()->unique());
        $this->addColumn('{{%category}}', 'description', $this->text());
        $this->addColumn('{{%category}}', 'icon', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%category}}', 'slug');
        $this->dropColumn('{{%category}}', 'description');
        $this->dropColumn('{{%category}}', 'icon');
    }
}
