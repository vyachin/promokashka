<?php

use yii\db\Migration;

class m190102_194735_add_column_shop_seo_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%shop}}', 'seo_title', $this->string());
        $this->addColumn('{{%shop}}', 'seo_description', $this->string());
        $this->addColumn('{{%shop}}', 'seo_keywords', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%shop}}', 'seo_title');
        $this->dropColumn('{{%shop}}', 'seo_description');
        $this->dropColumn('{{%shop}}', 'seo_keywords');
    }
}
