<?php

use yii\db\Migration;

class m160129_153444_create_table_shop extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%shop}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull()->unique(),
            'description' => $this->text(),
            'logo' => $this->string(),
            'domain' => $this->string()->notNull(),
            'status' => "enum('draft', 'active', 'deleted') NOT NULL",
            'admitad_id' => $this->integer(),
            'cityads_id' => $this->integer(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], 'ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }

    public function safeDown()
    {
        $this->dropTable('{{%shop}}');
    }
}
