<?php

use yii\db\Migration;

/**
 * Class m190205_213245_add_post_rate_columns
 */
class m190205_213245_add_post_rate_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%post}}', 'rate_sum', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('{{%post}}', 'rate_count', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%post}}', 'rate_sum');
        $this->dropColumn('{{%post}}', 'rate_count');
    }
}
