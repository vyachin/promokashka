<?php

use yii\db\Migration;

class m160208_165029_add_author_id_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%offer}}', 'author_id', $this->integer());
        $this->addForeignKey(
            '{{%offer_author_user_id}}',
            '{{%offer}}',
            'author_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('{{%offer_author_user_id}}', '{{%offer}}');
        $this->dropColumn('{{%offer}}', 'author_id');
    }
}
