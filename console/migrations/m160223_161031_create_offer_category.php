<?php

use yii\db\Migration;

class m160223_161031_create_offer_category extends Migration
{
    public function safeUp()
    {
        $this->createTable(
            '{{%offer_category}}',
            [
                'offer_id' => $this->integer()->notNull(),
                'category_id' => $this->integer()->notNull(),
                'PRIMARY KEY(offer_id, category_id)',
            ],
            'ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci'
        );
        $this->addForeignKey(
            '{{%fk_offer_category_offer}}',
            '{{%offer_category}}',
            'offer_id',
            '{{%offer}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            '{{%fk_offer_category_category}}',
            '{{%offer_category}}',
            'category_id',
            '{{%category}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('{{%fk_offer_category_offer}}', '{{%offer_category}}');
        $this->dropForeignKey('{{%fk_offer_category_category}}', '{{%offer_category}}');
        $this->dropTable('{{%offer_category}}');
    }
}
