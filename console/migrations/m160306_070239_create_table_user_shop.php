<?php

use yii\db\Migration;

class m160306_070239_create_table_user_shop extends Migration
{
    public function up()
    {
        $this->createTable(
            '{{%user_shop}}',
            [
                'user_id' => $this->integer()->notNull(),
                'shop_id' => $this->integer()->notNull(),
            ],
            'ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci'
        );
        $this->addPrimaryKey('{{%pk_user_shop}}', '{{%user_shop}}', ['user_id', 'shop_id']);
        $this->addForeignKey(
            '{{%fk_user_shop_user}}',
            '{{%user_shop}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            '{{%fk_user_shop_shop}}',
            '{{%user_shop}}',
            'shop_id',
            '{{%shop}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('{{%fk_user_shop_user}}', '{{%user_shop}}');
        $this->dropForeignKey('{{%fk_user_shop_shop}}', '{{%user_shop}}');
        $this->dropPrimaryKey('{{%pk_user_shop}}', '{{%user_shop}}');
        $this->dropTable('{{%user_shop}}');
    }
}
