<?php

use yii\db\Migration;

class m160210_193825_add_offer_twitter_status_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%offer}}', 'twitter_status_id', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%offer}}', 'twitter_status_id');
    }
}
