<?php

use yii\db\Migration;

class m160212_221631_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('{{%idx_shop_admitad_id}}', '{{%shop}}', ['admitad_id']);
        $this->createIndex('{{%idx_shop_cityads_id}}', '{{%shop}}', ['cityads_id']);
        $this->createIndex('{{%idx_shop_actionpay_id}}', '{{%shop}}', ['actionpay_id']);

        $this->createIndex('{{%idx_offer_admitad_id}}', '{{%offer}}', ['admitad_id']);
        $this->createIndex('{{%idx_offer_cityads_id}}', '{{%offer}}', ['cityads_id']);
        $this->createIndex('{{%idx_offer_actionpay_id}}', '{{%offer}}', ['actionpay_id']);
    }

    public function safeDown()
    {
        $this->dropIndex('{{%idx_shop_admitad_id}}', '{{%shop}}');
        $this->dropIndex('{{%idx_shop_cityads_id}}', '{{%shop}}');
        $this->dropIndex('{{%idx_shop_actionpay_id}}', '{{%shop}}');

        $this->dropIndex('{{%idx_offer_admitad_id}}', '{{%offer}}');
        $this->dropIndex('{{%idx_offer_cityads_id}}', '{{%offer}}');
        $this->dropIndex('{{%idx_offer_actionpay_id}}', '{{%offer}}');
    }
}
