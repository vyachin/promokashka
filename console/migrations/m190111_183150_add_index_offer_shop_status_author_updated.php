<?php

use yii\db\Migration;

/**
 * Class m190111_183150_add_index_offer_shop_status_author_updated
 */
class m190111_183150_add_index_offer_shop_status_author_updated extends Migration
{
    public function safeUp()
    {
        $this->createIndex('idx_shop_status_author_updated', '{{%offer}}',
            ['shop_id', 'status', 'author_id', 'updated_at']);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_shop_status_author_updated', '{{%offer}}');
    }
}
