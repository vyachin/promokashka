<?php

use yii\db\Migration;

class m190118_173422_create_table_post extends Migration
{
    public function safeUp()
    {
        $this->createTable(
            '{{%post}}',
            [
                'id' => $this->primaryKey(),
                'slug' => $this->string()->notNull()->unique(),
                'title' => $this->string()->notNull(),
                'image' => $this->string()->notNull(),
                'body' => $this->text(),
                'seo_description' => $this->string()->notNull(),
                'seo_keywords' => $this->string()->notNull(),
                'status' => $this->tinyInteger()->notNull(),
                'user_id' => $this->integer()->notNull(),
                'created_at' => $this->dateTime()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
                'published_at' => $this->dateTime(),
            ],
            'ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci'
        );
        $this->addForeignKey('{{%fk_post_user_id}}', '{{%post}}', 'user_id', '{{%user}}', 'id');
        $this->createIndex('{{%idx_post_status_published_at}}', '{{%post}}', ['status', 'published_at']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%post}}');
    }
}
