<?php

use yii\db\Migration;

/**
 * Class m181217_174919_create_fulltext_index_offer
 */
class m181217_174919_create_fulltext_index_offer extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE FULLTEXT INDEX fti_name_description ON {{%offer}}(name, description)');
    }

    public function safeDown()
    {
        $this->dropIndex('fti_name_description', '{{%offer}}');
    }
}
