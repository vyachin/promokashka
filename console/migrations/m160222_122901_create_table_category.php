<?php

use yii\db\Migration;

class m160222_122901_create_table_category extends Migration
{
    public function up()
    {
        $this->createTable(
            '{{%category}}',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull()->unique(),
                'status' => "enum('active', 'deleted') NOT NULL",
                'created_at' => $this->dateTime()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
            ],
            'ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci'
        );
    }

    public function down()
    {
        $this->dropTable('{{%category}}');
    }
}
