<?php

use yii\db\Migration;

class m160223_160758_add_category_names extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%offer}}', 'category_names', $this->string(512));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%offer}}', 'category_names');
    }
}
