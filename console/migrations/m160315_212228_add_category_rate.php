<?php

use yii\db\Migration;

class m160315_212228_add_category_rate extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'rate_sum', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('{{%category}}', 'rate_count', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%category}}', 'rate_sum');
        $this->dropColumn('{{%category}}', 'rate_count');
    }
}
