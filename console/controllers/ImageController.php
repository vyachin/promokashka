<?php

namespace console\controllers;

use common\models\Category;
use common\models\Shop;
use yii\console\Controller;

class ImageController extends Controller
{
    public $result;

    public function actionIndex()
    {
        /** @var Shop[] $shops */
        $shops = Shop::find()->all();
        foreach ($shops as $shop) {
            if (!$shop->logo) {
                continue;
            }
            $name = $this->processImage($shop->logo, $shop->slug);
            if ($name !== false) {
                $shop->logo = $name;
                $shop->save();
            }

        }
        /** @var Category[] $categories */
        $categories = Category::find()->all();
        foreach ($categories as $category) {
            if (!$category->icon) {
                continue;
            }
            $name = $this->processImage($category->icon, $category->slug);
            if ($name !== false) {
                $category->icon = $name;
                $category->save();
            }
        }
    }

    private function processImage($source, $slug)
    {
        $hash = base64_encode(\Yii::$app->params['yandex']['login'] . ':' . \Yii::$app->params['yandex']['password']);
        $opts = [
            'http' => [
                'method' => 'GET',
                'header' => "Authorization: Basic $hash\r\n"
            ]
        ];
        $context = stream_context_create($opts);
        $content = file_get_contents("https://webdav.yandex.ru/$source", 'r', $context);

        if (!$meta = getimagesizefromstring($content)) {
            return false;
        }

        list($width, $height, $type, $attr) = $meta;

        $expression = image_type_to_extension($type);
        $mimeType = image_type_to_mime_type($type);

        $newName = "{$slug}{$expression}";

        $opts = [
            'http' => [
                'method' => 'PUT',
                'header' => "Authorization: Basic $hash\r\nContent-type: $mimeType\r\n",
                'content' => $content,
            ]
        ];
        $context = stream_context_create($opts);

        $h = fopen("https://webdav.yandex.ru/v2/$newName", 'rb', false, $context);
        fclose($h);
        echo "$source $slug $newName\n";
        return $newName;
    }
}
