<?php

namespace console\controllers;

use yii\console\Controller;

class BackupController extends Controller
{
    public function actionDropbox()
    {
        if (!preg_match('/mysql:host=(\w+);dbname=(\w+);?/', \Yii::$app->db->dsn, $match)) {
            \Yii::error('Database name and host name not found: '.\Yii::$app->db->dsn, 'backup.dropbox');

            return;
        }
        $dbName = $match[2];
        $host = $match[1];
        $username = \Yii::$app->db->username;
        $password = \Yii::$app->db->password;
        $outfile = '/tmp/pc-'.date('Y-m-d-H-i-s').'.sql.gz';
        $cmd = "mysqldump -h{$host} -u{$username} -p{$password} {$dbName} | gzip -9 -f > {$outfile}";
        try {
            system($cmd, $returnValue);
            if ($returnValue) {
                throw new \Exception("Error backup database {$returnValue}");
            }
            $result = \Yii::$app->dropbox->uploadFile($outfile);
            @unlink($outfile);
            $this->stdout($result['path'].PHP_EOL);
        } catch (\Exception $e) {
            @unlink($outfile);
            \Yii::error($e, 'backup.dropbox');
            $this->stderr($e->getMessage().PHP_EOL);
        }
    }
}
