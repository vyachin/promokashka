<?php

namespace console\controllers;

use common\models\User;
use common\rbac\OfferGetWorkRule;
use common\rbac\OfferOwnRule;
use common\rbac\OfferViewRule;
use yii\base\UserException;
use yii\console\Controller;
use yii\db\Query;
use yii\rbac\DbManager;
use yii\rbac\Rule;

class AuthController extends Controller
{
    public function actionAddUser($username, $email, $password)
    {
        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->password = $user->passwordVerify = $password;
        if ($user->save()) {
            $this->stdout("Add new user\n");
        } else {
            foreach ($user->getErrors() as $error) {
                $this->stdout("$error\n");
            }
        }
    }

    public function actionInit()
    {
        $authManager = $this->getAuthManager();
        $assignments = (new Query())->from($authManager->assignmentTable)->all();
        $authManager->removeAll();

        $offerOwnRule = new OfferOwnRule();
        $authManager->add($offerOwnRule);

        $offerViewRule = new OfferViewRule();
        $authManager->add($offerViewRule);

        $offerGetWorkRule = new OfferGetWorkRule();
        $authManager->add($offerGetWorkRule);

        $shopList = $this->createPermission(User::SHOP_LIST);
        $shopCreate = $this->createPermission(User::SHOP_CREATE);
        $shopUpdate = $this->createPermission(User::SHOP_UPDATE);

        $categoryList = $this->createPermission(User::CATEGORY_LIST);
        $categoryCreate = $this->createPermission(User::CATEGORY_CREATE);
        $categoryUpdate = $this->createPermission(User::CATEGORY_UPDATE);

        $userList = $this->createPermission(User::USER_LIST);
        $userCreate = $this->createPermission(User::USER_CREATE);
        $userUpdate = $this->createPermission(User::USER_UPDATE);
        $userLogin = $this->createPermission(User::USER_LOGIN);

        $offerList = $this->createPermission(User::OFFER_LIST);

        $offerEditorList = $this->createPermission(User::OFFER_EDITOR_LIST);
        $authManager->addChild($offerEditorList, $offerList);

        $offerUpdate = $this->createPermission(User::OFFER_UPDATE);

        $offerEditorUpdate = $this->createPermission(User::OFFER_EDITOR_UPDATE, $offerOwnRule);
        $authManager->addChild($offerEditorUpdate, $offerUpdate);

        $offerView = $this->createPermission(User::OFFER_VIEW);

        $offerEditorView = $this->createPermission(User::OFFER_EDITOR_VIEW, $offerViewRule);
        $authManager->addChild($offerEditorView, $offerView);

        $offerGetWork = $this->createPermission(User::OFFER_GET_WORK, $offerGetWorkRule);


        $seoList = $this->createPermission(User::SEO_LIST);
        $seoCreate = $this->createPermission(User::SEO_CREATE);
        $seoUpdate = $this->createPermission(User::SEO_UPDATE);

        $postList = $this->createPermission(User::POST_LIST);
        $postCreate = $this->createPermission(User::POST_CREATE);
        $postUpdate = $this->createPermission(User::POST_UPDATE);


        $this->createRole(
            User::ROLE_EDITOR,
            [
                $offerEditorList,
                $offerEditorUpdate,
                $offerEditorView,
                $offerGetWork,
            ]
        );

        $this->createRole(
            User::ROLE_ADMIN,
            [
                $offerList,
                $offerUpdate,
                $offerView,

                $shopList,
                $shopCreate,
                $shopUpdate,

                $categoryList,
                $categoryCreate,
                $categoryUpdate,

                $userList,
                $userCreate,
                $userUpdate,
                $userLogin,

                $seoList,
                $seoCreate,
                $seoUpdate,

                $postList,
                $postCreate,
                $postUpdate,
            ]
        );

        $this->createRole(
            User::ROLE_SEO,
            [
                $offerList,
                $offerUpdate,
                $offerView,

                $shopList,
                $shopCreate,
                $shopUpdate,

                $categoryList,
                $categoryCreate,
                $categoryUpdate,

                $seoList,
                $seoCreate,
                $seoUpdate,

                $postList,
                $postCreate,
                $postUpdate,
            ]
        );

        foreach ($assignments as $item) {
            \Yii::$app->db->createCommand()->insert($authManager->assignmentTable, $item)->execute();
        }
    }

    /**
     * @return \yii\rbac\DbManager
     * @throws UserException
     */
    private function getAuthManager()
    {
        $authManager = \Yii::$app->authManager;
        if (!$authManager instanceof DbManager) {
            throw new UserException('AuthManager must be DbManager');
        }

        return $authManager;
    }

    /**
     * @param string $name
     * @param Rule $rule
     * @return \yii\rbac\Permission
     * @throws UserException
     */
    private function createPermission($name, Rule $rule = null)
    {
        $authManager = $this->getAuthManager();
        $permission = $authManager->createPermission($name);
        if ($rule) {
            $permission->ruleName = $rule->name;
        }
        $authManager->add($permission);

        return $permission;
    }

    protected function createRole($roleName, $childItems = [])
    {
        $authManager = $this->getAuthManager();
        $role = $authManager->createRole($roleName);
        $authManager->add($role);
        $this->stdout("Add role {$roleName} [");
        foreach ($childItems as $child) {
            $authManager->addChild($role, $child);
            $this->stdout("{$child->name} ");
        }
        $this->stdout("]\n");

        return $role;
    }

    public function actionAssign($role, $email)
    {
        $authManager = $this->getAuthManager();
        /** @var \yii\rbac\Role $role */
        if (!$role = $authManager->getRole($role)) {
            throw new \RuntimeException('Role not found');
        }
        if (!$user = User::findByEmail($email)) {
            throw new \RuntimeException('Email not found');
        }
        $authManager->assign($role, $user->id);
        $this->stdout($user->email.' assign role '.$role->name.PHP_EOL);
    }
}
