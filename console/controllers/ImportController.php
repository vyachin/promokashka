<?php

namespace console\controllers;

use common\models\Offer;
use common\models\Shop;
use yii\console\Controller;
use yii\helpers\VarDumper;

class ImportController extends Controller
{
    public function actionAdmitad($fileName = null)
    {
        if ($fileName === null) {
            $context = stream_context_create(
                [
                    'http' => [
                        'max_redirects' => 101,
                        'timeout' => 1200,
                    ]
                ]
            );
            $content = file_get_contents(\Yii::$app->params['admitad'], false, $context);
            file_put_contents(\Yii::getAlias('@runtime/admitad.xml'), $content);
            $xml = simplexml_load_string($content);
        } else {
            $xml = simplexml_load_file($fileName);
        }

        $shops = [];

        foreach ($xml->advcampaigns->advcampaign as $advcampaign) {
            $id = (int)$advcampaign->attributes()->id;

            $shop = $this->findShop(
                ['admitad_id' => $id],
                [
                    'admitad_id' => $id,
                    'name' => (string)$advcampaign->name,
                    'domain' => (string)(string)$advcampaign->site,
                ]
            );
            if (!$shop) {
                throw new \Exception(VarDumper::dumpAsString([$advcampaign, $shop]));
            }

            $shops[$id] = $shop->id;
        }

        $categories = [];
        foreach ($xml->categories->category as $category) {
            $id = (int)$category->attributes()->id;
            $categories[$id] = (string)$category;
        }

        foreach ($xml->coupons->coupon as $coupon) {
            if (mb_strtolower(trim($coupon->promocode), \Yii::$app->charset) == 'не нужен') {
                continue;
            }
            $id = (int)$coupon->attributes()->id;

            if (Offer::find()->where(['admitad_id' => $id])->exists()) {
                continue;
            }
            $offer = new Offer();
            $offer->name = (string)$coupon->name;
            $offer->description = (string)$coupon->description;
            $offer->promo_code = (string)$coupon->promocode;
            $offer->discount = (float)$coupon->discount;
            $offer->url = (string)$coupon->gotolink;
            $offer->begin_at = (string)$coupon->date_start;
            if (!$date = \DateTime::createFromFormat('Y-m-d H:i:s', (string)$coupon->date_end)) {
                $date = new \DateTime();
                $date->modify('next year');
            }
            $offer->end_at = $date->format('Y-m-d H:i:s');
            $offer->admitad_id = $id;
            $offer->status = Offer::STATUS_DRAFT;
            $couponCategories = [];
            foreach ($coupon->categories->category_id as $categoryId) {
                $couponCategories[] = $categories[(int)$categoryId];
            }
            $offer->category_names = implode(', ', $couponCategories);
            if ($offer->category_names) {
                /** @var Offer $pattern */
                $pattern = Offer::find()
                    ->andWhere('category_names=:category_names', [':category_names' => $offer->category_names,])
                    ->andWhere('EXISTS(SELECT 1 FROM {{%offer_category}} oc WHERE oc.offer_id = id)')
                    ->orderBy(['updated_at' => SORT_DESC])
                    ->one();
                if ($pattern) {
                    $offer->category_list = $pattern->category_list;
                }
            }
            if ($discount = (string)$coupon->discount) {
                if (strpos($discount, '%')) {
                    $offer->type = Offer::TYPE_DISCOUNT;
                    $offer->discount = (float)$discount;
                } else {
                    $offer->type = Offer::TYPE_CASH;
                    $offer->discount = (float)$discount;
                }
            } else {
                switch ($coupon->types->type_id) {
                    case 1:
                        $offer->type = Offer::TYPE_FREE_SHIPPING;
                        break;
                    case 2:
                        $offer->type = Offer::TYPE_DISCOUNT;
                        break;
                    case 3:
                        $offer->type = Offer::TYPE_GIFT;
                        break;
                    case 4:
                        $offer->type = Offer::TYPE_CASH;
                        break;
                }
            }
            $offer->shop_id = $shops[(int)$coupon->advcampaign_id];
            if (!$offer->save()) {
                \Yii::error(VarDumper::dumpAsString([$coupon, $offer]), 'import.admitad');
            }
        }
    }

    public function actionCityads($fileName = null)
    {
        if ($fileName === null) {
            $context = stream_context_create(
                [
                    'http' => [
                        'max_redirects' => 101,
                        'timeout' => 1200,
                    ]
                ]
            );
            $content = file_get_contents(\Yii::$app->params['cityads'], false, $context);
            file_put_contents(\Yii::getAlias('@runtime/cityads.xml'), $content);
            $xml = simplexml_load_string($content);
        } else {
            $xml = simplexml_load_file($fileName);
        }
        if ($xml->status != 200) {
            \Yii::error(VarDumper::dumpAsString($xml->error), 'import.cityads');

            return;
        }
        $count = 0;
        foreach ($xml->data->items->item as $item) {
            if (empty($item->promo_code)) {
                continue;
            }
            if (Offer::find()->where(['cityads_id' => (int)$item->id])->exists()) {
                continue;
            }
            $offer = new Offer();
            $offer->name = (string)$item->name;
            if (!$offer->name) {
                $offer->name = (string)$item->description;
            }
            $offer->description = (string)$item->description;
            $offer->promo_code = (string)$item->promo_code;
            $offer->type = Offer::TYPE_DISCOUNT;
            $offer->url = (string)$item->url;
            $offer->begin_at = (string)$item->start_date;
            $offer->end_at = (string)$item->active_to;
            $offer->cityads_id = (int)$item->id;
            $offer->status = Offer::STATUS_DRAFT;
            $shop = $this->findShop(
                ['cityads_id' => (int)$item->offer_id],
                [
                    'cityads_id' => (int)$item->offer_id,
                    'name' => (string)$item->offer_name,
                    'domain' => (string)$item->domain,
                ]
            );
            if (!$shop) {
                continue;
            }
            $offer->shop_id = $shop->id;
            if (!$offer->save()) {
                \Yii::error('Error save offer '.VarDumper::dumpAsString($offer->errors), 'import.cityads');
                continue;
            }
            ++$count;
        }
        $this->stdout("Import {$count} coupons.\n");
    }

    public function actionActionpay($fileName = null)
    {
        if ($fileName === null) {
            $context = stream_context_create(
                [
                    'http' => [
                        'max_redirects' => 101,
                        'timeout' => 1200,
                    ]
                ]
            );
            $content = file_get_contents(\Yii::$app->params['actionpay'], false, $context);
            file_put_contents(\Yii::getAlias('@runtime/actionpay.xml'), $content);
            $xml = simplexml_load_string($content);
        } else {
            $xml = simplexml_load_file($fileName);
        }
        $count = 0;
        foreach ($xml->promotion as $item) {
            if (empty($item->code) || empty($item->link)) {
                continue;
            }
            if (Offer::find()->where(['actionpay_id' => (int)$item->id])->exists()) {
                continue;
            }
            $offer = new Offer();
            $offer->name = (string)$item->title;
            $offer->description = (string)$item->description;
            $offer->promo_code = (string)$item->code;
            switch ((int)$item->type_id) {
                case 1:
                    $offer->type = Offer::TYPE_FREE_SHIPPING;
                    break;
                case 2:
                    $offer->type = Offer::TYPE_CASH;
                    break;
                case 3:
                    $offer->type = Offer::TYPE_GIFT;
                    break;
                case 4:
                    $offer->type = Offer::TYPE_DISCOUNT;
                    break;
            }
            $offer->url = (string)$item->link;
            $offer->begin_at = (string)$item->begin_date;
            $offer->end_at = (string)$item->end_date;
            $offer->actionpay_id = (int)$item->id;
            $offer->status = Offer::STATUS_DRAFT;
            $offer->category_names = (string)$item->categories;
            if ($offer->category_names) {
                /** @var Offer $pattern */
                $pattern = Offer::find()
                    ->andWhere('category_names=:category_names', [':category_names' => $offer->category_names,])
                    ->andWhere('EXISTS(SELECT 1 FROM {{%offer_category}} oc WHERE oc.offer_id = id)')
                    ->orderBy(['updated_at' => SORT_DESC])
                    ->one();
                if ($pattern) {
                    $offer->category_list = $pattern->category_list;
                }
            }
            $shop = $this->findShop(
                ['actionpay_id' => (int)$item->offer_id],
                [
                    'actionpay_id' => (int)$item->offer_id,
                    'name' => (string)$item->offer_name,
                    'domain' => (string)$item->offer_link,
                ]
            );
            if (!$shop) {
                continue;
            }
            $offer->shop_id = $shop->id;
            if (!$offer->save()) {
                \Yii::error('Error save offer '.VarDumper::dumpAsString($offer->errors), 'import.actionpay');
                continue;
            }
            ++$count;
        }
        $this->stdout("Import {$count} coupons.\n");
    }

    /**
     * @param array $searchCriteria
     * @param array $data
     * @return Shop
     */
    private function findShop(array $searchCriteria, array $data)
    {
        if (!$shop = Shop::findOne($searchCriteria)) {
            if (preg_match('/\/\/(www\.)?([^\/]+)\//', $data['domain'], $match)) {
                $shops = Shop::find()->where(['LIKE', 'domain', $match[2]])->all();
                if (count($shops) == 1) {
                    $shop = $shops[0];
                    $shop->setAttributes(
                        array_intersect_key($data, ['admitad_id' => '', 'cityads_id' => '', 'actionpay_id' => ''])
                    );
                    if (!$shop->save()) {
                        \Yii::error('Error save shop '.VarDumper::dumpAsString($shop->errors), 'import');
                    } else {
                        return $shop;
                    }
                }
            }
            $shop = new Shop();
            $shop->setAttributes($data);
            if (!$shop->save()) {
                \Yii::error('Error save shop '.VarDumper::dumpAsString($shop->errors), 'import');

                return null;
            }
        }

        return $shop;
    }
}
