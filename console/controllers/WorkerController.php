<?php

namespace console\controllers;

use yii\console\Controller;

class WorkerController extends Controller
{
    public function actionBalance()
    {
        $this->stdout('Balance: '.\Yii::$app->antiplagiat->getBalance().PHP_EOL);
    }

    public function actionShortUrl($url)
    {
        $this->stdout(\Yii::$app->shortUrl->shortUrl($url).PHP_EOL);
    }

    public function actionTweetText($text)
    {
        $this->stdout(\Yii::$app->twitter->updateStatus($text).PHP_EOL);
    }
}
