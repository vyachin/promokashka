<?php

namespace console\controllers;

use common\models\Offer;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\Url;

class OfferController extends Controller
{
    public function actionClear()
    {
        $now = date('Y-m-d H:i:s');
        Offer::updateAll(
            ['status' => Offer::STATUS_DELETED, 'updated_at' => $now],
            'status != :deleted and end_at < :now',
            [':deleted' => Offer::STATUS_DELETED, ':now' => $now]
        );
    }

    public function actionVktarget()
    {
        $result = (new Query())->select('s.slug')->from('{{%shop}} s')->leftJoin(
            '{{%offer}} o ',
            's.id = o.shop_id AND o.author_id IS NOT NULL'
        )->groupBy('s.slug')->having('count(o.id) > 0')->all();
        $data = [
            'task_name',
            'type',
            'text',
            'url',
            'min_fr',
            'max_fr',
            'sex',
            'min_age',
            'max_age',
            'limit',
            'towns',
            'near_towns',
            'status',
        ];
        echo implode(';', $data).';'.PHP_EOL;
        foreach ($result as $item) {
            $data = [
                'task_name' => 'vk - '.$item['slug'],
                'type' => 8,
                'text' => '',
                'url' => Url::to(
                    ['site/shop', 'slug' => $item['slug'], 'utm_medium' => 'vkt'],
                    true
                ),
                'min_fr' => 10,
                'max_fr' => 0,
                'sex' => 0,
                'min_age' => 18,
                'max_age' => 40,
                'limit' => 10,
                'towns' => 1,
                'near_towns' => 1,
                'status' => 2,
            ];
            echo implode(';', $data).';'.PHP_EOL;
        }
        foreach ($result as $item) {
            $data = [
                'task_name' => 'fb - '.$item['slug'],
                'type' => 20,
                'text' => '',
                'url' => Url::to(
                    ['site/shop', 'slug' => $item['slug'], 'utm_medium' => 'vkt'],
                    true
                ),
                'min_fr' => 10,
                'max_fr' => 0,
                'sex' => 0,
                'min_age' => 18,
                'max_age' => 40,
                'limit' => 10,
                'towns' => 1,
                'near_towns' => 1,
                'status' => 2,
            ];
            echo implode(';', $data).';'.PHP_EOL;
        }
        foreach ($result as $item) {
            $data = [
                'task_name' => 'gp - '.$item['slug'],
                'type' => 35,
                'text' => '',
                'url' => Url::to(
                    ['site/shop', 'slug' => $item['slug'], 'utm_medium' => 'vkt'],
                    true
                ),
                'min_fr' => 10,
                'max_fr' => 0,
                'sex' => 0,
                'min_age' => 18,
                'max_age' => 40,
                'limit' => 10,
                'towns' => 1,
                'near_towns' => 1,
                'status' => 2,
            ];
            echo implode(';', $data).';'.PHP_EOL;
        }
    }

    public function actionSeopult()
    {
        $result = (new Query())->select('s.slug, s.name')->from('{{%shop}} s')->leftJoin(
            '{{%offer}} o ',
            's.id = o.shop_id AND o.author_id IS NOT NULL'
        )->groupBy('s.name, s.slug')->having('count(o.id) > 0')->all();
        foreach ($result as $item) {
            $data = [
                'промокод '.mb_strtolower($item['name'], \Yii::$app->charset),
                3,
                Url::to(
                    ['shop/view', 'slug' => $item['slug']]
                ),
                25,
            ];
            echo implode(
                    ';',
                    array_map(
                        function ($v) {
                            return '"'.$v.'"';
                        },
                        $data
                    )
                ).PHP_EOL;
            $data = [
                'скидка '.mb_strtolower($item['name'], \Yii::$app->charset),
                3,
                Url::to(
                    ['shop/view', 'slug' => $item['slug']]
                ),
                25,
            ];
            echo implode(
                    ';',
                    array_map(
                        function ($v) {
                            return '"'.$v.'"';
                        },
                        $data
                    )
                ).PHP_EOL;

        }
    }

    public function actionUnique()
    {
        $result = (new Query())->select('description')->from('{{%offer}}')->where(
            'author_id IS NOT NULL AND status != "draft"'
        )->orderBy(['updated_at' => SORT_DESC])->all();
        foreach ($result as $item) {
            echo $item['description'].PHP_EOL.PHP_EOL;
        }
    }

    public function actionCategory()
    {
        /** @var Offer[] $offers */
        $offers = Offer::find()
            ->andWhere('category_names IS NOT NULL')
            ->andWhere('NOT EXISTS(SELECT 1 FROM {{%offer_category}} oc WHERE oc.offer_id = id)')
            ->all();

        foreach ($offers as $offer) {
            /** @var Offer $pattern */
            $pattern = Offer::find()
                ->andWhere('category_names=:category_names', [':category_names' => $offer->category_names,])
                ->andWhere('EXISTS(SELECT 1 FROM {{%offer_category}} oc WHERE oc.offer_id = id)')
                ->orderBy(['updated_at' => SORT_DESC])
                ->one();
            if ($pattern) {
                echo $offer->category_names.' => '.implode(', ', $pattern->category_list).PHP_EOL;
                $offer->category_list = $pattern->category_list;
                $offer->save();
            }
        }
    }
}
