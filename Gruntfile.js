const sass = require("node-sass");
const tilde_importer = require("grunt-sass-tilde-importer");

module.exports = function (grunt) {
    grunt.initConfig({
        svgmin: {
            dist: {
                options: {
                    plugins: [
                        {
                            removeXMLProcInst: false
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        cwd: "frontend/assets/svg",
                        src: ["*.svg"],
                        dest: "frontend/web/assets/icons/svg"
                    },
                    {
                        expand: true,
                        cwd: "backend/assets/svg",
                        src: ["*.svg"],
                        dest: "backend/web/assets/icons/svg"
                    }
                ]
            }
        },
        grunticon: {
            myIconsScss: {
                files: [
                    {
                        expand: true,
                        cwd: "frontend/web/assets/icons/svg",
                        src: ["*.svg"],
                        dest: "frontend/web/assets/icons",
                    },
                    {
                        expand: true,
                        cwd: "backend/web/assets/icons/svg",
                        src: ["*.svg"],
                        dest: "backend/web/assets/icons",
                    }
                ],
                options: {
                    cssprefix: ".svg--",
                    datasvgcss: "../../../assets/scss/icons.data.svg.scss",
                    datapngcss: "../../../assets/scss/icons.data.png.scss",
                    urlpngcss: "../../../assets/scss/icons.fallback.scss",
                    template: "common/assets/template.hbs",
                    corsEmbed: true,
                    enhanceSVG: true,
                    defaultWidth: "100px",
                    defaultHeight: "100px",
                    compressPNG: true,
                    pngpath: "/assets/icons/png"
                }
            },
        },
        sass: {
            dist: {
                options: {
                    implementation: sass,
                    sourceMap: false,
                    importer: tilde_importer
                },
                files: {
                    "backend/web/assets/css/style.css": "backend/assets/scss/style.scss",
                    "frontend/web/assets/css/style.css": "frontend/assets/scss/style.scss",
                    "frontend/web/assets/css/icons.data.png.css": "frontend/assets/scss/icons.data.png.scss",
                    "frontend/web/assets/css/icons.data.svg.css": "frontend/assets/scss/icons.data.svg.scss",
                    "frontend/web/assets/css/icons.fallback.css": "frontend/assets/scss/icons.fallback.scss",
                }
            }
        },
        cssmin: {
            options: {
                keepSpecialComments: 0
            },
            frontend: {
                files: {
                    'frontend/web/assets/css/style.min.css': ['frontend/web/assets/css/style.css']
                }
            },
            backend: {
                files: {
                    'backend/web/assets/css/style.min.css': ['backend/web/assets/css/style.css']
                }
            },
            critical: {
                files: {
                    'frontend/web/assets/css/critical.min.css': ['frontend/web/assets/css/critical.css']
                }
            }
        },
        concat: {
            frontend: {
                dest: 'frontend/web/assets/js/script.js',
                src: [
                    'node_modules/jquery/dist/jquery.js',
                    //'node_modules/bootstrap/js/transition.js',
                    //'node_modules/bootstrap/js/alert.js',
                    //'node_modules/bootstrap/js/button.js',
                    //'node_modules/bootstrap/js/carousel.js',
                    'node_modules/bootstrap/js/dist/util.js',
                    'node_modules/popper.js/dist/umd/popper.js',
                    'node_modules/bootstrap/js/dist/collapse.js',
                    'node_modules/bootstrap/js/dist/dropdown.js',
                    'node_modules/bootstrap/js/dist/modal.js',
                    //'node_modules/bootstrap/js/tooltip.js',
                    //'node_modules/bootstrap/js/popover.js',
                    //'node_modules/bootstrap/js/scrollspy.js',
                    //'node_modules/bootstrap/js/tab.js',
                    //'node_modules/bootstrap/js/affix.js',
                    'node_modules/typeahead.js/dist/typeahead.bundle.js',
                    'frontend/assets/js/jquery.transform2d.js',
                    //'vendor/bower-asset/owl-carousel/owl-carousel/owl.carousel.js',
                    'frontend/assets/js/StarRating.js',
                    'frontend/assets/js/global.js',
                ]
            },
            backend: {
                dest: 'backend/web/assets/js/script.js',
                src: [
                    'node_modules/jquery/dist/jquery.js',
                    //'node_modules/bootstrap/js/transition.js',
                    //'node_modules/bootstrap/js/alert.js',
                    //'node_modules/bootstrap/js/button.js',
                    //'node_modules/bootstrap/js/carousel.js',
                    'node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
                    'node_modules/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
                    'node_modules/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
                    //'node_modules/bootstrap/js/tooltip.js',
                    //'node_modules/bootstrap/js/popover.js',
                    //'node_modules/bootstrap/js/scrollspy.js',
                    //'node_modules/bootstrap/js/tab.js',
                    //'node_modules/bootstrap/js/affix.js',
                    'node_modules/bootstrap-select/dist/js/bootstrap-select.js',
                    'node_modules/bootstrap-select/dist/js/i18n/defaults-ru_RU.js',
                    'backend/assets/js/yii.js',
                    'backend/assets/js/yii.activeForm.js',
                    'backend/assets/js/yii.gridView.js',
                    'backend/assets/js/yii.validation.js',
                    'vendor/yiidoc/yii2-redactor/assets/redactor.js',
                    'node_modules/vue/dist/vue.min.js',
                    'backend/assets/js/show-modal.js',
                ]
            }
        },
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['node_modules/ubuntu-fontface/fonts/*'],
                        dest: 'frontend/web/assets/fonts/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: ['frontend/assets/img/*'],
                        dest: 'frontend/web/assets/img/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: ['node_modules/bootstrap-sass/assets/fonts/bootstrap/*'],
                        dest: 'backend/web/assets/fonts/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: ['vendor/yiidoc/yii2-redactor/assets/*.eot'],
                        dest: 'backend/web/assets/fonts/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: ['backend/assets/images/*'],
                        dest: 'backend/web/assets/images/',
                        filter: 'isFile'
                    },
                ]
            }
        },
        uglify: {
            options: {
                mangle: true,
                sourceMap: true
            },
            all: {
                files: {
                    'frontend/web/assets/js/script.min.js': 'frontend/web/assets/js/script.js'
                }
            }
        },
        penthouse: {
            extract: {
                outfile: 'frontend/web/assets/css/critical.css',
                css: 'frontend/web/assets/css/style.min.css',
                url: 'https://promokashka.com',
                width: 1200,
                height: 768,
                skipErrors: false
            }
        },
        watch: {
            options: {livereload: false, spawn: false},
            styles: {
                files: ["frontend/assets/scss/**/*.scss"],
                tasks: ["sass", "cssmin:frontend"],
            },
            scripts: {
                files: ["frontend/assets/js/**/*.js"],
                tasks: ["concat", "uglify"],
            }
        },
    });

    // Plugin loading
    grunt.loadNpmTasks("grunt-svgmin");
    grunt.loadNpmTasks("grunt-grunticon");
    grunt.loadNpmTasks("grunt-sass");
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-penthouse');

    // Task definition
    grunt.registerTask('default', ['svgmin', 'grunticon', 'sass', 'cssmin:frontend', 'concat', 'uglify', 'copy']);
    grunt.registerTask("dev", ["default", "watch"]);
};