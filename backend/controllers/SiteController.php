<?php

namespace backend\controllers;

use backend\models\LoginForm;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    public function actions(): array
    {
        return [
            'error' => ErrorAction::class,
        ];
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'denyCallback' => function () {
                    \Yii::$app->user->loginRequired();
                },
                'only' => ['index', 'upload'],
                'rules' => [
                    ['actions' => ['index', 'upload'], 'allow' => true, 'roles' => ['@']],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        }

        return $this->render('login', ['model' => $model]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionUpload()
    {
        $uploadFile = UploadedFile::getInstanceByName('file');
        if (!$uploadFile) {
            throw new BadRequestHttpException();
        }
        $fileName = sha1_file($uploadFile->tempName);
        [2 => $type, 'mime' => $mime] = getimagesize($uploadFile->tempName);
        $extension = image_type_to_extension($type);
        $fullFileName = "{$fileName}{$extension}";
        $hash = base64_encode(\Yii::$app->params['yandex']['login'].':'.\Yii::$app->params['yandex']['password']);
        $client = new Client(['baseUrl' => 'https://webdav.yandex.ru']);
        $response = $client->createRequest()
            ->setMethod('put')
            ->setUrl("v2/{$fullFileName}")
            ->addHeaders(['Authorization' => "Basic {$hash}", 'Content-type' => $mime])
            ->setContent(file_get_contents($uploadFile->tempName))
            ->send();

        if ($response->getIsOk()) {
            return $this->asJson(
                [
                    'filelink' => Url::to(['image/view', 'name' => $fullFileName]),
                    'filename' => $fullFileName,
                ]
            );
        }
        \Yii::error($response);
        throw new BadRequestHttpException();
    }
}