<?php

namespace backend\controllers;

use backend\models\PostSearch;
use common\models\Post;
use common\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PostController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'denyCallback' => function () {
                    \Yii::$app->user->loginRequired();
                },
                'rules' => [
                    ['actions' => ['index'], 'allow' => true, 'roles' => [User::POST_LIST]],
                    ['actions' => ['create'], 'allow' => true, 'roles' => [User::POST_CREATE]],
                    ['actions' => ['update'], 'allow' => true, 'roles' => [User::POST_UPDATE]],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new Post();
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var Post $model */
        $model = Post::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Пост не найдена');
        }
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', ['model' => $model]);
    }
}
