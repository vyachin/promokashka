<?php

namespace backend\controllers;

use backend\models\OfferSearch;
use common\models\Offer;
use common\models\Shop;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class OfferController extends Controller
{
    /**
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->can(User::OFFER_LIST)) {
            \Yii::$app->user->loginRequired();
        }

        $searchModel = new OfferSearch();
        if (\Yii::$app->user->can(User::OFFER_EDITOR_LIST)) {
            $searchModel->setScenario(OfferSearch::SCENARIO_EDITOR_LIST);
        }
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        /** @var Offer $model */
        $model = Offer::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Предложение не найдено');
        }
        if (!\Yii::$app->user->can(User::OFFER_UPDATE, ['model' => $model])) {
            \Yii::$app->user->loginRequired();
        }
        if (\Yii::$app->user->can(User::OFFER_EDITOR_UPDATE, ['model' => $model])) {
            $model->setScenario(Offer::SCENARIO_EDITOR_UPDATE);
        }
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return '';
        }

        return $this->renderPartial(
            '_form',
            [
                'model' => $model,
                'statuses' => static::getOfferStatuses($model->getScenario()),
                'shops' => static::getShopList(),
            ]
        );
    }

    /**
     * @param $scenario
     * @return array
     */
    private static function getOfferStatuses($scenario)
    {
        $list = Offer::getStatusList();
        if ($scenario == Offer::SCENARIO_EDITOR_UPDATE) {
            unset($list[Offer::STATUS_DELETED]);
            unset($list[Offer::STATUS_ACTIVE]);
        }

        return $list;
    }

    /**
     * @return array
     */
    private static function getShopList()
    {
        return ArrayHelper::map(
            Shop::find()->orderBy(['name' => SORT_ASC])->where(['status' => Shop::STATUS_ACTIVE])->all(),
            'id',
            'name'
        );
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionView($id)
    {
        /** @var Offer $model */
        $model = Offer::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Предложение не найдено');
        }
        if (!\Yii::$app->user->can(User::OFFER_VIEW, ['model' => $model])) {
            \Yii::$app->user->loginRequired();
        }

        return $this->renderPartial('_view', ['model' => $model]);
    }

    /**
     * @return array
     */
    public function actionGetWork()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var Offer $model */
        $model = Offer::findOne(\Yii::$app->request->post('id'));
        if (!$model) {
            return ['success' => false];
        }
        if (!\Yii::$app->user->can(User::OFFER_GET_WORK, ['model' => $model])) {
            return ['success' => false];
        }

        return ['success' => $model->getToWork()];
    }
}
