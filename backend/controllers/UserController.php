<?php

namespace backend\controllers;

use backend\models\UserSearch;
use common\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class UserController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'denyCallback' => function () {
                    \Yii::$app->user->loginRequired();
                },
                'rules' => [
                    ['actions' => ['index'], 'allow' => true, 'roles' => [User::USER_LIST]],
                    ['actions' => ['create'], 'allow' => true, 'roles' => [User::USER_CREATE]],
                    ['actions' => ['update'], 'allow' => true, 'roles' => [User::USER_UPDATE]],
                    ['actions' => ['login'], 'allow' => true, 'roles' => [User::USER_LOGIN]],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new User();
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return '';
        }

        return $this->renderPartial('_form', ['model' => $model]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var User $model */
        $model = User::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Пользователь не найден');
        }
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return '';
        }

        return $this->renderPartial('_form', ['model' => $model]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionLogin($id)
    {
        /** @var User $model */
        $model = User::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Пользователь не найден');
        }
        \Yii::$app->user->login($model);
        return $this->goHome();
    }
}
