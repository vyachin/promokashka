<?php

namespace backend\controllers;

use backend\models\SeoSearch;
use common\models\Seo;
use common\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SeoController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'denyCallback' => function () {
                    \Yii::$app->user->loginRequired();
                },
                'rules' => [
                    ['actions' => ['index'], 'allow' => true, 'roles' => [User::SEO_LIST]],
                    ['actions' => ['create'], 'allow' => true, 'roles' => [User::SEO_CREATE]],
                    ['actions' => ['update'], 'allow' => true, 'roles' => [User::SEO_UPDATE]],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SeoSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new Seo();
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return '';
        }

        return $this->renderPartial('_form', ['model' => $model]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var Seo $model */
        $model = Seo::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('SEO не найдена');
        }
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return '';
        }

        return $this->renderPartial('_form', ['model' => $model]);
    }
}
