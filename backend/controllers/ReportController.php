<?php

namespace backend\controllers;

use backend\models\OffersReport;
use common\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;

class ReportController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'denyCallback' => function () {
                    \Yii::$app->user->loginRequired();
                },
                'rules' => [
                    ['actions' => ['offers'], 'allow' => true, 'roles' => [User::ROLE_ADMIN]],
                ],
            ],
        ];
    }

    public function actionOffers()
    {
        $model = new OffersReport();
        $model->search(\Yii::$app->request->queryParams);

        return $this->render('offers', ['model' => $model]);
    }
}
