<?php

namespace backend\controllers;

use backend\models\ShopSearch;
use common\models\Shop;
use common\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ShopController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'denyCallback' => function () {
                    \Yii::$app->user->loginRequired();
                },
                'rules' => [
                    ['actions' => ['index'], 'allow' => true, 'roles' => [User::SHOP_LIST]],
                    ['actions' => ['create'], 'allow' => true, 'roles' => [User::SHOP_CREATE]],
                    ['actions' => ['update'], 'allow' => true, 'roles' => [User::SHOP_UPDATE]],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ShopSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new Shop();
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return '';
        }

        return $this->renderPartial('_form', ['model' => $model]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var Shop $model */
        $model = Shop::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Магазин не найден');
        }
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return '';
        }

        return $this->renderPartial('_form', ['model' => $model]);
    }
}
