<?php

namespace backend\controllers;

use backend\models\CategorySearch;
use common\models\Category;
use common\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CategoryController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'denyCallback' => function () {
                    \Yii::$app->user->loginRequired();
                },
                'rules' => [
                    ['actions' => ['index'], 'allow' => true, 'roles' => [User::CATEGORY_LIST]],
                    ['actions' => ['create'], 'allow' => true, 'roles' => [User::CATEGORY_CREATE]],
                    ['actions' => ['update'], 'allow' => true, 'roles' => [User::CATEGORY_UPDATE]],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new Category();
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return '';
        }

        return $this->renderPartial('_form', ['model' => $model]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var Category $model */
        $model = Category::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Категория не найдена');
        }
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return '';
        }

        return $this->renderPartial('_form', ['model' => $model]);
    }
}
