(function ($) {
    $.fn.toggleText = function (first, second) {
        this.text(this.text() == first ? second : first);
    }
})(jQuery);

(function ($) {
    var methods = {
        options: {url: ''},
        init: function (_options) {
            methods.options = $.extend(methods.options, _options || {});
            var self = this;
            methods.beforeShow(self);
            self.modal();
            self.on('hidden.bs.modal', function () {
                self.remove();
            });
        },
        beforeShow: function (_modal) {
            var copyButton = $('.js-copy', _modal);
            var clip = new ZeroClipboard(copyButton);
            clip.on('ready', function () {
                copyButton.prop('disabled', false);
                clip.on('aftercopy', function () {
                    ga && ga('send', 'event', 'CopyCode', location.pathname);
                });
            });
            VK && VK.Widgets.Like("vk_like_offer", {type: "button", pageUrl: methods.options.url});
            FB && FB.XFBML.parse(_modal[0]);
            OK && OK.CONNECT.insertShareWidget("ok_like_offer", methods.options.url, "{width:170,height:30,st:'straight',sz:20,ck:3}");
            twttr && twttr.widgets.load();
            gapi && gapi.plusone.go(_modal[0]);
            _modal.on("click", ".js-shop-link", function () {
                ga && ga('send', 'event', 'GoShop', $(this).prop("href"));
                return true;
            });
        }
    };

    $.fn.detailOffer = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для detailOffer');
        }
    };
})(jQuery);

(function ($) {
    var methods = {
        init: function () {
            this.click(function () {
                var self = $(this);
                ga && ga('send', 'pageview', self.prop('href'));
                window.open(self.data('href'));
            });
        }
    };

    $.fn.loadOffer = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для loadOffer');
        }
    };
})(jQuery);

(function ($) {
    var methods = {
        init: function () {
            this.on('click', 'a', function () {
                var item = $(this);
                var listItem = item.parent();
                $('li.active', listItem.parent()).removeClass('active');
                listItem.addClass('active');
                if (item.data('type')) {
                    $('.js-offer').hide();
                    $('.js-offer[data-type=\'' + item.data('type') + '\']').show();

                } else {
                    $('.js-offer').show();
                }
                ga && ga('send', 'event', 'Filter', item.data('type'));
            });
        }
    };

    $.fn.offerTypeFilter = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для offerTypeFilter');
        }
    };
})(jQuery);

(function ($) {
    var methods = {
        init: function (_options) {
            var options = $.extend({url: ''}, _options || {});
            var self = this;
            var searchResult = $('#search-results');
            this.keyup(function () {
                setTimeout(function () {
                    methods.search(options.url, searchResult, self.val());
                }, 300);
            });
            this.focusout(function () {
                setTimeout(function () {
                    searchResult.addClass('hidden');
                }, 300);
            });
            this.focusin(function () {
                setTimeout(function () {
                    methods.search(options.url, searchResult, self.val());
                }, 300);
            });
        },
        search: function (url, searchResult, query) {
            $.ajax({
                url: url,
                type: 'get',
                data: {
                    q: query
                },
                success: function (e) {
                    searchResult.html(e);
                    if (e.length) {
                        searchResult.removeClass('hidden');
                    } else {
                        searchResult.addClass('hidden');
                    }
                }
            });
            if (ga) {
                ga('send', 'event', 'Search', query);
                ga('send', 'pageview', '/search?q=' + query);
            }
        }
    };

    $.fn.SearchQuery = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для SearchQuery');
        }
    };
})(jQuery);