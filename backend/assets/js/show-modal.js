(function ($) {
    var methods = {
        init: function (_options) {
            var options = $.extend({
                complete: function () {
                    window.location.reload();
                },
                beforeShow: function () {
                }
            }, _options || {});
            this.click(function () {
                var url = $(this).attr('href');
                methods.show(url, options);
                return false;
            });
        },
        show: function (url, options) {
            $.ajax({
                type: 'GET',
                url: url,
                cache: false
            }).done(function (result) {
                methods.modal($(result), options);
            }).fail(function (e) {
                methods.showError(e);
            });
        },
        modal: function (_modal, options) {
            if (options && options.beforeShow) {
                options.beforeShow(_modal);
            }
            _modal.modal(); //bootstrap modal
            _modal.on('hidden.bs.modal', function () {
                $(this).remove();
            }).on('click', '.js-submit', function () {
                var form = $('form', _modal);
                $.ajax({
                    type: form[0].method,
                    url: form[0].action,
                    data: form.serialize()
                }).done(function (result) {
                    _modal.modal('hide');
                    if (result) {
                        methods.modal($(result), options);
                    } else {
                        if (options && options.complete) {
                            options.complete();
                        }
                    }
                }).fail(function (e) {
                    methods.showError(e);
                });
            });
        },
        showError: function (e) {
            if (e.status == 403)
                alert('Операция запрещена');
            else
                alert(e.statusText);
        }
    };

    $.fn.ShowModal = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для ShowModal');
        }
    };
})(jQuery);


(function ($) {
    var methods = {
        init: function (_options) {
            var options = $.extend({url: '', data: {}}, _options || {});
            this.ShowModal({
                beforeShow: function (_modal) {
                    _modal.on('click', '.js-get-work', function () {
                        var data = {id: $(this).data('offer-id')};
                        data[methods.getCsrfParam()] = methods.getCsrfToken();
                        $.ajax({
                            url: options.url,
                            type: 'POST',
                            data: data,
                        }).done(function (e) {
                            if (e.success) {
                                _modal.modal('hide');
                                window.location.reload();
                            } else {
                                alert('Произошла ошибка во время взятия в работу');
                            }
                        });
                    })
                }
            });
        },
        getCsrfParam: function () {
            return $('meta[name=csrf-param]').attr('content');
        },
        getCsrfToken: function () {
            return $('meta[name=csrf-token]').attr('content');
        }
    };

    $.fn.ViewOffer = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для ViewOffer');
        }
    };
})(jQuery);


function getCsrfParam() {
    return $("meta[name=csrf-param]").attr("content");
}

function getCsrfToken() {
    return $("meta[name=csrf-token]").attr("content");
}

function categoryModal(_modal) {
    jQuery(".selectpicker", _modal).selectpicker();
    var el = _modal.find('#js-image');
    var app = new Vue({
        el: el.get(0),
        data: {
            image: el.data("image"),
            imageUrl: el.data("image-url"),
        },
        methods: {
            removeImage() {
                this.image = "";
                this.imageUrl = "";
            },
            uploadImage() {
                var self = this;
                var input = $("<input type=\"file\">");
                input.change(function () {
                    let formData = new FormData();
                    formData.append("file", this.files[0]);
                    formData.append(getCsrfParam(), getCsrfToken());
                    $.ajax({
                        url: "/site/upload",
                        type: "post",
                        contentType: false,
                        processData: false,
                        data: formData,
                        dataType: "json"
                    }).always(function () {
                        input.remove();
                    }).done(function (r) {
                        self.image = r.filename;
                        self.imageUrl = r.filelink;
                    });
                }).click();
            },
        }
    })
}