<?php

namespace backend\assets;

use yii\web\AssetBundle;

class ModalAsset extends AssetBundle
{
    public $baseUrl = '';
    public $js = [
        'js/show-modal.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'backend\assets\BootstrapSelectAsset',
    ];
}
