<?php

namespace backend\assets;

use yii\web\AssetBundle;

class BootstrapSelectAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-select/dist';
    public $css = ['css/bootstrap-select.min.css'];
    public $js = [
        'js/bootstrap-select.min.js',
        'js/i18n/defaults-ru_RU.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
