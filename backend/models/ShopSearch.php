<?php

namespace backend\models;

use common\models\Offer;
use common\models\Shop;
use yii\data\ActiveDataProvider;

class ShopSearch extends Shop
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    'name',
                    'status',
                    'description',
                    'domain',
                    'admitad_id',
                    'cityads_id',
                    'seo_title',
                    'seo_description',
                    'seo_keywords',
                ],
                'safe',
            ],
        ];
    }

    public function formName(): string
    {
        return '';
    }

    public function search($params)
    {
        $query = static::find();
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['LIKE', 'name', $this->name]);
            $query->andFilterWhere(['LIKE', 'description', $this->description]);
            $query->andFilterWhere(['LIKE', 'domain', $this->domain]);
            $query->andFilterWhere(['LIKE', 'seo_title', $this->seo_title]);
            $query->andFilterWhere(['LIKE', 'seo_description', $this->seo_description]);
            $query->andFilterWhere(['LIKE', 'seo_keywords', $this->seo_keywords]);
            $query->andFilterWhere(
                [
                    'status' => $this->status,
                    'admitad_id' => $this->admitad_id,
                    'cityads_id' => $this->cityads_id,
                    'actionpay_id' => $this->actionpay_id,
                ]
            );
        }
        $query->leftJoin(
            '(SELECT shop_id, count(*) as offerCount FROM {{%offer}} o GROUP BY shop_id) offers',
            'offers.shop_id = id'

        );
        $query->leftJoin(
            '(SELECT shop_id, count(*) as changedOfferCount FROM {{%offer}} o WHERE o.author_id IS NOT NULL GROUP BY shop_id) offers2',
            'offers2.shop_id = id'
        );
        $query->leftJoin(
            '(SELECT shop_id, count(*) as activeOfferCount FROM {{%offer}} o WHERE o.status != :deleted GROUP BY shop_id) offers1',
            'offers1.shop_id = id',
            [':deleted' => Offer::STATUS_DELETED]
        );


        return new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'name',
                        'domain',
                        'offerCount' => [
                            'asc' => ['offers.offerCount' => SORT_ASC],
                            'desc' => ['offers.offerCount' => SORT_DESC],
                        ],
                        'activeOfferCount' => [
                            'asc' => ['offers1.activeOfferCount' => SORT_ASC],
                            'desc' => ['offers1.activeOfferCount' => SORT_DESC],
                        ],
                        'changedOfferCount' => [
                            'asc' => ['offers2.changedOfferCount' => SORT_ASC],
                            'desc' => ['offers2.changedOfferCount' => SORT_DESC],
                        ],
                        'admitad_id',
                        'cityads_id',
                        'actionpay_id',
                        'status',
                        'created_at',
                        'updated_at',
                        'seo_title',
                        'seo_description',
                        'seo_keywords',
                    ],
                ],
            ]
        );
    }
}
