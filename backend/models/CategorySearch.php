<?php

namespace backend\models;

use common\models\Category;
use yii\data\ActiveDataProvider;

class CategorySearch extends Category
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['name', 'status', 'seo_title', 'seo_description', 'seo_keywords',], 'safe'],
        ];
    }

    public function formName(): string
    {
        return '';
    }

    public function search($params)
    {
        $query = static::find();
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['LIKE', 'name', $this->name]);
            $query->andFilterWhere(['LIKE', 'seo_title', $this->seo_title]);
            $query->andFilterWhere(['LIKE', 'seo_description', $this->seo_description]);
            $query->andFilterWhere(['LIKE', 'seo_keywords', $this->seo_keywords]);
            $query->andFilterWhere(['status' => $this->status,]);
        }

        return new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'id',
                        'name',
                        'status',
                        'created_at',
                        'updated_at',
                        'seo_title',
                        'seo_description',
                        'seo_keywords',
                    ],
                ],
            ]
        );
    }
}
