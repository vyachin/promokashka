<?php

namespace backend\models;

use common\models\Post;
use yii\data\ActiveDataProvider;

class PostSearch extends Post
{
    public function rules(): array
    {
        return [
            [['id', 'title', 'body', 'status', 'seo_title', 'seo_description', 'seo_keywords',], 'safe'],
        ];
    }

    public function formName(): string
    {
        return '';
    }

    public function search($params)
    {
        $query = static::find();
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['LIKE', 'title', $this->title]);
            $query->andFilterWhere(['LIKE', 'body', $this->body]);
            $query->andFilterWhere(['LIKE', 'seo_description', $this->seo_description]);
            $query->andFilterWhere(['LIKE', 'seo_title', $this->seo_title]);
            $query->andFilterWhere(['LIKE', 'seo_keywords', $this->seo_keywords]);
            $query->andFilterWhere(['status' => $this->status,]);
            $query->andFilterWhere(['id' => $this->id,]);
        }

        return new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'id',
                        'title',
                        'status',
                        'created_at',
                        'updated_at',
                        'published_at',
                        'seo_description',
                        'seo_keywords',
                        'seo_title',
                    ],
                ],
            ]
        );
    }
}
