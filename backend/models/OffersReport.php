<?php

namespace backend\models;

use common\models\Offer;
use yii\base\Model;

class OffersReport extends Model
{
    public $created_from;
    public $created_to;

    public function rules(): array
    {
        return [[['created_from', 'created_to'], 'date', 'format' => 'yyyy-MM-dd']];
    }

    public function formName(): string
    {
        return '';
    }

    public function getOffers()
    {
        return Offer::find()->with(['shop'])->andWhere('author_id IS NOT NULL')
            ->andFilterWhere(['>=', 'created_at', $this->created_from])
            ->andFilterWhere(['<=', 'created_at', $this->created_to])
            ->orderBy(['shop_id' => SORT_ASC])
            ->all();
    }

    public function search($queryParams)
    {
        $this->load($queryParams);
        $this->validate();
    }
}