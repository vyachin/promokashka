<?php

namespace backend\models;

use common\models\Offer;
use common\models\Shop;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;

class OfferSearch extends Offer
{
    const SCENARIO_EDITOR_LIST = 'scenario-editor-list';

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    'id',
                    'name',
                    'description',
                    'status',
                    'type',
                    'admitad_id',
                    'cityads_id',
                    'actionpay_id',
                    'author_id',
                    'shop_id',
                ],
                'safe',
                'on' => self::SCENARIO_DEFAULT,
            ],
            [['name', 'description'], 'safe', 'on' => self::SCENARIO_EDITOR_LIST],
        ];
    }

    public function formName(): string
    {
        return '';
    }

    public function search($params)
    {
        $isEditor = $this->getScenario() == self::SCENARIO_EDITOR_LIST;

        $query = static::find()->alias('o')->innerJoinWith(
            [
                'shop' => function (ActiveQuery $query) use ($isEditor) {
                    $query->alias('s')->andWhere(['s.status' => Shop::STATUS_ACTIVE]);
                    if ($isEditor) {
                        $query->innerJoinWith(
                            [
                                'users' => function (ActiveQuery $q) {
                                    $q->andWhere(['{{%user}}.id' => \Yii::$app->user->id]);
                                },
                            ]
                        );
                    }
                },
            ]
        );
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['LIKE', 'o.name', $this->name]);
            $query->andFilterWhere(['LIKE', 'o.description', $this->description]);
            $query->andFilterWhere(
                [
                    'o.id' => $this->id,
                    'o.status' => $this->status,
                    'o.type' => $this->type,
                    'o.admitad_id' => $this->admitad_id,
                    'o.cityads_id' => $this->cityads_id,
                    'o.actionpay_id' => $this->actionpay_id,
                    'o.author_id' => $this->author_id,
                    'o.shop_id' => $this->shop_id,
                ]
            );
        }

        if ($isEditor) {
            $countOffers = 5; //round((time() - 1451595600) / 604800); // кол-во отредактированных промокодов
            $shops = (new Query())->select('shop_id')->from('{{%offer}}')->andWhere('author_id IS NOT NULL')
                ->groupBy('shop_id')->having('count(*) >= '.$countOffers)->column();
            $query->andWhere(
                [
                    'OR',
                    [
                        'AND',
                        ['o.author_id' => \Yii::$app->user->id],
                        ['IN', 'o.status', [self::STATUS_READY, self::STATUS_DRAFT]],
                    ],
                    [
                        'AND',
                        'o.author_id IS NULL',
                        ['o.status' => self::STATUS_DRAFT],
                        ['NOT', ['o.shop_id' => $shops]],
                    ],
                ]
            );
            $query->andWhere(['>=', 'o.end_at', date('Y-m-d H:i:s')]);
        }

        return new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'id',
                        'shop.name' => [
                            'asc' => ['shop_id' => SORT_ASC],
                            'desc' => ['shop_id' => SORT_DESC],
                        ],
                        'name',
                        'typeName' => [
                            'asc' => ['type' => SORT_ASC],
                            'desc' => ['type' => SORT_DESC],
                        ],
                        'status',
                        'content_unique_percent',
                        'created_at',
                        'updated_at',
                        'count_used',
                        'count_like',
                        'count_dislike',
                        'author.username' => [
                            'asc' => ['author_id' => SORT_ASC],
                            'desc' => ['author_id' => SORT_DESC],
                        ],
                        'descriptionLength' => [
                            'asc' => ['char_length(o.description)' => SORT_ASC],
                            'desc' => ['char_length(o.description)' => SORT_DESC],
                        ],
                    ],
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ],
                ],
            ]
        );
    }
}
