<?php

namespace backend\models;

use common\models\User;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['email'], 'safe'],
        ];
    }

    public function formName(): string
    {
        return '';
    }

    public function search($params)
    {
        $query = static::find();
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['LIKE', 'email', $this->email]);
            $query->andFilterWhere(['LIKE', 'username', $this->username]);
        }

        return new ActiveDataProvider(['query' => $query]);
    }
}
