<?php

namespace backend\models;

use common\models\Seo;
use yii\data\ActiveDataProvider;

class SeoSearch extends Seo
{
    public function rules(): array
    {
        return [
            [['page', 'seo_title', 'seo_description', 'seo_keywords',], 'safe'],
        ];
    }

    public function formName(): string
    {
        return '';
    }

    public function search($params)
    {
        $query = static::find();
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['page' => $this->page]);
            $query->andFilterWhere(['LIKE', 'seo_title', $this->seo_title]);
            $query->andFilterWhere(['LIKE', 'seo_description', $this->seo_description]);
            $query->andFilterWhere(['LIKE', 'seo_keywords', $this->seo_keywords]);
        }

        return new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'attributes' => [
                        'id',
                        'page',
                        'seo_title',
                        'seo_description',
                        'seo_keywords',
                        'created_at',
                    ],
                ],
            ]
        );
    }
}
