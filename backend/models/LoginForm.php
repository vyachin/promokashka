<?php

namespace backend\models;

use common\models\User;
use yii\base\Model;

class LoginForm extends Model
{
    public $email;
    public $password;
    /** @var User */
    private $_user = null;

    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @throws \yii\base\InvalidConfigException
     */
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Не корректный email / пароль.');
                \Yii::warning('Не корректный email "' . $this->email . '" или пароль "' . $this->password . '"', 'login');
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return \Yii::$app->user->login($this->getUser(), 86400);
        }

        return false;
    }
}