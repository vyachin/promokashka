<?php

namespace backend\widgets;

use backend\assets\BootstrapDateTimePickerAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\InputWidget;

class DatePicker extends InputWidget
{
    public $format;

    public function init()
    {
        parent::init();
        $this->options['id'] = $this->getId();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerClientScript();
        if ($this->hasModel()) {
            echo Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textInput($this->name, $this->value, $this->options);
        }
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        $view = $this->getView();
        BootstrapDateTimePickerAsset::register($view);
        $params = Json::encode(['format' => $this->format]);
        $view->registerJs("$('#{$this->options['id']}').datetimepicker({$params});", View::POS_READY);
    }
}