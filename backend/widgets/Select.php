<?php

namespace backend\widgets;

use backend\assets\BootstrapSelectAsset;
use yii\helpers\Html;
use yii\widgets\InputWidget;

class Select extends InputWidget
{
    public $items;

    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, 'selectpicker');
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerClientScript();
        if ($this->hasModel()) {
            echo Html::activeDropDownList($this->model, $this->attribute, $this->items, $this->options);
        } else {
            echo Html::dropDownList($this->name, $this->value, $this->items, $this->options);
        }
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        BootstrapSelectAsset::register($this->getView());
    }
}