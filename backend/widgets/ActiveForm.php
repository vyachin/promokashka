<?php

namespace backend\widgets;

class ActiveForm extends \yii\bootstrap\ActiveForm
{
    public $enableClientValidation = false;
    public $enableAjaxValidation = false;
    public $enableClientScript = false;
}