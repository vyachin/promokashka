<?php

return [
    'id' => 'promokashka-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'name' => 'Admin',
    'controllerMap' => [
        'image' => \common\controllers\ImageController::class,
    ],
    'components' => [
        'assetManager' => [
            'bundles' => false,
        ],
        'user' => [
            'identityClass' => \common\models\User::class,
            'enableAutoLogin' => true,
            'on afterLogin' => 'common\models\User::onAfterLogin',
        ],
        'errorHandler' => [
            'errorAction' => '/site/error',
        ],
        'log' => [
            'targets' => [
                'file' => [
                    'logFile' => '@runtime/logs/backend.log',
                ],
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                'image/<width>x<height>/<name>' => 'image/get',
                'image/<name>' => 'image/view',
            ],
        ],
    ],
];
