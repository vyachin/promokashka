<?php

require_once __DIR__.'/../../common/bootstrap.php';

$config = \yii\helpers\ArrayHelper::merge(
    require __DIR__.'/../../common/config/main.php',
    require __DIR__.'/../../common/config/_local.php',
    require __DIR__.'/../config/main.php',
    require __DIR__.'/../config/_local.php'
);

$app = new \yii\web\Application($config);
$app->run();
