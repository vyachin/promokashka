<?php
/**
 * @var $this yii\web\View
 * @var $searchModel common\models\UserSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use backend\assets\ModalAsset;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Список пользователей';
ModalAsset::register($this);
$this->registerJs(
    'jQuery(".js-show-modal").ShowModal({beforeShow: function (_modal) { jQuery(".selectpicker", _modal).selectpicker(); } });'
);
?>
    <h2 class="page-title"><?= $this->title ?></h2>
<?php if (\Yii::$app->user->can(User::USER_CREATE)): ?>
    <?= Html::a('Новый пользователь', ['create'], ['class' => 'btn btn-sm btn-primary js-show-modal']) ?>
<?php endif ?>

<?= $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget(
    [
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'pager' => ['maxButtonCount' => 20],
        'columns' => [
            'id',
            'username',
            'email',
            'last_login_at:datetime',
            'created_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {login}',
                'buttons' => [
                    'update' => function ($url) {
                        return \Yii::$app->user->can(User::USER_UPDATE) ? Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>',
                            $url, ['class' => 'js-show-modal', 'title' => 'Редактировать']) : '';
                    },
                    'login' => function ($url, User $model) {
                        return \Yii::$app->user->can(User::USER_LOGIN) ? Html::a('<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>',
                            $url, ['title' => 'Войти как ' . $model->username]) : '';
                    },
                ],
            ],
        ],
    ]
);