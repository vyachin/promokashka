<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model common\models\UserSearch
 * @var $form yii\widgets\ActiveForm
 */

$this->registerJs('jQuery("body").on("click", ".js-filter", function () { jQuery("#js-user-search").toggle(); });');
?>
<?= Html::a('Фильтр', '#', ['class' => 'btn btn-sm btn-success js-filter']) ?>
<div id="js-user-search" style="display: none;">
    <?php $form = ActiveForm::begin(['action' => ['index'], 'method' => 'get']); ?>
    <section class="item">
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'email')->textInput(['class' => 'form-control input-sm']); ?>
            </div>
            <div class="col-sm-1">
                <label for="" class="control-label">&nbsp;</label>
                <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
            <div class="col-sm-1">
                <label for="" class="control-label">&nbsp;</label>
                <?= Html::resetButton('Сброс', ['class' => 'btn btn-sm btn-default']) ?>
            </div>
        </div>
    </section>
    <?php ActiveForm::end(); ?>
</div>