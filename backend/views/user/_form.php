<?php

use backend\widgets\Select;
use common\models\Shop;
use common\models\User;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 * @var yii\widgets\ActiveForm $form
 * @var string[] $shops
 */
?>

<?php Modal::begin(
    [
        'header' => '<h4 class="modal-title">' . ($model->isNewRecord ? 'Новый пользователь' : 'Редактирование пользователя') . '</h4>',
        'footer' =>
            Html::button('Закрыть', ['class' => 'btn btn-sm btn-default', 'data-dismiss' => 'modal']) .
            Html::button(
                $model->isNewRecord ? 'Добавить' : 'Сохранить',
                ['class' => 'btn btn-sm btn-primary js-submit']
            ),
    ]
); ?>
<?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
<?= $form->field($model, 'username')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'email')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'password')->passwordInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'passwordVerify')->passwordInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'roles')->widget(Select::class, [
    'options' => [
        'class' => 'form-control input-sm',
        'data-style' => 'btn-sm btn-default',
        'multiple' => true
    ],
    'items' => User::getRoleList(),
]) ?>
<?= $form->field($model, 'shop_list')->widget(
    Select::class,
    [
        'options' => ['class' => 'form-control input-sm', 'data-style' => 'btn-sm btn-default', 'multiple' => true],
        'items' => ArrayHelper::map(Shop::find()->active()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
    ]
) ?>

<?php ActiveForm::end(); ?>
<?php Modal::end() ?>