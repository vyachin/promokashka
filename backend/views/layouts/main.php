<?php
/* @var $this View */
/* @var $content string */

use common\helpers\AssetHelper;
use common\models\User;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\web\View;


$this->registerMetaTag(['charset' => Yii::$app->charset]);
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']);

$this->registerJsFile(AssetHelper::getUrl('js/script.js'));
$this->registerCssFile(AssetHelper::getUrl('css/style.css'));
foreach ([32, 96, 16] as $size) {
    $this->registerLinkTag([
        'rel' => 'icon',
        'type' => 'image/png',
        'sizes' => "{$size}x{$size}",
        'href' => AssetHelper::getUrl("images/favicon-{$size}x{$size}.png"),
    ]);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?php if (!YII_DEBUG): ?>
        <?= \common\widgets\RollbarWidger::widget(); ?>
    <?php endif; ?>
    <title><?= \Yii::$app->name ?> | <?= $this->title ?></title>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin(
        [
            'brandLabel' => \Yii::$app->name,
            'brandUrl' => \Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]
    );
    ?>
    <?php echo Nav::widget(
        [
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Главная', 'url' => ['/site/index']],
                [
                    'label' => 'Отчеты',
                    'items' => [
                        [
                            'label' => 'Список промкодов',
                            'url' => ['/report/offers'],
                            'visible' => \Yii::$app->user->can(User::ROLE_ADMIN),
                        ],
                    ],
                ],
                [
                    'label' => 'Списки',
                    'items' => [
                        [
                            'label' => 'Пользователи',
                            'url' => ['/user/index'],
                            'visible' => \Yii::$app->user->can(User::USER_LIST),
                        ],
                        [
                            'label' => 'Категории',
                            'url' => ['/category/index'],
                            'visible' => \Yii::$app->user->can(User::CATEGORY_LIST),
                        ],
                        [
                            'label' => 'Магазины',
                            'url' => ['/shop/index'],
                            'visible' => \Yii::$app->user->can(User::SHOP_LIST),
                        ],
                        [
                            'label' => 'SEO',
                            'url' => ['/seo/index'],
                            'visible' => \Yii::$app->user->can(User::SEO_LIST),
                        ],
                        [
                            'label' => 'Посты',
                            'url' => ['/post/index'],
                            'visible' => \Yii::$app->user->can(User::POST_LIST),
                        ],
                    ],
                ],
                [
                    'label' => 'Промокоды',
                    'url' => ['/offer/index'],
                    'visible' => \Yii::$app->user->can(User::OFFER_LIST),
                ],
                Yii::$app->user->isGuest ? (['label' => 'Войти', 'url' => ['/site/login']]) : ([
                    'label' => 'Выйти',
                    'url' => ['/site/logout'],
                ]),
            ],
        ]
    );
    NavBar::end();
    ?>

    <div class="container">
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::$app->name ?> <?= date('Y') ?></p>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
