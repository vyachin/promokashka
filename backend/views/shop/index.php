<?php
/**
 * @var $this yii\web\View
 * @var $searchModel backend\models\ShopSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use backend\assets\ModalAsset;
use common\helpers\Image;
use common\models\Shop;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Список магазинов';
ModalAsset::register($this);
$this->registerJs(
    'jQuery(".js-show-modal").ShowModal({beforeShow: categoryModal });'
);
?>
    <h2 class="page-title"><?= $this->title ?></h2>
<?php if (\Yii::$app->user->can(User::SHOP_CREATE)): ?>
    <?= Html::a('Новый магазин', ['create'], ['class' => 'btn btn-sm btn-primary js-show-modal']) ?>
<?php endif ?>

<?= $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget(
    [
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'pager' => ['maxButtonCount' => 20],
        'columns' => [
            [
                'format' => 'image',
                'value' => function (Shop $model) {
                    return Image::url($model->logo, 50, 20);
                },
            ],
            'name',
            'seo_title',
            'seo_description',
            'seo_keywords',
            'domain',
            'offerCount:integer:Total',
            'activeOfferCount:integer:Active',
            'changedOfferCount:integer:Edited',
            'admitad_id',
            'cityads_id',
            'actionpay_id',
            'rate',
            'status:shopStatusName',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url) {
                        return \Yii::$app->user->can(User::SHOP_UPDATE) ? Html::a(
                            '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>',
                            $url,
                            ['class' => 'js-show-modal', 'title' => 'Редактировать']
                        ) : '';
                    },
                ],
            ],
        ],
    ]
);