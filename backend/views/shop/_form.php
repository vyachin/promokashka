<?php

use backend\widgets\Select;
use common\models\User;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model common\models\Shop
 * @var $form yii\widgets\ActiveForm
 * @var $header string
 * @var $sources string[]
 */
?>

<?php Modal::begin(
    [
        'header' => '<h4 class="modal-title">'.($model->isNewRecord ? 'Новый магазин' : 'Редактирование магазина').'</h4>',
        'footer' =>
            Html::button('Закрыть', ['class' => 'btn btn-sm btn-default', 'data-dismiss' => 'modal']).
            Html::button(
                $model->isNewRecord ? 'Добавить' : 'Сохранить',
                ['class' => 'btn btn-sm btn-primary js-submit']
            ),
    ]
); ?>
<?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>

<?= $form->field($model, 'name')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'domain')->textInput(['class' => 'form-control input-sm']); ?>
    <div id="js-image" data-image="<?= $model->logo ?>"
         data-image-url="<?= \yii\helpers\Url::to(['image/view', 'name' => $model->logo]) ?>">
        <?= $form->field($model, 'logo')->hiddenInput(['v-model' => 'image']); ?>
        <img :src="imageUrl" width="200">
        <div>
            <button type="button" class="btn btn-sm btn-success" v-on:click="uploadImage()">Загрузить</button>
            <button type="button" class="btn btn-sm btn-warning" v-on:click="removeImage()">Удалить</button>
        </div>
    </div>
<?= $form->field($model, 'description')->textarea(['class' => 'form-control input-sm']); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'admitad_id')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'cityads_id')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'actionpay_id')->textInput(['class' => 'form-control input-sm']); ?>
        </div>
    </div>
<?= $form->field($model, 'seo_title')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'seo_description')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'seo_keywords')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'user_list')->widget(
    Select::class,
    [
        'options' => ['class' => 'form-control input-sm', 'data-style' => 'btn-sm btn-default', 'multiple' => true],
        'items' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
    ]
) ?>

<?= $form->field($model, 'status')->widget(
    Select::class,
    [
        'options' => ['class' => 'form-control input-sm', 'data-style' => 'btn-sm btn-default'],
        'items' => $model::getStatusList(),
    ]
) ?>

<?php ActiveForm::end(); ?>
<?php Modal::end() ?>