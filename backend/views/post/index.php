<?php
/**
 * @var $this yii\web\View
 * @var $searchModel backend\models\CategorySearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use common\models\User;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Список постов';
?>
    <h2 class="page-title"><?= $this->title ?></h2>
<?php if (\Yii::$app->user->can(User::POST_CREATE)): ?>
    <?= Html::a('Новый пост', ['create'], ['class' => 'btn btn-sm btn-primary js-show-modal']) ?>
<?php endif ?>

<?= $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget(
    [
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'pager' => ['maxButtonCount' => 20],
        'columns' => [
            'id',
            'title',
            'status:postStatusName',
            'created_at:datetime',
            'updated_at:datetime',
            'published_at:datetime',
            'user.email:text:Автор',
            [
                'class' => \yii\grid\ActionColumn::class,
                'template' => '{update}',
                'visibleButtons' => [
                    'update' => Yii::$app->user->can(User::POST_UPDATE),
                ],
                'buttons' => [
                    'update' => function ($url) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>',
                            $url,
                            ['title' => 'Редактировать']
                        );
                    },
                ],
            ],
        ],
    ]
);