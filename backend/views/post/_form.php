<?php

use backend\widgets\Select;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model common\models\Post
 * @var $form yii\widgets\ActiveForm
 */
$params = Json::encode(
    [
        'minHeight' => '500px',
        'imageUpload' => Url::to(['site/upload']),
        'uploadImageFields' => [Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,],
    ]
);
$this->registerJs('$("#js-redactor").redactor('.$params.');');
$image = $model->image ?: null;
$imageUrl = $model->image ? Url::to(['image/view', 'name' => $model->image]) : null;
$script = <<< END
function getCsrfParam() {
    return $("meta[name=csrf-param]").attr("content");
}

function getCsrfToken() {
    return $("meta[name=csrf-token]").attr("content");
}

var app = new Vue({
  el: '#js-image',
  data: {
    image: '{$image}',
    imageUrl: '{$imageUrl}',
  },
  methods: {
    removeImage() {
        this.image = '';
        this.imageUrl = '';
    },
    uploadImage() {
        var self = this;
        var input = $("<input type=\"file\">");
        input.change(function () {
            let formData = new FormData();
            formData.append("file", this.files[0]);
            formData.append(getCsrfParam(), getCsrfToken());
            $.ajax({
                url: "/site/upload",
                type: "post",
                contentType: false,
                processData: false,
                data: formData,
                dataType: "json"
            }).always(function () {
                input.remove();
            }).done(function (r) { 
                 self.image = r.filename;
                 self.imageUrl = r.filelink;
            });
        }).click();
    },
  }
})
END;

$this->registerJs($script);
?>

<?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
<?= $form->field($model, 'title')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'seo_title')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'seo_description')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'seo_keywords')->textInput(['class' => 'form-control input-sm']); ?>
<div id="js-image">
    <?= $form->field($model, 'image')->hiddenInput(['v-model' => 'image']); ?>
    <img :src="imageUrl" width="300">
    <div>
        <button type="button" class="btn btn-sm btn-success" v-on:click="uploadImage()">Загрузить</button>
        <button type="button" class="btn btn-sm btn-warning" v-on:click="removeImage()">Удалить</button>
    </div>
</div>
<?= $form->field($model, 'body')->textarea(['class' => 'form-control input-sm', 'id' => 'js-redactor'])->hint(
    'В качестве разделителя контента используем '.Html::encode(\common\models\Post::DELIMITER)
); ?>
<?= $form->field($model, 'status')->widget(
    Select::class,
    [
        'options' => ['class' => 'form-control input-sm', 'data-style' => 'btn-sm btn-default'],
        'items' => $model::getStatusList(),
    ]
) ?>
<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => 'btn btn-sm btn-success']) ?>
<?= Html::a('Отмена', ['index'], ['class' => 'btn btn-sm btn-success']) ?>
<?php ActiveForm::end(); ?>
