<?php

use backend\widgets\Select;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model \backend\models\PostSearch
 * @var $form yii\widgets\ActiveForm
 */

$this->registerJs('jQuery("body").on("click", ".js-filter", function () { jQuery(".js-post-search").toggle(); });');
?>
<?= Html::a('Фильтр', '#', ['class' => 'btn btn-sm btn-success js-filter']) ?>
<div class="js-post-search" style="display: none;">
    <?php $form = ActiveForm::begin(['action' => ['index'], 'method' => 'get',]); ?>
    <section class="item">
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'title')->textInput(['class' => 'form-control input-sm']); ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'body')->textInput(['class' => 'form-control input-sm']); ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'seo_title')->textInput(['class' => 'form-control input-sm']); ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'seo_description')->textInput(['class' => 'form-control input-sm']); ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'seo_keywords')->textInput(['class' => 'form-control input-sm']); ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'status')->widget(
                    Select::class,
                    [
                        'options' => [
                            'class' => 'form-control input-sm',
                            'prompt' => 'Все',
                            'data-style' => 'btn-sm btn-default',
                        ],
                        'items' => $model::getStatusList(),
                    ]
                ) ?>
            </div>
            <div class="col-sm-1">
                <label for="" class="control-label">&nbsp;</label>
                <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
            <div class="col-sm-1">
                <label for="" class="control-label">&nbsp;</label>
                <?= Html::a('Сброс', ['post/index'], ['class' => 'btn btn-sm btn-default']) ?>
            </div>
        </div>
    </section>
    <?php ActiveForm::end(); ?>
</div>