<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\Post
 */
$this->title = 'Редактирование поста: '.$model->title;

echo $this->render('_form', ['model' => $model]);