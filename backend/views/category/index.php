<?php
/**
 * @var $this yii\web\View
 * @var $searchModel backend\models\CategorySearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use backend\assets\ModalAsset;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Список категорий';
ModalAsset::register($this);
$this->registerJs(
    'jQuery(".js-show-modal").ShowModal({beforeShow: categoryModal });'
);
?>
    <h2 class="page-title"><?= $this->title ?></h2>
<?php if (\Yii::$app->user->can(User::CATEGORY_CREATE)): ?>
    <?= Html::a('Новая категория', ['create'], ['class' => 'btn btn-sm btn-primary js-show-modal']) ?>
<?php endif ?>

<?= $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget(
    [
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'pager' => ['maxButtonCount' => 20],
        'columns' => [
            'id',
            [
                'attribute' => 'icon',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(Url::to(['image/get', 'name' => $model->icon, 'width' => 50, 'height' => 50]));
                },
            ],
            'name',
            'seo_title',
            'seo_description',
            'seo_keywords',
            'status:categoryStatusName',
            'rate',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url) {
                        return \Yii::$app->user->can(User::CATEGORY_UPDATE) ? Html::a(
                            '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>',
                            $url,
                            ['class' => 'js-show-modal', 'title' => 'Редактировать']
                        ) : '';
                    },
                ],
            ],
        ],
    ]
);