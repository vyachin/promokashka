<?php

use backend\widgets\Select;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model common\models\Category
 * @var $form yii\widgets\ActiveForm
 */
?>

<?php Modal::begin(
    [
        'header' => '<h4 class="modal-title">'.($model->isNewRecord ? 'Новая категория' : 'Редактирование категории').'</h4>',
        'footer' =>
            Html::button('Закрыть', ['class' => 'btn btn-sm btn-default', 'data-dismiss' => 'modal']).
            Html::button(
                $model->isNewRecord ? 'Добавить' : 'Сохранить',
                ['class' => 'btn btn-sm btn-primary js-submit']
            ),
    ]
); ?>
<?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
<?= $form->field($model, 'name')->textInput(['class' => 'form-control input-sm']); ?>
    <div id="js-image" data-image="<?= $model->icon ?>"
         data-image-url="<?= \yii\helpers\Url::to(['image/view', 'name' => $model->icon]) ?>">
        <?= $form->field($model, 'icon')->hiddenInput(['v-model' => 'image']); ?>
        <img :src="imageUrl" width="300">
        <div>
            <button type="button" class="btn btn-sm btn-success" v-on:click="uploadImage()">Загрузить</button>
            <button type="button" class="btn btn-sm btn-warning" v-on:click="removeImage()">Удалить</button>
        </div>
    </div>
<?= $form->field($model, 'description')->textarea(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'seo_title')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'seo_description')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'seo_keywords')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'status')->widget(
    Select::class,
    [
        'options' => ['class' => 'form-control input-sm', 'data-style' => 'btn-sm btn-default'],
        'items' => $model::getStatusList(),
    ]
) ?>
<?php ActiveForm::end(); ?>
<?php Modal::end() ?>