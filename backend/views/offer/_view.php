<?php

use common\models\User;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\Offer $model
 */
?>

<?php Modal::begin(
    [
        'header' => '<h4 class="modal-title">Просмотр промокода</h4>',
        'footer' =>
            (\Yii::$app->user->can(User::OFFER_GET_WORK, ['model' => $model]) ? Html::button(
                'Взять в работу',
                ['class' => 'btn btn-sm btn-success js-get-work', 'data-offer-id' => $model->id]
            ) : '').
            Html::button('Закрыть', ['class' => 'btn btn-sm btn-default', 'data-dismiss' => 'modal']),
    ]
); ?>
<?= DetailView::widget(
    [
        'model' => $model,
        'attributes' => [
            'name',
            'description',
            'category_names',
            [
                'label' => 'Категории',
                'value' => implode(', ', ArrayHelper::map($model->categories, 'id', 'name')),
            ],
            'shop.name:text:Магазин',
            'type:offerTypeName',
            'status:offerStatusName',
            'begin_at:date',
            'end_at:date',
            'author.username:text:Редактор',
            'count_like',
            'count_dislike',
            'count_used',
        ],
    ]
) ?>
<?php Modal::end() ?>