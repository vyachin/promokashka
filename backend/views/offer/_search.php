<?php

use backend\models\OfferSearch;
use backend\widgets\Select;
use common\models\Shop;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model backend\models\OfferSearch
 * @var $form yii\widgets\ActiveForm
 */

$this->registerJs('jQuery("body").on("click", ".js-filter", function () { jQuery(".js-source-search").toggle(); });');
?>
<?= Html::a('Фильтр', '#', ['class' => 'btn btn-sm btn-success js-filter']) ?>
<div class="js-source-search" style="display: none;">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'enableClientValidation' => false,
        'enableClientScript' => false,
    ]); ?>
    <section class="item">
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'shop_id')->widget(
                    Select::class,
                    [
                        'options' => [
                            'class' => 'form-control input-sm',
                            'data-style' => 'btn-sm btn-default',
                            'multiple' => true,
                        ],
                        'items' => ArrayHelper::map(
                            Shop::find()->active()->orderBy(['name' => SORT_ASC])->all(),
                            'id',
                            'name'
                        ),
                    ]
                ) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'name')->textInput(['class' => 'form-control input-sm']); ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'description')->textInput(['class' => 'form-control input-sm']); ?>
            </div>
            <?php if ($model->getScenario() === OfferSearch::SCENARIO_DEFAULT): ?>
                <div class="col-sm-1">
                    <?= $form->field($model, 'admitad_id')->textInput(['class' => 'form-control input-sm']); ?>
                </div>
                <div class="col-sm-1">
                    <?= $form->field($model, 'cityads_id')->textInput(['class' => 'form-control input-sm']); ?>
                </div>
                <div class="col-sm-1">
                    <?= $form->field($model, 'actionpay_id')->textInput(['class' => 'form-control input-sm']); ?>
                </div>
                <div class="col-sm-1">
                    <?= $form->field($model, 'id')->textInput(['class' => 'form-control input-sm']); ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'type')->widget(
                        Select::class,
                        [
                            'options' => [
                                'class' => 'form-control input-sm',
                                'prompt' => 'Все',
                                'data-style' => 'btn-sm btn-default',
                            ],
                            'items' => $model::getTypeList(),
                        ]
                    ) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'status')->widget(
                        Select::class,
                        [
                            'options' => [
                                'class' => 'form-control input-sm',
                                'prompt' => 'Все',
                                'data-style' => 'btn-sm btn-default',
                            ],
                            'items' => $model::getStatusList(),
                        ]
                    ) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'author_id')->widget(
                        Select::class,
                        [
                            'options' => [
                                'class' => 'form-control input-sm',
                                'prompt' => 'Все',
                                'data-style' => 'btn-sm btn-default',
                            ],
                            'items' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                        ]
                    ) ?>
                </div>
            <?php endif ?>
            <div class="col-sm-1">
                <label for="" class="control-label">&nbsp;</label>
                <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
            <div class="col-sm-1">
                <label for="" class="control-label">&nbsp;</label>
                <?= Html::resetButton('Сброс', ['class' => 'btn btn-sm btn-default']) ?>
            </div>
        </div>
    </section>
    <?php ActiveForm::end(); ?>
</div>