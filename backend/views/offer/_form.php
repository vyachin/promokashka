<?php

use backend\widgets\Select;
use common\models\Category;
use common\models\Offer;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Offer $model
 * @var yii\widgets\ActiveForm $form
 * @var string[] $shops
 * @var string[] $statuses
 */
?>

<?php Modal::begin(
    [
        'header' => '<h4 class="modal-title">'.($model->isNewRecord ? 'Новый промокод' : 'Редактирование промокода').'</h4>',
        'footer' =>
            Html::button('Закрыть', ['class' => 'btn btn-sm btn-default', 'data-dismiss' => 'modal']).
            Html::button(
                $model->isNewRecord ? 'Добавить' : 'Сохранить',
                ['class' => 'btn btn-sm btn-primary js-submit']
            ),
    ]
); ?>
<?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
<?php if ($model->getScenario() != Offer::SCENARIO_EDITOR_UPDATE): ?>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'shop_id')->widget(
                Select::class,
                [
                    'options' => [
                        'class' => 'form-control input-sm',
                        'data-style' => 'btn-sm btn-default',
                        'disabled' => $model->getScenario() == Offer::SCENARIO_EDITOR_UPDATE,
                    ],
                    'items' => $shops,
                ]
            ) ?>
        </div>
        <div class="col-md-6">    <?= $form->field($model, 'author_id')->widget(
                Select::class,
                [
                    'options' => [
                        'class' => 'form-control input-sm',
                        'data-style' => 'btn-sm btn-default',
                        'prompt' => 'Нет',
                        'disabled' => $model->getScenario() == Offer::SCENARIO_EDITOR_UPDATE,
                    ],
                    'items' => ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username'),
                ]
            ) ?>
        </div>
    </div>

<?php endif ?>
<?= $form->field($model, 'name')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'description')->textarea(['class' => 'form-control input-sm', 'style' => 'height: 112px;']); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'content_unique_percent')->textInput(
                [
                    'class' => 'form-control input-sm',
                    'disabled' => $model->getScenario() == Offer::SCENARIO_EDITOR_UPDATE,
                ]
            ); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'type')->widget(
                Select::class,
                [
                    'options' => ['class' => 'form-control input-sm', 'data-style' => 'btn-sm btn-default'],
                    'items' => $model::getTypeList(),
                ]
            ) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->widget(
                Select::class,
                [
                    'options' => ['class' => 'form-control input-sm', 'data-style' => 'btn-sm btn-default'],
                    'items' => $statuses,
                ]
            ) ?>

        </div>
    </div>
<?= $form->field($model, 'category_list')->widget(
    Select::class,
    [
        'options' => ['class' => 'form-control input-sm', 'data-style' => 'btn-sm btn-default', 'multiple' => true],
        'items' => ArrayHelper::map(Category::find()->active()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
    ]
) ?>
    <div class="form-group"><?= Html::encode($model->category_names) ?></div>
    <div class="form-group">
        <p>
            <?= Html::a('Ссылка', $model->url, ['target' => '_blank']) ?>
            Действует с <b><?= \Yii::$app->formatter->asDatetime($model->begin_at) ?></b> по
            <b><?= \Yii::$app->formatter->asDatetime($model->end_at) ?></b>

            <?php if ($model->admitad_id) : ?>
                admitad <b>#<?= $model->admitad_id ?></b>
            <?php endif ?>
            <?php if ($model->cityads_id) : ?>
                cityads <b>#<?= $model->cityads_id ?></b>
            <?php endif ?>
            <?php if ($model->actionpay_id) : ?>
                actionpay <b>#<?= $model->actionpay_id ?></b>
            <?php endif ?>

            <?= Html::encode($model->promo_code) ?>
        </p>
    </div>
<?php ActiveForm::end(); ?>
<?php Modal::end() ?>