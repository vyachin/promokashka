<?php
/**
 * @var $this yii\web\View
 * @var $searchModel backend\models\OfferSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use backend\assets\ModalAsset;
use common\models\Offer;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

$this->title = 'Список промокодов';
ModalAsset::register($this);
$this->registerJs(
    'jQuery(".js-show-modal").ShowModal({beforeShow: function (_modal) { jQuery(".selectpicker", _modal).selectpicker(); } });
    jQuery(".js-view-offer").ViewOffer('.Json::encode(['url' => Url::to(['get-work'])]).');'
);
?>
    <h2 class="page-title"><?= $this->title ?></h2>

<?= $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget(
    [
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'pager' => ['maxButtonCount' => 20],
        'columns' => [
            'id',
            'shop.name:text:Магазин',
            'name',
            'status:offerStatusName',
            'content_unique_percent:percent:A',
            'count_used:integer:T',
            'count_like:integer:+',
            'count_dislike:integer:-',
            'descriptionLength:integer:L',
            [
                'format' => 'integer',
                'value' => function (Offer $model) {
                    return $model->getCategories()->count();
                },
                'label' => 'C',
            ],
            'author.username:text:Автор',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'buttons' => [
                    'update' => function ($url, Offer $model) {
                        return \Yii::$app->user->can(User::OFFER_UPDATE, ['model' => $model]) ? Html::a(
                            '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>',
                            $url,
                            ['class' => 'js-show-modal', 'title' => 'Редактировать']
                        ) : '';
                    },
                    'view' => function ($url, Offer $model) {
                        return \Yii::$app->user->can(User::OFFER_VIEW, ['model' => $model]) ? Html::a(
                            '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>',
                            $url,
                            ['class' => 'js-view-offer', 'title' => 'Просмотр']
                        ) : '';
                    },
                ],
            ],
        ],
    ]
);