<?php

use backend\widgets\Select;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model common\models\Seo
 * @var $form yii\widgets\ActiveForm
 */
?>

<?php Modal::begin(
    [
        'header' => '<h4 class="modal-title">'.($model->isNewRecord ? 'Новая запись' : 'Редактирование записи').'</h4>',
        'footer' =>
            Html::button('Закрыть', ['class' => 'btn btn-sm btn-default', 'data-dismiss' => 'modal']).
            Html::button(
                $model->isNewRecord ? 'Добавить' : 'Сохранить',
                ['class' => 'btn btn-sm btn-primary js-submit']
            ),
    ]
); ?>
<?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
<?= $form->field($model, 'page')->widget(
    Select::class,
    [
        'options' => ['class' => 'form-control input-sm', 'data-style' => 'btn-sm btn-default'],
        'items' => $model::getPageList(),
    ]
) ?>
<?= $form->field($model, 'seo_title')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'seo_description')->textInput(['class' => 'form-control input-sm']); ?>
<?= $form->field($model, 'seo_keywords')->textInput(['class' => 'form-control input-sm']); ?>
<?php ActiveForm::end(); ?>
<?php Modal::end() ?>