<?php use backend\assets\ModalAsset;
use common\models\User;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $searchModel backend\models\CategorySearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = 'SEO';
ModalAsset::register($this);
$this->registerJs(
    'jQuery(".js-show-modal").ShowModal({beforeShow: function (_modal) { jQuery(".selectpicker", _modal).selectpicker(); } });'
);
?>
    <h2 class="page-title"><?= $this->title ?></h2>
<?php if (\Yii::$app->user->can(User::SEO_CREATE)): ?>
    <?= Html::a('Новая запись', ['create'], ['class' => 'btn btn-sm btn-primary js-show-modal']) ?>
<?php endif ?>

<?= $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget(
    [
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'pager' => ['maxButtonCount' => 20],
        'columns' => [
            'id',
            'page:seoPageName',
            'seo_title',
            'seo_description',
            'seo_keywords',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'class' => ActionColumn::class,
                'template' => '{update}',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can(User::SEO_UPDATE),
                ],
                'buttons' => [
                    'update' => function ($url) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>',
                            $url,
                            ['class' => 'js-show-modal', 'title' => 'Редактировать']
                        );
                    },
                ],
            ],
        ],
    ]
);