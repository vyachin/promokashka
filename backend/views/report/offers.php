<?php
/**
 * @var $this yii\web\View
 * @var $model backend\models\OffersReport
 */

use backend\widgets\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$currentShop = null;
$this->title = 'Список промокодов';
?>
<h2 class="page-title"><?= $this->title ?></h2>
<?php $form = ActiveForm::begin(
    [
        'action' => ['offers'],
        'method' => 'get',
        'enableClientValidation' => false,
        'enableClientScript' => false,
    ]
); ?>
<div class="row">
    <div class="col-sm-2">
        <?= $form->field($model, 'created_from')->widget(DatePicker::class, ['format' => 'YYYY-MM-DD']); ?>
    </div>
    <div class="col-sm-2">
        <?= $form->field($model, 'created_to')->widget(DatePicker::class, ['format' => 'YYYY-MM-DD']); ?>
    </div>
    <div class="col-sm-1">
        <label for="" class="control-label">&nbsp;</label>
        <?= Html::submitButton('Найти', ['class' => 'btn btn-sm btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php foreach ($model->getOffers() as $offer): ?>
    <?php if ($currentShop != $offer->shop_id): ?>
        <h1><?= $offer->shop->name ?></h1>
        <?php $currentShop = $offer->shop_id; ?>
    <?php endif ?>
    <div>
        <h4><?= $offer->name ?></h4>
        <div><?= $offer->description ?></div>
        <?php $url = Url::to(['/site/shop', 'slug' => $offer->shop->slug, 'id' => $offer->id], true) ?>
        <div><?= Html::a($url, $url) ?></div>
    </div>
<?php endforeach ?>
